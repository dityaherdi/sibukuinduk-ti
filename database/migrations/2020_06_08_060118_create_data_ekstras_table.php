<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataEkstrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_ekstras', function (Blueprint $table) {
            $table->bigIncrements('id_data_ekstra');
            $table->string('nama_ekstra')->nullable();
            $table->string('pembina_ekstra')->nullable();
            $table->string('deskripsi_ekstra')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_ekstras');
    }
}
