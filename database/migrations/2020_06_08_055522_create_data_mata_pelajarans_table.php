<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataMataPelajaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_mata_pelajarans', function (Blueprint $table) {
            $table->bigIncrements('id_data_mata_pelajaran');
            $table->string('kode_mapel')->nullable();
            $table->string('nama_mp')->nullable();
            $table->string('semester')->nullable();
            $table->string('jurusan')->nullable();
            $table->string('kkm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_mata_pelajarans');
    }
}
