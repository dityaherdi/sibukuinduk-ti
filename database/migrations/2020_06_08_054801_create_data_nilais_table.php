<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_nilais', function (Blueprint $table) {
            $table->bigIncrements('id_data_nilai');
            // $table->integer('id_user')->unsigned()->nullable();
            // $table->string('nama')->nullable();
            $table->string('id_data_mata_pelajaran')->nullable();
            $table->string('kode_rombel')->nullable();
            $table->string('nis')->nullable();
            // $table->string('nama_mp')->nullable();
            // $table->string('semester')->nullable();
            $table->string('nilai_pengetahuan')->nullable();
            $table->string('nilai_keterampilan')->nullable();
            $table->string('nilai_sikap')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_nilais');
    }
}
