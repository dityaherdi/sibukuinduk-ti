<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPribadiSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pribadi_siswas', function (Blueprint $table) {
            $table->bigIncrements('id_data_pribadi_siswa');
            $table->integer('id_user')->unsigned()->nullable();
            $table->string('nama_siswa_lengkap')->nullable();
            $table->string('nama_siswa_panggilan')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('ttgl_lahir')->nullable();
            $table->string('agama')->nullable();
            $table->string('kewarganegaraan')->nullable();
            $table->string('anak_keberapa')->nullable();
            $table->string('jum_saudara_kandung')->nullable();
            $table->string('jum_saudara_tiri')->nullable();
            $table->string('jum_saudara_angkat')->nullable();
            $table->string('status_anak')->nullable();
            $table->string('bahasa_seharihari')->nullable();
            $table->string('alamat')->nullable();
            $table->string('no_tlp')->nullable();
            $table->string('tinggal_dgn')->nullable();
            $table->string('jarak_kesekolah')->nullable();
            $table->string('golongan_darah')->nullable();
            $table->text('penyakit_yang_pernah_diderita')->nullable();
            $table->text('kelainan_jasmani')->nullable();
            $table->string('tinggi')->nullable();
            $table->string('berat_badan')->nullable();
            $table->string('lulusan_dari')->nullable();
            $table->date('tgl_lulus')->nullable();
            $table->string('no_sttb')->nullable();
            $table->string('lama_belajar')->nullable();
            $table->string('pindahan_sekolah')->nullable();
            $table->string('alasan')->nullable();
            $table->string('diterima_kls')->nullable();
            $table->string('kelompok')->nullable();
            $table->string('jurusan')->nullable();
            $table->date('tgl_diterima')->nullable();
            $table->text('kesenian')->nullable();
            $table->text('olahraga')->nullable();
            $table->text('kemasyarakatan')->nullable();
            $table->text('lainya')->nullable();
            $table->string('foto')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pribadi_siswas');
    }
}
