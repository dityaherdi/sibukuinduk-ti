<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataJenisRombelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_jenis_rombels', function (Blueprint $table) {
            $table->bigIncrements('id_data_jenis_rombel');
            $table->string('nama_rombel')->nullable();
            $table->string('jurusan')->nullable();
            $table->string('thn_ajaran')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_jenis_rombels');
    }
}
