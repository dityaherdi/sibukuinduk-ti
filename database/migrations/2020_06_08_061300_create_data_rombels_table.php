<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataRombelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_rombels', function (Blueprint $table) {
            $table->bigIncrements('id_data_rombel');
            $table->integer('id_data_jenis_rombel')->unsigned()->nullable();
            $table->integer('id_user')->unsigned()->nullable();
            $table->string('nis')->nullable();
            // $table->string('nama_siswa')->nullable();
            // $table->string('thn_ajaran')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_rombels');
    }
}
