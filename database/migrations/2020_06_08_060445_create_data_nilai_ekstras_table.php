<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataNilaiEkstrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_nilai_ekstras', function (Blueprint $table) {
            $table->bigIncrements('id_data_nilai_ekstra');
            $table->integer('id_data_ekstra')->nullable();
            $table->integer('id_user')->nullable();
            $table->string('nis')->nullable();
            $table->string('semester')->nullable();
            $table->string('nilai_siswa')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_nilai_ekstras');
    }
}
