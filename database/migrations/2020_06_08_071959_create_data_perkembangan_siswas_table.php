<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataPerkembanganSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_perkembangan_siswas', function (Blueprint $table) {
            $table->bigIncrements('id_data_perkembangan_siswa');
            $table->integer('id_user')->unsigned()->nullable();
            $table->string('menerima_beasiswa')->nullable();
            $table->date('tgl_meninggalkan_sekolah')->nullable();
            $table->string('alasan_keluar')->nullable();
            $table->string('thn_tamat_belajar')->nullable();
            $table->string('no_sttb_tamat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_perkembangan_siswas');
    }
}
