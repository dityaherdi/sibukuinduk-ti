<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataKeluargaSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_keluarga_siswas', function (Blueprint $table) {
            $table->bigIncrements('id_data_keluarga_siswa');
            $table->integer('id_user')->unsigned()->nullable();
            $table->string('nama_ayah')->nullable();
            $table->string('ttgl_lahir_ayah')->nullable();
            $table->string('agama_ayah')->nullable();
            $table->string('kewarganegaraan_ayah')->nullable();
            $table->string('pendidikan_ayah')->nullable();
            $table->string('pekerjaan_ayah')->nullable();
            $table->string('penghasilan_bln_ayah')->nullable();
            $table->string('alamat_ayah')->nullable();
            $table->string('tlp_ayah')->nullable();
            $table->text('ket_ayah')->nullable();
            $table->string('nama_ibu')->nullable();
            $table->string('ttgl_lahir_ibu')->nullable();
            $table->string('agama_ibu')->nullable();
            $table->string('kewarganegaraan_ibu')->nullable();
            $table->string('pendidikan_ibu')->nullable();
            $table->string('pekerjaan_ibu')->nullable();
            $table->string('penghasilan_bln_ibu')->nullable();
            $table->string('alamat_ibu')->nullable();
            $table->string('tlp_ibu')->nullable();
            $table->text('ket_ibu')->nullable();
            $table->string('nama_wali')->nullable();
            $table->string('ttgl_lahir_wali')->nullable();
            $table->string('agama_wali')->nullable();
            $table->string('kewarganegaraan_wali')->nullable();
            $table->string('pendidikan_wali')->nullable();
            $table->string('pekerjaan_wali')->nullable();
            $table->string('penghasilan_bln_wali')->nullable();
            $table->string('alamat_wali')->nullable();
            $table->string('tlp_wali')->nullable();
            $table->text('ket_wali')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_keluarga_siswas');
    }
}
