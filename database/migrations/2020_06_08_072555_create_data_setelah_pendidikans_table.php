<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataSetelahPendidikansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_setelah_pendidikans', function (Blueprint $table) {
            $table->bigIncrements('id_data_setelah_pendidikan');
            $table->integer('id_user')->unsigned()->nullable();
            $table->string('melanjutkan_di_bekerja')->nullable();
            $table->date('tgl_kerja')->nullable();
            $table->string('nama_perusahaan_lembaga_dll')->nullable();
            $table->string('penghasilan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_setelah_pendidikans');
    }
}
