<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataKehadiranSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_kehadiran_siswas', function (Blueprint $table) {
            $table->bigIncrements('id_data_kehadiran_siswa');
            $table->integer('id_user')->nullable();
            $table->string('nis')->nullable();
            $table->string('semester')->nullable();
            $table->string('total_sakit')->nullable();
            $table->string('total_ijin')->nullable();
            $table->string('total_tanpaket')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_kehadiran_siswas');
    }
}
