<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataNilaiIjazahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_nilai_ijazahs', function (Blueprint $table) {
            $table->bigIncrements('id_data_nilai_ijazah');
            $table->string('id_data_mata_pelajaran')->nullable();
            $table->string('kode_rombel')->nullable();
            $table->string('nis')->nullable();
            $table->string('nilai_rata_rata')->nullable();
            $table->string('nilai_ujian')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_nilai_ijazahs');
    }
}
