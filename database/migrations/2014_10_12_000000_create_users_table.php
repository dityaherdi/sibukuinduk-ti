<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id_user');
            $table->string('nama');
            $table->string('nis')->nullable(); // Untuk penambahan Kaur kesiswaan dan BK tidak perlu NIS
            $table->string('username')->unique();
            $table->string('password');
            $table->integer('level'); // 1. Kaur kesiswaan (kasek), 2. BK (bk), 3. Siswa
            $table->string('avatar')->nullable();
            $table->boolean('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
