
<?php

use Illuminate\Database\Seeder;

class data_ekstras extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_ekstras')->insert([
            [
                'nama_ekstra' => 'Basket',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami passing, shooting under basket, 3 point dan strategi dalam permainan basket',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Bulutangkis',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami teknik pukulan footwork, lop, drop, dan smash dalam Bulutangkis',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Sepakbola',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami peraturan, teknik, dan strategi dalam Sepak Bola',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Broadcast',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Menerapkan Audio Visual',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Jurnalistik',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami dan Menerapkan Ilmu Jurnalis',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Teater',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami teknik teater serta memperagakan dan menjiwai peran yang diberikan',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'English Club',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami dan menguasai Bahasa Inggris dalam percakapan sehari-hari',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Catur',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami dan menguasai startegi dan teknik bermain Catur',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Pemrograman Java',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami teknik pemrograman menggunakan bahasa java dan mengkombinasikannya dalam pemrograman bahasa lain',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Mekidung',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami teknik mekidung serta mempraktekkannya ke dalam kegiatan acara agama',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Tarung Derajat',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami gerakan tangan, gerakan kaki, teknik bertahan, menyerang dan teknik bertarung ',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Dance',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami konsep seni gerak tari kontemporer',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Silat',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami gerakan tangan, gerakan kaki, teknik bertahan, menyerang dan teknik silat ',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'nama_ekstra' => 'Web Design',
                'pembina_ekstra' => 'Guru',
                'deskripsi_ekstra' => 'Memahami pembuatan Web Design',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ]
                
        ]);
    }
}
