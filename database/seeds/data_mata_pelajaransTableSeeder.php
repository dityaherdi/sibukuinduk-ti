<?php

use Illuminate\Database\Seeder;

class Mata_PelajaranTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_mata_pelajarans')->insert([
            [
                'kode_mapel' => '1_pendidikan_agama_dan_budi_pekerti',
                'nama_mp' => 'Pendidikan Agama dan Budi Pekerti',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_ppkn',
                'nama_mp' => 'PPKN',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_bahasa_indonesia',
                'nama_mp' => 'Bahasa Indonesia',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_matematika',
                'nama_mp' => 'Matematika',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_sejarah_indonesia',
                'nama_mp' => 'Sejarah Indonesia',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_bahasa_inggris',
                'nama_mp' => 'Bahasa Inggris',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_seni_budaya',
                'nama_mp' => 'Seni Budaya',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_penjasorkes',
                'nama_mp' => 'Penjasorkes',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_penjasorkes',
                'nama_mp' => 'Penjasorkes',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_penjasorkes',
                'nama_mp' => 'Penjasorkes',
                'semester' => '3',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_penjasorkes',
                'nama_mp' => 'Penjasorkes',
                'semester' => '4',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_bahasa_daerah_bali',
                'nama_mp' => 'Bahasa Daerah Bali',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_pendidikan_agama_dan_budi_pekerti',
                'nama_mp' => 'Pendidikan Agama dan Budi Pekerti',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_pendidikan_pancasila_dan_kewarganegaraan',
                'nama_mp' => 'Pendidikan Pancasila dan Kewarganegaraan',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_bahasa_indonesia',
                'nama_mp' => 'Bahasa Indonesia',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_matematika',
                'nama_mp' => 'Matematika',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_sejarah_indonesia',
                'nama_mp' => 'Sejarah Indonesia',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_bahasa_inggris',
                'nama_mp' => 'Bahasa Inggris',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_seni_budaya',
                'nama_mp' => 'Seni Budaya',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_pendidikan_jasmani_olah_raga_dan_kesehatan',
                'nama_mp' => 'Pendidikan Jasmani Olah Raga dan Kesehatan',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_bahasa_daerah_bali',
                'nama_mp' => 'Bahasa Daerah Bali',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '73',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_pendidikan_agama_dan_budi_pekerti',
                'nama_mp' => 'Pendidikan Agama dan Budi Pekerti',
                'semester' => '3',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_pendidikan_pancasila_dan_kewarganegaraan',
                'nama_mp' => 'Pendidikan Pancasila dan Kewarganegaraan',
                'semester' => '3',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_bahasa_indonesia',
                'nama_mp' => 'Bahasa Indonesia',
                'semester' => '3',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_matematika',
                'nama_mp' => 'Matematika',
                'semester' => '3',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_bahasa_inggris',
                'nama_mp' => 'Bahasa Inggris',
                'semester' => '3',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_pendidikan_jasmani_olah_raga_dan_kesehatan',
                'nama_mp' => 'Pendidikan Jasmani Olah Raga dan Kesehatan',
                'semester' => '3',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_bahasa_daerah_bali',
                'nama_mp' => 'Bahasa Daerah Bali',
                'semester' => '3',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_pendidikan_agama_dan_budi_pekerti',
                'nama_mp' => 'Pendidikan Agama dan Budi Pekerti',
                'semester' => '4',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_pendidikan_pancasila_dan_kewarganegaraan',
                'nama_mp' => 'Pendidikan Pancasila dan Kewarganegaraan',
                'semester' => '4',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_bahasa_indonesia',
                'nama_mp' => 'Bahasa Indonesia',
                'semester' => '4',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_matematika',
                'nama_mp' => 'Matematika',
                'semester' => '4',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_bahasa_inggris',
                'nama_mp' => 'Bahasa Inggris',
                'semester' => '4',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_pendidikan_jasmani_olah_raga_dan_kesehatan',
                'nama_mp' => 'Pendidikan Jasmani Olah Raga dan Kesehatan',
                'semester' => '4',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_bahasa_daerah_bali',
                'nama_mp' => 'Bahasa Daerah Bali',
                'semester' => '4',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '76',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_pendidikan_agama_dan_budi_pekerti',
                'nama_mp' => 'Pendidikan Agama dan Budi Pekerti',
                'semester' => '5',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_pendidikan_pancasila_dan_kewarganegaraan',
                'nama_mp' => 'Pendidikan Pancasila dan Kewarganegaraan',
                'semester' => '5',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_bahasa_indonesia',
                'nama_mp' => 'Bahasa Indonesia',
                'semester' => '5',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_matematika',
                'nama_mp' => 'Matematika',
                'semester' => '5',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_bahasa_inggris',
                'nama_mp' => 'Bahasa Inggris',
                'semester' => '5',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_bahasa_daerah_bali',
                'nama_mp' => 'Bahasa Daerah Bali',
                'semester' => '5',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_pendidikan_agama_dan_budi_pekerti',
                'nama_mp' => 'Pendidikan Agama dan Budi Pekerti',
                'semester' => '6',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_pendidikan_pancasila_dan_kewarganegaraan',
                'nama_mp' => 'Pendidikan Pancasila dan Kewarganegaraan',
                'semester' => '6',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_bahasa_indonesia',
                'nama_mp' => 'Bahasa Indonesia',
                'semester' => '6',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_matematika',
                'nama_mp' => 'Matematika',
                'semester' => '6',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_bahasa_inggris',
                'nama_mp' => 'Bahasa Inggris',
                'semester' => '6',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_bahasa_daerah_bali',
                'nama_mp' => 'Bahasa Daerah Bali',
                'semester' => '6',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_simulasi_dan_komunikasi_digital',
                'nama_mp' => 'Simulasi dan Komunikasi Digital',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_simulasi_dan_komunikasi_visual',
                'nama_mp' => 'Simulasi dan Komunikasi Visual',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_simulasi_dan_komunikasi_digital',
                'nama_mp' => 'Simulasi dan Komunikasi Digital',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_fisika',
                'nama_mp' => 'Fisika',
                'semester' => '1',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_kimia',
                'nama_mp' => 'Kimia',
                'semester' => '1',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_fisika_dan_kimia',
                'nama_mp' => 'Fisika dan Kimia',
                'semester' => '1',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_sistem_komputer',
                'nama_mp' => 'Sistem Komputer',
                'semester' => '1',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_komputer_dan_jaringan_dasar',
                'nama_mp' => 'Komputer dan Jaringan Dasar',
                'semester' => '1',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_pemrograman_dasar',
                'nama_mp' => 'Pemrograman Dasar',
                'semester' => '1',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_dasar_desain_grafis',
                'nama_mp' => 'Dasar Desain Grafis',
                'semester' => '1',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_fisika',
                'nama_mp' => 'Fisika',
                'semester' => '2',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_kimia',
                'nama_mp' => 'Kimia',
                'semester' => '2',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_sistem_komputer',
                'nama_mp' => 'Sistem Komputer',
                'semester' => '2',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_komputer_dan_jaringan_dasar',
                'nama_mp' => 'Komputer dan Jaringan Dasar',
                'semester' => '2',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_pemrograman_dasar',
                'nama_mp' => 'Pemrograman Dasar',
                'semester' => '2',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_dasar_desain_grafis',
                'nama_mp' => 'Dasar Desain Grafis',
                'semester' => '2',
                'jurusan' => 'RPL MM TKJ',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_tinjauan_seni',
                'nama_mp' => 'Tinjauan Seni',
                'semester' => '1',
                'jurusan' => 'AN DKV',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_dasar_dasar_kreatifitas',
                'nama_mp' => 'Dasar dasar Kreatifitas',
                'semester' => '1',
                'jurusan' => 'AN DKV',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_dasar_dasar_seni_rupa',
                'nama_mp' => 'Dasar dasar Seni Rupa',
                'semester' => '1',
                'jurusan' => 'AN DKV',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_dasar_dasar_desain_grafis',
                'nama_mp' => 'Dasar Dasar Desain Grafis',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_gambar',
                'nama_mp' => 'Gambar',
                'semester' => '1',
                'jurusan' => 'AN DKV',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_sketsa',
                'nama_mp' => 'Sketsa',
                'semester' => '1',
                'jurusan' => 'AN DKV',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_tinjauan_seni',
                'nama_mp' => 'Tinjauan Seni',
                'semester' => '2',
                'jurusan' => 'AN DKV',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_dasar_dasar_kreatifitas',
                'nama_mp' => 'Dasar dasar Kreatifitas',
                'semester' => '2',
                'jurusan' => 'AN DKV',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_dasar_dasar_seni_rupa',
                'nama_mp' => 'Dasar dasar Seni Rupa',
                'semester' => '2',
                'jurusan' => 'AN DKV',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_gambar',
                'nama_mp' => 'Gambar',
                'semester' => '2',
                'jurusan' => 'AN DKV',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_sketsa',
                'nama_mp' => 'Sketsa',
                'semester' => '2',
                'jurusan' => 'AN DKV',
                'kkm' => '75',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_produk_kreatif_dan_kewirausahaan',
                'nama_mp' => 'Produk Kreatif dan Kewirausahaan',
                'semester' => '3',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_produk_kreatif_dan_kewirausahaan',
                'nama_mp' => 'Produk Kreatif dan Kewirausahaan',
                'semester' => '4',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_produk_kreatif_dan_kewirausahaan',
                'nama_mp' => 'Produk Kreatif dan Kewirausahaan',
                'semester' => '5',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_produk_kreatif_dan_kewirausahaan',
                'nama_mp' => 'Produk Kreatif dan Kewirausahaan',
                'semester' => '6',
                'jurusan' => 'Semua Jurusan ',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_pemodelan_perangkat_lunak',
                'nama_mp' => 'Pemodelan Perangkat Lunak',
                'semester' => '3',
                'jurusan' => 'RPL',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_basis_data',
                'nama_mp' => 'Basis Data',
                'semester' => '3',
                'jurusan' => 'RPL',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_pemrograman_berorientasi_objek',
                'nama_mp' => 'Pemrograman Berorientasi Objek',
                'semester' => '3',
                'jurusan' => 'RPL',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_pemrograman_web_dan_perangkat_bergerak',
                'nama_mp' => 'Pemrograman Web dan Perangkat Bergerak',
                'semester' => '3',
                'jurusan' => 'RPL',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_pemodelan_perangkat_lunak',
                'nama_mp' => 'Pemodelan Perangkat Lunak',
                'semester' => '4',
                'jurusan' => 'RPL',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_basis_data',
                'nama_mp' => 'Basis Data',
                'semester' => '4',
                'jurusan' => 'RPL',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_pemrograman_berorientasi_objek',
                'nama_mp' => 'Pemrograman Berorientasi Objek',
                'semester' => '4',
                'jurusan' => 'RPL',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_pemrograman_web_dan_perangkat_bergerak',
                'nama_mp' => 'Pemrograman Web dan Perangkat Bergerak',
                'semester' => '4',
                'jurusan' => 'RPL',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_basis_data',
                'nama_mp' => 'Basis Data',
                'semester' => '5',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_pemrograman_berorientasi_objek',
                'nama_mp' => 'Pemrograman Berorientasi Objek',
                'semester' => '5',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_pemrograman_web_dan_perangkat_bergerak',
                'nama_mp' => 'Pemrograman Web dan Perangkat Bergerak',
                'semester' => '5',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_pemodelan_perangkat_lunak',
                'nama_mp' => 'Pemodelan Perangkat Lunak',
                'semester' => '5',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_basis_data',
                'nama_mp' => 'Basis Data',
                'semester' => '6',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_pemrograman_berorientasi_objek',
                'nama_mp' => 'Pemrograman Berorientasi Objek',
                'semester' => '6',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_pemrograman_web_dan_perangkat_bergerak',
                'nama_mp' => 'Pemrograman Web dan Perangkat Bergerak',
                'semester' => '6',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_pemodelan_perangkat_lunak',
                'nama_mp' => 'Pemodelan Perangkat Lunak',
                'semester' => '6',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_desain_grafis_percetakan',
                'nama_mp' => 'Desain Grafis Percetakan',
                'semester' => '3',
                'jurusan' => 'MM',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_teknik_animasi_2d_dan_3d',
                'nama_mp' => 'Teknik animasi 2D dan 3D',
                'semester' => '3',
                'jurusan' => 'MM',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_teknik_animasi_dan_3d',
                'nama_mp' => 'Teknik animasi dan 3D',
                'semester' => '3',
                'jurusan' => 'MM',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_teknik_animasi_dan_3d',
                'nama_mp' => 'Teknik animasi dan 3D',
                'semester' => '4',
                'jurusan' => 'MM',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_teknik_animasi_dan_3d',
                'nama_mp' => 'Teknik animasi dan 3D',
                'semester' => '5',
                'jurusan' => 'MM',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_teknik_animasi_dan_3d',
                'nama_mp' => 'Teknik animasi dan 3D',
                'semester' => '6',
                'jurusan' => 'MM',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_desain_grafis_percetakan',
                'nama_mp' => 'Desain Grafis Percetakan',
                'semester' => '4',
                'jurusan' => 'MM',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_teknik_animasi_2d_dan_3d',
                'nama_mp' => 'Teknik animasi 2D dan 3D',
                'semester' => '4',
                'jurusan' => 'MM',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_design_media_interaktif',
                'nama_mp' => 'Design media interaktif',
                'semester' => '5',
                'jurusan' => 'MM',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_teknik_pengolahan_audio_video',
                'nama_mp' => 'Teknik pengolahan audio video',
                'semester' => '5',
                'jurusan' => 'MM',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_design_media_interaktif',
                'nama_mp' => 'Design media interaktif',
                'semester' => '6',
                'jurusan' => 'MM',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_teknik_pengolahan_audio_video',
                'nama_mp' => 'Teknik pengolahan audio video',
                'semester' => '6',
                'jurusan' => 'MM',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_teknologi_jaringan_berbasis_luas',
                'nama_mp' => 'Teknologi Jaringan Berbasis Luas',
                'semester' => '3',
                'jurusan' => 'TKJ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_administrasi_infrastruktur_jaringan',
                'nama_mp' => 'Administrasi Infrastruktur Jaringan',
                'semester' => '3',
                'jurusan' => 'TKJ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_administrasi_sistem_jaringan',
                'nama_mp' => 'Administrasi Sistem Jaringan',
                'semester' => '3',
                'jurusan' => 'TKJ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_teknologi_layanan_jaringan',
                'nama_mp' => 'Teknologi Layanan Jaringan',
                'semester' => '3',
                'jurusan' => 'TKJ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_teknologi_jaringan_berbasis_luas',
                'nama_mp' => 'Teknologi Jaringan Berbasis Luas',
                'semester' => '4',
                'jurusan' => 'TKJ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_administrasi_infrastruktur_jaringan',
                'nama_mp' => 'Administrasi Infrastruktur Jaringan',
                'semester' => '4',
                'jurusan' => 'TKJ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_administrasi_sistem_jaringan',
                'nama_mp' => 'Administrasi Sistem Jaringan',
                'semester' => '4',
                'jurusan' => 'TKJ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_teknologi_layanan_jaringan',
                'nama_mp' => 'Teknologi Layanan Jaringan',
                'semester' => '4',
                'jurusan' => 'TKJ',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_administrasi_infrastruktur_jaringan',
                'nama_mp' => 'Administrasi Infrastruktur Jaringan',
                'semester' => '5',
                'jurusan' => 'TKJ',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_administrasi_sistem_jaringan',
                'nama_mp' => 'Administrasi Sistem Jaringan',
                'semester' => '5',
                'jurusan' => 'TKJ',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_teknologi_layanan_jaringan',
                'nama_mp' => 'Teknologi Layanan Jaringan',
                'semester' => '5',
                'jurusan' => 'TKJ',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_administrasi_infrastruktur_jaringan',
                'nama_mp' => 'Administrasi Infrastruktur Jaringan',
                'semester' => '6',
                'jurusan' => 'TKJ',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_administrasi_sistem_jaringan',
                'nama_mp' => 'Administrasi Sistem Jaringan',
                'semester' => '6',
                'jurusan' => 'TKJ',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_teknologi_layanan_jaringan',
                'nama_mp' => 'Teknologi Layanan Jaringan',
                'semester' => '6',
                'jurusan' => 'TKJ',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_desain_publikasi',
                'nama_mp' => 'Desain Publikasi',
                'semester' => '3',
                'jurusan' => 'DKV',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_komputer_grafis',
                'nama_mp' => 'Komputer Grafis',
                'semester' => '3',
                'jurusan' => 'DKV',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_fotografi',
                'nama_mp' => 'Fotografi',
                'semester' => '3',
                'jurusan' => 'DKV',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_videografi',
                'nama_mp' => 'Videografi',
                'semester' => '3',
                'jurusan' => 'AN DKV',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_desain_publikasi',
                'nama_mp' => 'Desain Publikasi',
                'semester' => '4',
                'jurusan' => 'DKV',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_komputer_grafis',
                'nama_mp' => 'Komputer Grafis',
                'semester' => '4',
                'jurusan' => 'DKV',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_fotografi',
                'nama_mp' => 'Fotografi',
                'semester' => '4',
                'jurusan' => 'DKV',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_videografi',
                'nama_mp' => 'Videografi',
                'semester' => '4',
                'jurusan' => 'AN DKV',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_desain_publikasi',
                'nama_mp' => 'Desain Publikasi',
                'semester' => '5',
                'jurusan' => 'DKV',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_komputer_grafis',
                'nama_mp' => 'Komputer Grafis',
                'semester' => '5',
                'jurusan' => 'DKV',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_fotografi',
                'nama_mp' => 'Fotografi',
                'semester' => '5',
                'jurusan' => 'DKV',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_videografi',
                'nama_mp' => 'Videografi',
                'semester' => '5',
                'jurusan' => 'DKV',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_desain_publikasi',
                'nama_mp' => 'Desain Publikasi',
                'semester' => '6',
                'jurusan' => 'DKV',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_komputer_grafis',
                'nama_mp' => 'Komputer Grafis',
                'semester' => '6',
                'jurusan' => 'DKV',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_fotografi',
                'nama_mp' => 'Fotografi',
                'semester' => '6',
                'jurusan' => 'DKV',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_videografi',
                'nama_mp' => 'Videografi',
                'semester' => '6',
                'jurusan' => 'DKV',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_animasi_2d',
                'nama_mp' => 'Animasi 2D',
                'semester' => '3',
                'jurusan' => 'AN',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_animasi_3d',
                'nama_mp' => 'Animasi 3D',
                'semester' => '3',
                'jurusan' => 'AN',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_proses_digital',
                'nama_mp' => 'Proses Digital',
                'semester' => '3',
                'jurusan' => 'AN',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_animasi_2d',
                'nama_mp' => 'Animasi 2D',
                'semester' => '4',
                'jurusan' => 'AN',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_animasi_3d',
                'nama_mp' => 'Animasi 3D',
                'semester' => '4',
                'jurusan' => 'AN',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_proses_digital',
                'nama_mp' => 'Proses Digital',
                'semester' => '4',
                'jurusan' => 'AN',
                'kkm' => '80',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_animasi_2d',
                'nama_mp' => 'Animasi 2D',
                'semester' => '5',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_animasi_3d',
                'nama_mp' => 'Animasi 3D',
                'semester' => '5',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_proses_digital',
                'nama_mp' => 'Proses Digital',
                'semester' => '5',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_animasi_2d',
                'nama_mp' => 'Animasi 2D',
                'semester' => '6',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_animasi_3d',
                'nama_mp' => 'Animasi 3D',
                'semester' => '6',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_proses_digital',
                'nama_mp' => 'Proses Digital',
                'semester' => '6',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],

                [
                'kode_mapel' => '1_ppkn',
                'nama_mp' => 'PPKN',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan',
                'kkm' => '74',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_ppkn',
                'nama_mp' => 'PPKN',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan',
                'kkm' => '74',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_simulasi_dan_komunikasi_visual',
                'nama_mp' => 'Simulasi dan Komunikasi Visual',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan',
                'kkm' => '74',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_simulasi_dan_komunikasi_visual',
                'nama_mp' => 'Simulasi dan Komunikasi Visual',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan',
                'kkm' => '74',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_fisika_dan_kimia',
                'nama_mp' => 'Fisika dan Kimia',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan',
                'kkm' => '74',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_fisika_dan_kimia',
                'nama_mp' => 'Fisika dan Kimia',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan',
                'kkm' => '74',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '1_dasar_dasar_desain_grafis',
                'nama_mp' => 'Dasar Dasar Desain Grafis',
                'semester' => '1',
                'jurusan' => 'Semua Jurusan',
                'kkm' => '74',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '2_dasar_dasar_desain_grafis',
                'nama_mp' => 'Dasar Dasar Desain Grafis',
                'semester' => '2',
                'jurusan' => 'Semua Jurusan',
                'kkm' => '74',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_pemrograman_web_dinamis_dan_perangkat_bergerak',
                'nama_mp' => 'Pemrograman Web Dinamis dan Perangkat Bergerak',
                'semester' => '5',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_pemrograman_web_dinamis_dan_perangkat_bergerak',
                'nama_mp' => 'Pemrograman Web Dinamis dan Perangkat Bergerak',
                'semester' => '6',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_pemrograman_web_dinamis_dan_perangkat_bergerak',
                'nama_mp' => 'Pemrograman Web Dinamis dan Perangkat Bergerak',
                'semester' => '5',
                'jurusan' => 'RPL',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_teknik_pengelolaan_audio_video',
                'nama_mp' => 'Teknik pengelolaan audio video',
                'semester' => '6',
                'jurusan' => 'MM',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_teknik_pengelolaan_audio_video',
                'nama_mp' => 'Teknik pengelolaan audio video',
                'semester' => '5',
                'jurusan' => 'MM',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '3_digital_processing',
                'nama_mp' => 'Digital Processing',
                'semester' => '3',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '4_digital_processing',
                'nama_mp' => 'Digital Processing',
                'semester' => '4',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '5_digital_processing',
                'nama_mp' => 'Digital Processing',
                'semester' => '5',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_digital_processing',
                'nama_mp' => 'Digital Processing',
                'semester' => '6',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],
                [
                'kode_mapel' => '6_desain_media_interaktif',
                'nama_mp' => 'Desain Media Interaktif',
                'semester' => '6',
                'jurusan' => 'AN',
                'kkm' => '85',
                'created_at' => NOW(),
                'updated_at' => NOW()
                ],

        ]);
    }
}
