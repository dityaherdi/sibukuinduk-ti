<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'nama' => 'John Doe',
                'nis' => null,
                'username' => 'johndoe',
                'password' => Hash::make('123456'),
                'level' => 1,
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'nama' => 'Hendra Adi',
                'nis' => null,
                'username' => 'hendraadi',
                'password' => Hash::make('123456'),
                'level' => 1,
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'nama' => 'Jane Doe',
                'nis' => null,
                'username' => 'janedoe',
                'password' => Hash::make('123456'),
                'level' => 2,
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'nama' => 'Kepala Sekolah',
                'nis' => null,
                'username' => 'kepsek',
                'password' => Hash::make('123456'),
                'level' => 4,
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ]
        ]);
    }
}