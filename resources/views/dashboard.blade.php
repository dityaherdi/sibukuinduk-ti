@extends('layouts.master',['activeMenu' => 'dashboard'])
@section('title','Dashboard')
@section('css')
<link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">    
@endsection
@section('content')
    @if (Auth::user()->level == 3)
    @php
        $siswa = \App\DataPribadiSiswa::where('id_user',$user->id_user)->first();
        $keluarga = \App\DataKeluargaSiswa::where('id_user', $user->id_user)->first();
    @endphp
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab">Data Pribadi Siswa</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Data Keluarga Siswa</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <form action="{{url('siswa/data-pribadi-siswa/edit')}}" method="post" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="put">
                                        <input type="hidden" name="id_user" value="{{$user->id_user}}">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Nama Lengkap</label>
                                                        <input type="text" class="form-control" name="nama_siswa_lengkap" placeholder="Masukkan Nama Lengkap"
                                                            value="{{ $siswa->nama_siswa_lengkap }}">
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jenis Kelamin</label>
                                                        <select name="jenis_kelamin" class="form-control" value="{{ $siswa->jenis_kelamin }}">
                                                            <option value="0">Pilih jenis kelamin</option>
                                                            <option value="Laki-laki" {{$siswa->jenis_kelamin == 'Laki-laki' ? 'selected' : ''}}>Laki-laki</option>
                                                            <option value="Perempuan" {{$siswa->jenis_kelamin == 'Perempuan' ? 'selected' : ''}}>Perempuan</option>
                                                        </select>
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12" id="ttl">
                                                    <label class="col-form-label">Tempat & Tanggal Lahir</label>
                                                    <div class="form-inline">
                                                        <input type="text" class="form-control" name="tempatLahir" placeholder="Tempat" value="{{ strtok($siswa->ttgl_lahir, ',') }}">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                              <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" name="tanggalLahir" id="tanggalLahir" value="{{substr($siswa->ttgl_lahir, strpos($siswa->ttgl_lahir, ",") + 1)}}">
                                                        </div>
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12" style="margin-top: 15px">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Anak Ke</label>
                                                        <input type="number" min="0" class="form-control" name="anak_keberapa" value="{{$siswa->anak_keberapa}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jumlah Saudara Kandung</label>
                                                        <input type="number" min="0" class="form-control" name="jum_saudara_kandung" value="{{$siswa->jum_saudara_kandung}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jumlah Saudara Tiri</label>
                                                        <input type="number" min="0" class="form-control" name="jum_saudara_tiri" value="{{$siswa->jum_saudara_tiri}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jumlah Saudara Angkat</label>
                                                        <input type="number" min="0" class="form-control" name="jum_saudara_angkat" value="{{$siswa->jum_saudara_angkat}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Status Anak</label>
                                                        <select name="status_anak" id="status_anak" class="form-control" value="{{$siswa->status_anak}}">
                                                            <option value="kandung" {{$siswa->status_anak == 'kandung' ? 'selected' : ''}}>Kandung</option>
                                                            <option value="tiri" {{$siswa->status_anak == 'tiri' ? 'selected' : ''}}>Tiri</option>
                                                            <option value="angkat" {{$siswa->status_anak == 'angkat' ? 'selected' : ''}}>Angkat</option>
                                                        </select>
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Golongan Darah</label>
                                                        <select name="golongan_darah" id="golongan_darah" class="form-control" value="{{$siswa->golongan_darah}}">
                                                            <option value="A" {{$siswa->golongan_darah == 'A' ? 'selected' : ''}}>A</option>
                                                            <option value="B" {{$siswa->golongan_darah == 'B' ? 'selected' : ''}}>B</option>
                                                            <option value="O" {{$siswa->golongan_darah == 'O' ? 'selected' : ''}}>O</option>
                                                            <option value="AB" {{$siswa->golongan_darah == 'AB' ? 'selected' : ''}}>AB</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Tinggi Badan (Sentimeter)</label>
                                                        <input type="number" min="0" class="form-control" name="tinggi" value="{{$siswa->tinggi}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Berat Badan (Kilogram)</label>
                                                        <input type="number" min="0" class="form-control" min="0" name="berat_badan" value="{{$siswa->berat_badan}}">
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Penyakit Yang Pernah Diderita</label> <br>
                                                    <textarea style="width: 100%" class="form-control" name="penyakit_yang_pernah_diderita" rows="3" id="penyakit_yang_pernah_diderita">{{$siswa->penyakit_yang_pernah_diderita}}
                                                    </textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Kelainan Jasmani</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="kelainan_jasmani" rows="3" id="kelainan_jasmani">{{$siswa->kelainan_jasmani}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Alasan Pindah</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="alasan" rows="3" id="alasan">{{$siswa->alasan}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Kesenian</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="kesenian" rows="3" id="kesenian">{{$siswa->kesenian}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Olahraga</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="olahraga" rows="3" id="olahraga">{{$siswa->olahraga}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    @if ($siswa->foto != null)
                                                        <img src="{{asset('images/fotosiswa/'.$siswa->foto)}}" alt="" class="img-responsive" style="height: 100px;">
                                                    @else
                                                    <div class="form-group">
                                                        <label>Foto</label>
                                                        <input type="file" id="fotoSiswa" name="fotoSiswa">
                                                    </div>
                                                    @endif
                                                </div>
                                               
                                            </div>
                
                                            <div class="col-xs-6">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Nama Panggilan</label>
                                                        <input type="text" class="form-control" name="nama_siswa_panggilan" placeholder="Masukkan Nama Panggilan"
                                                        value="{{$siswa->nama_siswa_panggilan}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Agama</label>
                                                        <select name="agama" id="agama" class="form-control" value="{{$siswa->agama}}">
                                                            <option value="buddha" {{$siswa->agama == 'buddha' ? 'selected' : ''}}>Buddha</option>
                                                            <option value="hindu" {{$siswa->agama == 'hindu' ? 'selected' : ''}}>Hindu</option>
                                                            <option value="islam" {{$siswa->agama == 'islam' ? 'selected' : ''}}>Islam</option>
                                                            <option value="katolik" {{$siswa->agama == 'katolik' ? 'selected' : ''}}>Katolik</option>
                                                            <option value="protestan" {{$siswa->agama == 'protestan' ? 'selected' : ''}}>Protestan</option>
                                                            <option value="konghucu" {{$siswa->agama == 'konghucu' ? 'selected' : ''}}>Konghucu</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" id="kewarganegaraan">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Kewarganegaraan</label>
                                                        <input type="text" class="form-control" name="kewarganegaraan" placeholder="Masukkan kewarganegaraan" value="{{$siswa->kewarganegaraan}}">
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Bahasa Sehari-hari</label>
                                                        <input type="text" class="form-control" name="bahasa_seharihari" value="{{$siswa->bahasa_seharihari}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Alamat</label>
                                                        <input type="text" class="form-control" name="alamat" value="{{$siswa->alamat}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Telepon</label>
                                                        <input type="text" class="form-control" name="no_tlp" value="{{$siswa->no_tlp}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Tinggal Bersama</label>
                                                        <input type="text" class="form-control" name="tinggal_dgn" value="{{$siswa->tinggal_dgn}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jarak Kesekolah (Kilometer)</label>
                                                        <input type="number" min="0" class="form-control" name="jarak_kesekolah" value="{{$siswa->jarak_kesekolah}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Lulusan Dari</label>
                                                        <input type="text" class="form-control" min="0" name="lulusan_dari" value="{{$siswa->lulusan_dari}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Tanggal Lulus</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                              <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" id="tgl_lulus" name="tgl_lulus" value="{{$siswa->tgl_lulus}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">No. STTB</label>
                                                        <input type="text" class="form-control" name="no_sttb" value="{{$siswa->no_sttb}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Lama Belajar (Tahun)</label>
                                                        <input type="number" min="0" class="form-control" name="lama_belajar" value="{{$siswa->lama_belajar}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Pindahan Sekolah</label>
                                                        <input type="text" class="form-control" name="pindahan_sekolah" value="{{$siswa->pindahan_sekolah}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Diterima Kelas</label>
                                                        <select name="diterima_kls" id="diterima_kls" class="form-control" value="{{$siswa->diterima_kls}}">
                                                            <option value="X" {{$siswa->diterima_kls == 'X' ? 'selected' : ''}}>X</option>
                                                            <option value="XI" {{$siswa->diterima_kls == 'XI' ? 'selected' : ''}}>XI</option>
                                                            <option value="XII" {{$siswa->diterima_kls == 'XII' ? 'selected' : ''}}>XII</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Kelompok</label>
                                                        <input type="text" class="form-control" name="kelompok" value="{{$siswa->kelompok}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jurusan</label>
                                                        <input type="text" class="form-control" name="jurusan" value="{{$siswa->jurusan}}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Tanggal Diterima</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                              <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" id="tgl_diterima" name="tgl_diterima" value="{{$siswa->tgl_diterima}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Lainya</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="lainya" rows="3" id="lainya">{{$siswa->lainya}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Kemasyarakatan</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="kemasyarakatan" rows="3" id="kemasyarakatan">{{$siswa->kemasyarakatan}}</textarea>
                                                    </div>
                                                </div>
                                                
                
                                                <div class="col-xs-12">
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                                <!-- UPDATE KELUARGA DARI SINI -->
                                <div class="tab-pane" id="tab_2">
                                    <form action="{{url('siswa/data-pribadi-siswa/editkeluarga')}}" method="post" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="put">
                                        <input type="hidden" name="id_user" value="{{$user->id_user}}">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Nama Ayah</label>
                                                        <input type="text" class="form-control" name="nama_ayah" placeholder="Masukkan Nama Ayah"
                                                            value="{{ $keluarga->nama_ayah }}">
                                                    </div>
                                                </div>
                                            
                                                <div class="col-xs-12" id="ttl">
                                                    <label class="col-form-label">Tempat & Tanggal Lahir Ayah</label>
                                                    <div class="form-inline">
                                                        <input type="text" class="form-control" name="tempatLahirAyah" placeholder="Tempat" value="{{ strtok($keluarga->ttgl_lahir_ayah, ',') }}">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                              <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" name="tanggalLahirAyah" id="tanggalLahirAyah" value="{{substr($keluarga->ttgl_lahir_ayah, strpos($keluarga->ttgl_lahir_ayah, ",") + 1)}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Agama Ayah</label>
                                                        <select name="agama_ayah" id="agama_ayah" class="form-control" value="{{$keluarga->agama_ayah}}">
                                                            <option value="buddha" {{$keluarga->agama_ayah == 'buddha' ? 'selected' : ''}}>Buddha</option>
                                                            <option value="hindu" {{$keluarga->agama_ayah == 'hindu' ? 'selected' : ''}}>Hindu</option>
                                                            <option value="islam" {{$keluarga->agama_ayah == 'islam' ? 'selected' : ''}}>Islam</option>
                                                            <option value="katolik" {{$keluarga->agama_ayah == 'katolik' ? 'selected' : ''}}>Katolik</option>
                                                            <option value="protestan" {{$keluarga->agama_ayah == 'protestan' ? 'selected' : ''}}>Protestan</option>
                                                            <option value="konghucu" {{$keluarga->agama_ayah == 'konghucu' ? 'selected' : ''}}>Konghucu</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Kewarganegaraan Ayah</label>
                                                        <input type="text" class="form-control" name="kewarganegaraan_ayah" placeholder="Kewarganegaraan Ayah"
                                                            value="{{ $keluarga->kewarganegaraan_ayah }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Pendidikan Ayah</label>
                                                        <input type="text" class="form-control" name="pendidikan_ayah" placeholder="Pendidikan Ayah"
                                                            value="{{ $keluarga->pendidikan_ayah }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Pekerjaan Ayah</label>
                                                        <input type="text" class="form-control" name="pekerjaan_ayah" placeholder="Pekerjaan Ayah"
                                                            value="{{ $keluarga->pekerjaan_ayah }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Penghasilan Ayah</label>
                                                        <input type="text" class="form-control" name="penghasilan_bln_ayah" placeholder="Penghasilan Ayah"
                                                            value="{{ $keluarga->penghasilan_bln_ayah }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Alamat Ayah</label>
                                                        <input type="text" class="form-control" name="alamat_ayah" placeholder="Alamat Ayah"
                                                            value="{{ $keluarga->alamat_ayah }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Telepon Ayah</label>
                                                        <input type="text" class="form-control" name="tlp_ayah" placeholder="Telepon Ayah"
                                                            value="{{ $keluarga->tlp_ayah }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Keterangan Ayah</label>
                                                        <input type="text" class="form-control" name="ket_ayah" placeholder="Keterangan Ayah"
                                                            value="{{ $keluarga->ket_ayah }}">
                                                    </div>
                                                </div>




                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Nama Ibu</label>
                                                        <input type="text" class="form-control" name="nama_ibu" placeholder="Masukkan Nama Ibu"
                                                            value="{{ $keluarga->nama_ibu }}">
                                                    </div>
                                                </div>
                                            
                                                <div class="col-xs-12" id="ttl">
                                                    <label class="col-form-label">Tempat & Tanggal Lahir Ibu</label>
                                                    <div class="form-inline">
                                                        <input type="text" class="form-control" name="tempatLahirIbu" placeholder="Tempat" value="{{ strtok($keluarga->ttgl_lahir_ibu, ',') }}">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                              <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" name="tanggalLahirIbu" id="tanggalLahirIbu" value="{{substr($keluarga->ttgl_lahir_ibu, strpos($keluarga->ttgl_lahir_ibu, ",") + 1)}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Agama Ibu</label>
                                                        <select name="agama_ibu" id="agama_ibu" class="form-control" value="{{$keluarga->agama_ibu}}">
                                                            <option value="buddha" {{$keluarga->agama_ibu == 'buddha' ? 'selected' : ''}}>Buddha</option>
                                                            <option value="hindu" {{$keluarga->agama_ibu == 'hindu' ? 'selected' : ''}}>Hindu</option>
                                                            <option value="islam" {{$keluarga->agama_ibu == 'islam' ? 'selected' : ''}}>Islam</option>
                                                            <option value="katolik" {{$keluarga->agama_ibu == 'katolik' ? 'selected' : ''}}>Katolik</option>
                                                            <option value="protestan" {{$keluarga->agama_ibu == 'protestan' ? 'selected' : ''}}>Protestan</option>
                                                            <option value="konghucu" {{$keluarga->agama_ibu == 'konghucu' ? 'selected' : ''}}>Konghucu</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Kewarganegaraan Ibu</label>
                                                        <input type="text" class="form-control" name="kewarganegaraan_ibu" placeholder="Kewarganegaraan Ibu"
                                                            value="{{ $keluarga->kewarganegaraan_ibu }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Pendidikan Ibu</label>
                                                        <input type="text" class="form-control" name="pendidikan_ibu" placeholder="Pendidikan Ibu"
                                                            value="{{ $keluarga->pendidikan_ibu }}">
                                                    </div>
                                                </div>

                                                

                                               
                
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Pekerjaan Ibu</label>
                                                        <input type="text" class="form-control" name="pekerjaan_ibu" placeholder="Pekerjaan Ibu"
                                                            value="{{ $keluarga->pekerjaan_ibu }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Penghasilan Ibu</label>
                                                        <input type="text" class="form-control" name="penghasilan_bln_ibu" placeholder="Penghasilan Ibu"
                                                            value="{{ $keluarga->penghasilan_bln_ibu }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Alamat Ibu</label>
                                                        <input type="text" class="form-control" name="alamat_ibu" placeholder="Alamat Ibu"
                                                            value="{{ $keluarga->alamat_ibu }}">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Telepon Ibu</label>
                                                        <input type="text" class="form-control" name="tlp_ibu" placeholder="Telepon Ibu"
                                                            value="{{ $keluarga->tlp_ibu }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Keterangan Ibu</label>
                                                        <input type="text" class="form-control" name="ket_ibu" placeholder="Keterangan Ibu"
                                                            value="{{ $keluarga->ket_ibu }}">
                                                    </div>
                                                </div>





                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Nama Wali</label>
                                                        <input type="text" class="form-control" name="nama_wali" placeholder="Masukkan Nama Wali"
                                                            value="{{ $keluarga->nama_wali }}">
                                                    </div>
                                                </div>
                                            
                                                <div class="col-xs-12" id="ttl">
                                                    <label class="col-form-label">Tempat & Tanggal Lahir Wali</label>
                                                    <div class="form-inline">
                                                        <input type="text" class="form-control" name="tempatLahirWali" placeholder="Tempat" value="{{ strtok($keluarga->ttgl_lahir_wali, ',') }}">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                              <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" name="tanggalLahirWali" id="tanggalLahirWali" value="{{substr($keluarga->ttgl_lahir_wali, strpos($keluarga->ttgl_lahir_wali, ",") + 1)}}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Agama Wali</label>
                                                        <select name="agama_wali" id="agama_wali" class="form-control" value="{{$keluarga->agama_wali}}">
                                                            <option value="buddha" {{$keluarga->agama_wali == 'buddha' ? 'selected' : ''}}>Buddha</option>
                                                            <option value="hindu" {{$keluarga->agama_wali == 'hindu' ? 'selected' : ''}}>Hindu</option>
                                                            <option value="islam" {{$keluarga->agama_wali == 'islam' ? 'selected' : ''}}>Islam</option>
                                                            <option value="katolik" {{$keluarga->agama_wali == 'katolik' ? 'selected' : ''}}>Katolik</option>
                                                            <option value="protestan" {{$keluarga->agama_wali == 'protestan' ? 'selected' : ''}}>Protestan</option>
                                                            <option value="konghucu" {{$keluarga->agama_wali == 'konghucu' ? 'selected' : ''}}>Konghucu</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Kewarganegaraan Wali</label>
                                                        <input type="text" class="form-control" name="kewarganegaraan_wali" placeholder="Kewarganegaraan Wali"
                                                            value="{{ $keluarga->kewarganegaraan_wali }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Pendidikan Wali</label>
                                                        <input type="text" class="form-control" name="pendidikan_wali" placeholder="Pendidikan Wali"
                                                            value="{{ $keluarga->pendidikan_wali }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Pekerjaan Wali</label>
                                                        <input type="text" class="form-control" name="pekerjaan_wali" placeholder="Pekerjaan Wali"
                                                            value="{{ $keluarga->pekerjaan_wali }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Penghasilan Wali</label>
                                                        <input type="text" class="form-control" name="penghasilan_bln_wali" placeholder="Penghasilan Wali"
                                                            value="{{ $keluarga->penghasilan_bln_wali }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Alamat Wali</label>
                                                        <input type="text" class="form-control" name="alamat_wali" placeholder="Alamat Wali"
                                                            value="{{ $keluarga->alamat_wali }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Telepon Wali</label>
                                                        <input type="text" class="form-control" name="tlp_wali" placeholder="Telepon Wali"
                                                            value="{{ $keluarga->tlp_wali }}">
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Keterangan Wali</label>
                                                        <input type="text" class="form-control" name="ket_wali" placeholder="Keterangan Wali"
                                                            value="{{ $keluarga->ket_wali }}">
                                                    </div>
                                                </div>

                
                                                <div class="col-xs-12">
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    @else
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-lg-12 col-xs-12 col-md-12">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-check"></i> Selamat datang kembali!</h4>
                        Pada halaman dashboard anda dapat melihat beberapa informasi mengenai website anda.
                    </div>
                </div>
                  <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                      <div class="inner">
                        <h3>{{$siswas}}</h3>
            
                        <p>Jumlah Siswa</p>
                      </div>
                      <div class="icon">
                        <i class="fa fa-users"></i>
                      </div>
                      <a href="#" class="small-box-footer">
                        <i class="fa fa-arrow-circle-right"></i>
                      </a>
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-blue">
                      <div class="inner">
                        <h3>{{$mm}}</h3>
            
                        <p>Jumlah Siswa Jurusan MM</p>
                      </div>
                      <div class="icon">
                        <i class="fa fa-users"></i>
                      </div>
                      <a href="{{url('admin/pm')}}" class="small-box-footer">
                        <i class="fa fa-arrow-circle-right"></i>
                      </a>
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                      <div class="inner">
                        <h3>{{$rpl}}</h3>
            
                        <p>Jumlah Siswa Jurusan RPL</p>
                      </div>
                      <div class="icon">
                        <i class="fa fa-users"></i>
                      </div>
                      <a href="{{url('admin/video')}}" class="small-box-footer">
                        <i class="fa fa-arrow-circle-right"></i>
                      </a>
                    </div>
                  </div>
                  <!-- ./col -->
                  <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                      <div class="inner">
                        <h3>{{$tkj}}</h3>
            
                        <p>Jumlah Siswa Jurusan TKJ</p>
                      </div>
                      <div class="icon">
                        <i class="fa fa-users"></i>
                      </div>
                      <a href="{{url('admin/buku')}}" class="small-box-footer">
                        <i class="fa fa-arrow-circle-right"></i>
                      </a>
                    </div>
                </div>

                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-gray">
                      <div class="inner">
                        <h3>{{$an}}</h3>
            
                        <p>Jumlah Siswa Jurusan AN</p>
                      </div>
                      <div class="icon">
                        <i class="fa fa-users"></i>
                      </div>
                      <a href="{{url('admin/buku')}}" class="small-box-footer">
                        <i class="fa fa-arrow-circle-right"></i>
                      </a>
                    </div>
                </div>

                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                      <div class="inner">
                        <h3>{{$dkv}}</h3>
            
                        <p>Jumlah Siswa Jurusan DKV</p>
                      </div>
                      <div class="icon">
                        <i class="fa fa-users"></i>
                      </div>
                      <a href="{{url('admin/buku')}}" class="small-box-footer">
                        <i class="fa fa-arrow-circle-right"></i>
                      </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <!-- DONUT CHART -->
                    <div class="box box-danger">
                        <div class="box-header with-border">
                        <h3 class="box-title">Grafik Jenis Kelamin</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                        </div>
                        <div class="box-body">
                            <canvas id="jenisKelaminChart" style="height:250px"></canvas>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-xs-6">
                    <!-- DONUT CHART -->
                    <div class="box box-danger">
                        <div class="box-header with-border">
                        <h3 class="box-title">Grafik Agama</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                        </div>
                        <div class="box-body">
                            <canvas id="agamaChart" style="height:250px"></canvas>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    @endif
@endsection
@section('js')
<script src="{{ asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('backend/bower_components/chart.js/Chart.js') }}"></script>
<script type="text/javascript">
    $(function(){
        $('#tabelKehadiran').dataTable()
    });
    $('#tanggalLahir').datepicker({
        autoclose: true
    });
    $('#tanggalLahirAyah').datepicker({
        autoclose: true
    });
    $('#tanggalLahirIbu').datepicker({
        autoclose: true
    });
    $('#tgl_lulus').datepicker({
        autoclose: true
    });
    $('#tgl_diterima').datepicker({
        autoclose: true
    });

    $(function () {
        // Chart Jenis Kelamin
    var pieChartColor = [
        '#f56954', '#3c8dbc', '#00a65a', '#f39c12', '#00c0ef', '#d2d6de'
    ]
    var dataJenisKelamin = @json($dataJenisKelamin);

    jenKelData = [];
    indexColorJenisKelamin = 0;
    Object.entries(dataJenisKelamin).forEach(([key, val]) => {
        jenKelData.push({
            value    : val,
            color    : pieChartColor[indexColorJenisKelamin],
            highlight: pieChartColor[indexColorJenisKelamin++],
            label    : key
        })
    });

    var jenisKelaminChartCanvas = $('#jenisKelaminChart').get(0).getContext('2d')
    var jenisKelaminChart       = new Chart(jenisKelaminChartCanvas)
    var jenisKelaminChartData   = jenKelData
    // Chart Jenis Kelamin

    // Chart Agama
    var dataAgama = @json($dataAgama);
    agamaData = [];
    indexColorAgama = 0;
    Object.entries(dataAgama).forEach(([key, val]) => {
        agamaData.push({
            value    : val,
            color    : pieChartColor[indexColorAgama],
            highlight: pieChartColor[indexColorAgama++],
            label    : key
        })
    });
    var agamaChartCanvas = $('#agamaChart').get(0).getContext('2d')
    var agamaChart       = new Chart(agamaChartCanvas)
    var agamaChartData   = agamaData
    // Chart Agama
    

    var pieOptions     = {
      segmentShowStroke    : true,
      segmentStrokeColor   : '#fff',
      segmentStrokeWidth   : 2,
      percentageInnerCutout: 50,
      animationSteps       : 100,
      animationEasing      : 'easeOutBounce',
      animateRotate        : true,
      animateScale         : false,
      responsive           : true,
      maintainAspectRatio  : true,
      legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
    
    jenisKelaminChart.Doughnut(jenisKelaminChartData, pieOptions)
    agamaChart.Doughnut(agamaChartData, pieOptions)
  })
</script>
@endsection
