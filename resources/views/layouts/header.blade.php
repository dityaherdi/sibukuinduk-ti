<header class="main-header">
    @php
        $us = \App\User::where('id_user',Auth::user()->id_user)->first();
    @endphp
    <a href="index2.html" class="logo">
        <span class="logo-mini"><b>L</b>BI</span>
        <span class="logo-lg"><b>Buku</b>Induk</span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if ($us->avatar == null)
                            <img src="{{asset('images/avatar5.png')}}" class="user-image" alt="User Image">
                        @else
                            <img src="{{asset('images/avatar/'.$us->avatar)}}" class="user-image" alt="User Image">
                        @endif
                        <span class="hidden-xs">{{$us->nama}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            @if ($us->avatar == null)
                                <img src="{{asset('images/avatar5.png')}}" class="img-circle" alt="User Image">
                            @else
                                <img src="{{asset('images/avatar/'.$us->avatar)}}" class="img-circle" alt="User Image">
                            @endif
                            <p>
                                {{$us->nama}}
                                <small>
                                    @if ($us->level == 1)
                                    Kaur Kesiswaan
                                    @elseif ($us->level == 2)
                                    Bimbingan Konseling
                                    @elseif ($us->level == 3)
                                    Siswa
                                    @elseif($us->level == 4)
                                    Kepala Sekolah
                                    @endif
                                </small>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{url('profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Sign out</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
  