<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                @if (Auth::user()->avatar == null)
                    <img src="{{asset('images/avatar5.png')}}" class="img-circle" alt="User Image">
                @else
                    <img src="{{asset('images/avatar/'.Auth::user()->avatar)}}" class="img-circle" alt="User Image">
                @endif
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->nama}}</p>
                <a><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            
            @if (Auth::user()->level == 1)
                <li class="header">DASHBOARD NAVIGATION</li>
                <li class="{{$activeMenu == 'dashboard' ? 'active' : ''}}"><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li class="header">IMPORT NAVIGATION</li>
                <li class="{{$activeMenu == 'users' ? 'active' : ''}}"><a href="{{url('admin/user')}}"><i class="fa fa-users"></i> <span>Data Pengguna</span></a></li>
                <li class="{{$activeMenu == 'jenis_rombel' ? 'active' : ''}}"><a href="{{url('admin/jenis-rombel')}}"><i class="fa fa-database"></i> <span>Data Jenis Rombel</span></a></li>
                <li class="{{$activeMenu == 'rombel' ? 'active' : ''}}"><a href="{{url('admin/rombel')}}"><i class="fa fa-desktop"></i> <span>Data Rombel</span></a></li>
                <li class="{{$activeMenu == 'masters' ? 'active' : ''}}"><a href="{{url('admin/master')}}"><i class="fa fa-gear"></i> <span>Import Nilai</span></a></li>
                
                <li class="header">MAIN NAVIGATION</li>
                <li class="{{$activeMenu == 'kelas' ? 'active' : ''}}"><a href="{{url('admin/kelas')}}"><i class="fa fa-cogs"></i> <span>Data Kelas</span></a></li>
                <li class="{{$activeMenu == 'data-pribadi' ? 'active' : ''}}"><a href="{{url('admin/data-pribadi')}}"><i class="fa fa-users"></i> <span>Data Pribadi Siswa</span></a></li>
                <li class="treeview {{$activeMenu == 'mapel' ? 'active' : ''}}">
                    <a href="#">
                        <i class="fa fa-book"></i>
                        <span>Mata Pelajaran</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('admin/mata-pelajaran/tambah')}}"><i class="fa fa-circle-o"></i> Tambah Mata Pelajaran</a></li>
                        <li><a href="{{url('admin/mata-pelajaran')}}"><i class="fa fa-circle-o"></i> Data Mata Pelajaran</a></li>
                    </ul>
                </li>

                
                <li class="treeview {{$activeMenu == 'ekstra' ? 'active' : ''}}">
                    <a href="#">
                        <i class="fa fa-external-link"></i>
                        <span>Data Ekstra</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('admin/ekstra/tambah')}}"><i class="fa fa-circle-o"></i> Tambah Ekstra</a></li>
                        <li><a href="{{url('admin/ekstra')}}"><i class="fa fa-circle-o"></i> Data Ekstra</a></li>
                    </ul>
                </li>
                <li class="{{$activeMenu == 'kehadiran' ? 'active' : ''}}"><a href="{{url('admin/kehadiran')}}"><i class="fa fa-bar-chart"></i> <span>Data Kehadiran</span></a></li>
                {{-- <li class="{{$activeMenu == 'keluarga' ? 'active' : ''}}"><a href="{{url('admin/keluarga')}}"><i class="fa fa-bar-chart"></i> <span>Data Keluarga Siswa</span></a></li> --}}
                {{-- <li class="{{$activeMenu == 'perkembangan' ? 'active' : ''}}"><a href="{{url('admin/perkembangan')}}"><i class="fa fa-bar-chart"></i> <span>Data Perkembangan Siswa</span></a></li>
                <li class="{{$activeMenu == 'setelah_pendidikan' ? 'active' : ''}}"><a href="{{url('admin/setelah-pendidikan')}}"><i class="fa fa-bar-chart"></i> <span>Data Setelah Pendidikan</span></a></li> --}}
                <li class="header">NILAI NAVIGATION</li>
                <li class="{{$activeMenu == 'nilai_ekstra' ? 'active' : ''}}"><a href="{{url('admin/nilai-ekstra')}}"><i class="fa fa-sort-numeric-asc"></i> <span>Data Nilai Ekstra</span></a></li>
                <li class="{{$activeMenu == 'nilai' ? 'active' : ''}}"><a href="{{url('admin/nilai')}}"><i class="fa fa-sort-numeric-asc"></i> <span>Data Nilai</span></a></li>
                <li class="treeview {{$activeMenu == 'buku-induk' ? 'active' : ''}}">
                    <a href="#">
                        <i class="fa fa-file-pdf-o"></i>
                        <span>Buku Induk Siswa</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('admin/tabel-buku-induk')}}"><i class="fa fa-circle-o"></i> Tabel Buku Induk</a></li>
                        <li><a href="{{url('admin/lembar-buku-induk')}}"><i class="fa fa-circle-o"></i> Lembar Buku Induk</a></li>
                        <li><a href="{{url('admin/laporan-hasil-belajar')}}"><i class="fa fa-circle-o"></i> Laporan Hasil Belajar</a></li>
                    </ul>
                </li>
                <li class="{{$activeMenu == 'ijazah' ? 'active' : ''}}"><a href="{{url('admin/ijazah')}}"><i class="fa fa-file-pdf-o"></i> <span>Ijazah</span></a></li>
            @elseif(Auth::user()->level == 2)
                <li class="header">DASHBOARD NAVIGATION</li>
                <li class="{{$activeMenu == 'dashboard' ? 'active' : ''}}"><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li class="header">MAIN NAVIGATION</li>
                <li class="{{$activeMenu == 'users' ? 'active' : ''}}"><a href="{{url('admin/user')}}"><i class="fa fa-users"></i> <span>Daftar User</span></a></li>
                <li class="{{$activeMenu == 'data-pribadi' ? 'active' : ''}}"><a href="{{url('admin/data-pribadi')}}"><i class="fa fa-users"></i> <span>Data Pribadi Siswa</span></a></li>
                <li class="{{$activeMenu == 'buku-induk' ? 'active' : ''}}"><a href="{{url('admin/lembar-buku-induk')}}"><i class="fa fa-file-pdf-o"></i> <span>Lembar Buku Induk</span></a></li>
            @elseif(Auth::user()->level == 3)
                <li class="header">NAVIGATION</li>
                <li class="{{$activeMenu == 'dashboard' ? 'active' : ''}}"><a href="{{url('home')}}"><i class="fa fa-users"></i> <span>Data Pribadi Siswa</span></a></li>
            @elseif(Auth::user()->level == 4)
                <li class="header">DASHBOARD NAVIGATION</li>
                <li class="{{$activeMenu == 'dashboard' ? 'active' : ''}}"><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                <li class="header">MAIN NAVIGATION</li>
                <li class="{{$activeMenu == 'data-pribadi' ? 'active' : ''}}"><a href="{{url('admin/data-pribadi')}}"><i class="fa fa-users"></i> <span>Data Pribadi Siswa</span></a></li>
                <li class="{{$activeMenu == 'kehadiran' ? 'active' : ''}}"><a href="{{url('admin/kehadiran')}}"><i class="fa fa-bar-chart"></i> <span>Data Kehadiran</span></a></li>
                <li class="{{$activeMenu == 'nilai_ekstra' ? 'active' : ''}}"><a href="{{url('admin/nilai-ekstra')}}"><i class="fa fa-sort-numeric-asc"></i> <span>Data Nilai Ekstra</span></a></li>
                <li class="{{$activeMenu == 'nilai' ? 'active' : ''}}"><a href="{{url('admin/nilai')}}"><i class="fa fa-sort-numeric-asc"></i> <span>Data Nilai</span></a></li>
            @endif
        </ul>
    </section>
</aside>
  