@extends('layouts.master',['activeMenu' => 'siswa'])
@section('title','Daftar Siswa')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Siswa
        <small>Data Pribadi Siswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Data Pribadi Siswa</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <form action="#" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Nama Lengkap</label>
                                        <input type="text" class="form-control" name="nama_siswa_lengkap" placeholder="Masukkan Nama Lengkap"
                                            value="{{ old('nama_siswa_lengkap') }}">
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jenis Kelamin</label>
                                        <select name="jenis_kelamin" class="form-control" value="{{ old('jenis_kelamin') }}">
                                            <option value="Laki-laki">Laki-laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12" id="ttl">
                                    <label class="col-form-label">Tempat & Tanggal Lahir</label>
                                    <div class="form-inline">
                                        <input type="text" class="form-control" name="tempatLahir" placeholder="Tempat" value="{{ old('tempatLahir') }}">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="tanggalLahir" id="tanggalLahir" value="{{ old('tanggalLahir') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12" style="margin-top: 15px">
                                    <div class="form-group">
                                        <label class="col-form-label">Anak Ke</label>
                                        <input type="number" min="0" class="form-control" name="anak_keberapa" value="{{ old('anak_keberapa') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jumlah Saudara Kandung</label>
                                        <input type="number" min="0" class="form-control" name="jum_saudara_kandung" value="{{ old('jum_saudara_kandung') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jumlah Saudara Tiri</label>
                                        <input type="number" min="0" class="form-control" name="jum_saudara_tiri" value="{{ old('jum_saudara_tiri') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jumlah Saudara Angkat</label>
                                        <input type="number" min="0" class="form-control" name="jum_saudara_angkat" value="{{ old('jum_saudara_angkat') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Status Anak</label>
                                        <select name="status_anak" id="status_anak" class="form-control" value="{{ old('status_anak') }}">
                                            <option value="kandung">Kandung</option>
                                            <option value="tiri">Tiri</option>
                                            <option value="angkat">Angkat</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Golongan Darah</label>
                                        <select name="golongan_darah" id="golongan_darah" class="form-control" value="{{ old('golongan_darah') }}">
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="O">O</option>
                                            <option value="AB">AB</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Tinggi Badan (Sentimeter)</label>
                                        <input type="number" min="0" class="form-control" name="tinggi" value="{{ old('tinggi') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Berat Badan (Kilogram)</label>
                                        <input type="number" min="0" class="form-control" min="0" name="berat_badan" value="{{ old('berat_badan') }}">
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Penyakit Yang Pernah Diderita</label> <br>
                                    <textarea style="width: 100%" class="form-control" name="penyakit_yang_pernah_diderita" rows="3" id="penyakit_yang_pernah_diderita">{{ old('penyakit_yang_pernah_diderita') }}
                                    </textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Kelainan Jasmani</label> <br>
                                        <textarea style="width: 100%" class="form-control" name="kelainan_jasmani" rows="3" id="kelainan_jasmani">{{ old('kelainan_jasmani') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Alasan Pindah</label> <br>
                                        <textarea style="width: 100%" class="form-control" name="alasan" rows="3" id="alasan">{{ old('alasan') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Kesenian</label> <br>
                                        <textarea style="width: 100%" class="form-control" name="kesenian" rows="3" id="kesenian">{{ old('kesenian') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Olahraga</label> <br>
                                        <textarea style="width: 100%" class="form-control" name="olahraga" rows="3" id="olahraga">{{ old('olahraga') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input type="file" id="fotoSiswa" name="fotoSiswa">
                                    </div>
                                </div>
                               
                            </div>

                            <div class="col-xs-6">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Nama Panggilan</label>
                                        <input type="text" class="form-control" name="nama_siswa_panggilan" placeholder="Masukkan Nama Panggilan"
                                        value="{{ old('nama_siswa_panggilan') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Agama</label>
                                        <select name="agama" id="agama" class="form-control" value="{{ old('agama') }}">
                                            <option value="buddha">Buddha</option>
                                            <option value="hindu">Hindu</option>
                                            <option value="islam">Islam</option>
                                            <option value="katolik">Katolik</option>
                                            <option value="protestan">Protestan</option>
                                            <option value="konghucu">Konghucu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12" id="kewarganegaraan">
                                    <div class="form-group">
                                        <label class="col-form-label">Kewarganegaraan</label>
                                        <input type="text" class="form-control" name="kewarganegaraan" placeholder="Masukkan kewarganegaraan" value="{{ old('kewarganegaraan') }}">
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Bahasa Sehari-hari</label>
                                        <input type="text" class="form-control" name="bahasa_seharihari" value="{{ old('bahasa_seharihari') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Alamat</label>
                                        <input type="text" class="form-control" name="alamat" value="{{ old('alamat') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Telepon</label>
                                        <input type="text" class="form-control" name="no_tlp" value="{{ old('no_tlp') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Tinggal Bersama</label>
                                        <input type="text" class="form-control" name="tinggal_dgn" value="{{ old('tinggal_dgn') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jarak Kesekolah (Kilometer)</label>
                                        <input type="number" min="0" class="form-control" name="jarak_kesekolah" value="{{ old('jarak_kesekolah') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Lulusan Dari</label>
                                        <input type="text" class="form-control" min="0" name="lulusan_dari" value="{{ old('lulusan_dari') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Tanggal Lulus</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="tgl_lulus" name="tgl_lulus" value="{{ old('tgl_lulus') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">No. STTB</label>
                                        <input type="text" class="form-control" name="no_sttb" value="{{ old('no_sttb') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Lama Belajar (Tahun)</label>
                                        <input type="number" min="0" class="form-control" name="lama_belajar" value="{{ old('lama_belajar') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Pindahan Sekolah</label>
                                        <input type="text" class="form-control" name="pindahan_sekolah" value="{{ old('pindahan_sekolah') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Diterima Kelas</label>
                                        <select name="diterima_kls" id="diterima_kls" class="form-control" value="{{ old('diterima_kls') }}">
                                            <option value="X">X</option>
                                            <option value="XI">XI</option>
                                            <option value="XII">XII</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Kelompok</label>
                                        <input type="text" class="form-control" name="kelompok" value="{{ old('kelompok') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jurusan</label>
                                        <input type="text" class="form-control" name="jurusan" value="{{ old('jurusan') }}">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Tanggal Diterima</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="tgl_diterima" name="tgl_diterima" value="{{ old('tgl_diterima') }}">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Lainya</label> <br>
                                        <textarea style="width: 100%" class="form-control" name="lainya" rows="3" id="lainya">{{ old('lainya') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Kemasyarakatan</label> <br>
                                        <textarea style="width: 100%" class="form-control" name="kemasyarakatan" rows="3" id="kemasyarakatan">{{ old('kemasyarakatan') }}</textarea>
                                    </div>
                                </div>
                                

                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{ asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelKehadiran').dataTable()
        });
        $('#tanggalLahir').datepicker({
            autoclose: true
        });
        $('#tgl_lulus').datepicker({
            autoclose: true
        });
        $('#tgl_diterima').datepicker({
            autoclose: true
        });
    </script>
@endsection
