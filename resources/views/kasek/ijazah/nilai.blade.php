@extends('layouts.master',['activeMenu' => 'Ijazah'])
@section('title','Daftar Ijazah Siswa')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Nilai Ijazah Siswa
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Nilai Ijazah Siswa {{$siswa->nis}} - {{$siswa->nama}}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <h4>Nama</h4>
                            <h4>NIS</h4>
                        </div>
                        <div class="col-md-4">
                            <h4>: {{ $siswa->nama }}</h4>
                            <h4>: {{ $siswa->nis }}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <form action="{{url('admin/ijazah/nilai/insert')}}" method="POST">
                            {{csrf_field()}}
                            <input type="hidden" name="nis" value="{{ $siswa->nis }}">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Nama Mata Pelajaran</label>
                                    <select name="id_data_mata_pelajaran" id="kodeMapel" value="{{old('kode_mapel')}}" style="width: 100%">
                                        @foreach ($mapels as $mapel)
                                            <option value="{{$mapel->kode_mapel}}">{{$mapel->nama_mp}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Nilai rata-rata raport</label>
                                    <input type="text" class="form-control" name="nilai_rata_rata" value="" placeholder="Masukan Nilai rata-rata">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Nilai Ujian</label>
                                    <input type="text" class="form-control" name="nilai_ujian" value="" placeholder="Masukan Nilai Ujian">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <input type="submit" class="btn btn-primary btn-md" name="submit" id="" value="Submit" style="margin-top: 25px">
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>*Jika ingin update nilai ijazah, input ulang dengan memilih mata pelajaran dan nilai yang telah ditentukan</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelIjazah" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    @if (Auth::user()->level == 4)
                                        
                                    @else
                                    <th>Opsi</th>
                                    @endif
                                    <th>Mata Pelajaran</th>
                                    <th>Nilai Rata-rata Rapot</th>
                                    <th>Nilai Ujian</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($nilaiIjazah as $nilai)
                                    <tr>
                                        @if (Auth::user()->level == 4)

                                        @else
                                        <td>
                                            <a href="{{url('admin/ijazah/nilai/hapus/'.$nilai->id_data_nilai_ijazah.'/'.$siswa->nis)}}" class="btn btn-danger btn-md">
                                                Hapus
                                            </a>
                                        </td>
                                        @endif
                                        <td>{{$nilai->nama_mp}}</td>
                                        <td>{{$nilai->nilai_rata_rata}}</td>
                                        <td>{{$nilai->nilai_ujian}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#kodeMapel').select2()
            $('#tabelIjazah').dataTable()
        });
    </script>
@endsection
