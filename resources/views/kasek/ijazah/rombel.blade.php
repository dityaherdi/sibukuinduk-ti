@extends('layouts.master',['activeMenu' => 'ijazah'])
@section('title','Daftar Rombel')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Rombel
        <small>Daftar Rombel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Rombel</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelRombel" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Jenis Rombel</th>
                                    <th>Tahun Ajaran</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($rombels as $rombel)
                                    <tr>
                                        <td>
                                            <a href="{{url('admin/data-ijazah/'.$rombel->id_data_jenis_rombel)}}" class="btn btn-primary btn-md">
                                                <i class="fa fa-arrow-right"></i>
                                                Data Ijazah Siswa
                                            </a>
                                        </td>
                                        <td>{{ $rombel->nama_rombel}}</td>
                                        <td>{{ $rombel->thn_ajaran }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $('#jenRombel').select2()
        $('#siswaAll').select2()
        $(function(){
            $('#tabelRombel').dataTable()
        });
    </script>
@endsection
