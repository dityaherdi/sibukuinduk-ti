@extends('layouts.master',['activeMenu' => 'masters'])
@section('title','Daftar User')
@section('css')

@endsection
@section('content')
<section class="content-header">
    <h1>
        Data Ledger
        <small>Import Data Ledger</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Import Data Ledger</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="box">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Import Data Ledger Siswa</h3>
                    </div>
                    <form action="{{ url('admin/utils/import') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                {{-- <label class="custom-file-input file-blue"> --}}
                                    <input type="file" name="fileExcel" class="form-control">
                                {{-- </label> --}}
                                
                            </div>
                            <small>Import nilai dalam format excel disini.</small>
                        </div>
            
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    
@endsection
