@extends('layouts.master',['activeMenu' => 'users'])
@section('title','Daftar User')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        User
        <small>Daftar User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar User</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <a href="javascript:void(0);" class="btn btn-primary btn-md" style="margin-bottom: 5px" data-target="#tambahUserModal" data-toggle="modal">
                        <i class="fa fa-plus"></i>
                        Tambah User Baru
                    </a>
                    <a href="javascript:void(0);" class="btn btn-success btn-md" style="margin-bottom: 5px" data-target="#importUserModal" data-toggle="modal">
                        <i class="fa fa-upload"></i>
                        Import Excel
                    </a>
                    <a href="javascript:void(0);" class="btn btn-danger btn-md" style="margin-bottom: 5px" onclick="resetDatabase()">
                        <i class="fa fa-refresh"></i>
                        Reset Database
                    </a>
                    <!-- BOOK CREATE MODAL -->
                    <div class="modal fade" id="tambahUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel">Tambah User Baru</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{url('admin/user/tambah')}}" method="post" name="formUser" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="length" value="6">
                                    <div class="modal-body">
                                        <div class="row">
                                            @if (Auth::user()->level == 1)
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Level</label>
                                                        <select name="level" id="level" class="form-control" value="{{old('level')}}">
                                                            <option value="1">Kaur Kesiswaan</option>
                                                            <option value="2">Bimbingan Konseling</option>
                                                            <option value="3">Siswa</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" id="nis" style="display: none">
                                                    <div class="form-group">
                                                        <label class="col-form-label">NIS</label>
                                                        <input type="text" class="form-control" name="nis" placeholder="Masukkan nis" value="{{old('nis')}}">
                                                    </div>
                                                </div>
                                            @elseif(Auth::user()->level == 2)
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">NIS</label>
                                                        <input type="text" class="form-control" name="nis" placeholder="Masukkan nis" value="{{old('nis')}}">
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Nama</label>
                                                    <input type="text" class="form-control" name="nama" placeholder="Masukan nama" value="{{ old('nama') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Username</label>
                                                    <input type="text" class="form-control" name="username" placeholder="Masukan Username" value="{{ old('username') }}">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="col-md-9">
                                                    <label class="col-form-label">Password</label>
                                                    <input type="text" class="form-control" name="password">
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="col-form-label"></label>
                                                    <button type="button" class="btn btn-success btn-md" onclick="generate();" style="margin-top: 20%">
                                                        <i class="fa fa-refresh"></i>
                                                        Generate
                                                    </button>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Status</label>
                                                    <select name="status" class="form-control" value="{{old('status')}}">
                                                        <option value="1">Aktif</option>
                                                        <option value="0">Tidak Aktif</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                        <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- IMPORT USER MODAL --}}
                    <div class="modal fade" id="importUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel">Tambah User Baru</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{url('admin/user/import')}}" method="post" name="importUser" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="length" value="6">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Pilih file excel</label>
                                                    <input type="file" name="userExcel" id="userExcel">
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Pastikan format Excel sesuai contoh di bawah ini</label>
                                                </div>
                                                <table class="table table-bordered table-striped">
                                                    <tr>
                                                        <th>NIS</th>
                                                        <th>NAMA</th>
                                                        <th>USERNAME</th>
                                                        <th>LEVEL</th>
                                                        <th>STATUS</th>
                                                    </tr>
                                                    <tr>
                                                        <td>9999</td>
                                                        <td>Admin Buku Induk</td>
                                                        <td>admin</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                        <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tabelUser" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>NIS</th>
                                    <th>Level</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($users as $user)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Option <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="javascript:void(0);" onclick="updatePassword('{{$user->id_user}}')">Change Password</a></li>

                                                    <li class="divider"></li>
                                                    @if ($user->status == 1)
                                                        <li><a href="javascript:void(0);" onclick="nonaktifStatus('{{$user->id_user}}')">Non Aktifkan User</a></li>
                                                    @else
                                                        <li><a href="javascript:void(0);" onclick="aktifStatus('{{$user->id_user}}')">Aktifkan User</a></li>
                                                    @endif
                                                    
                                                </ul>
                                            </div>
                                        </td>
                                        <td>{{$user->nama}}</td>
                                        <td>{{$user->username}}</td>
                                        <td>
                                            @if($user->nis == null)
                                                -
                                            @else
                                                {{$user->nis}}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($user->level == 1)
                                                <span class="label label-info">Kaur Kesiswaan</span>
                                            @elseif ($user->level == 2)
                                                <span class="label label-info">Bimbingan Konseling</span>
                                            @elseif($user->level == 4)
                                                <span class="label label-info">Kepala Sekolah</span>
                                            @elseif ($user->level == 3)
                                                <span class="label label-success">Siswa</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($user->status == 1)
                                                <span class="label label-success">Aktif</span>
                                            @else
                                                <span class="label label-warning">Non Aktif</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<form class="hidden" action="" method="post" id="formDelete">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="delete">
</form>
<form class="hidden" action="" method="post" id="formActive">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
</form>
<form class="hidden" action="" method="post" id="formInnactive">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
</form>
<form class="hidden" action="" method="post" id="formPassword">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="password" value="" id="password">
</form>
<form class="hidden" action="" id="formReset">

</form>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        function resetDatabase(){
            swal({
                title: "Reset database saat ini?",
                text: "Database akan di reset seperti semula!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willReset) => {
                if (willReset) {
                    swal("Berhasil! Database berhasil di reset!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formReset').attr('action', '{{url('admin/reset')}}');
                        $('#formReset').submit();
                    }); 
                }
            });
        }
        $('#level').on('change', function(){
            var val = this.value;
            $('#nis').hide();
            if (val == 3) {
                console.log('nis');
                $('#nis').show();
            }else{
                $('#nis').hide();
            }
        });
        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function generate() {
            formUser.password.value = randomPassword(formUser.length.value);
        }
        function deleteUser(id){
            swal({
                title: "Anda yakin?",
                text: "User akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! User yang anda pilih berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/user')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
        function nonaktifStatus(id){
            swal({
                title: "Anda yakin?",
                text: "User akan dinonaktifkan, user dapat kembali diaktifkan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willInnactive) => {
                if (willInnactive) {
                    swal("Berhasil! User yang anda pilih berhasil dinonaktifkan!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formInnactive').attr('action', '{{url('admin/user/nonaktif')}}/'+id);
                        $('#formInnactive').submit();
                    }); 
                }
            });
        }
        function aktifStatus(id){
            swal({
                title: "Anda yakin?",
                text: "User akan diaktifkan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willActive) => {
                if (willActive) {
                    swal("Berhasil! User yang anda pilih berhasil diaktifkan!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formActive').attr('action', '{{url('admin/user/aktif')}}/'+id);
                        $('#formActive').submit();
                    }); 
                }
            });
        }
        function updatePassword(id){
            bootbox.prompt({
                title: 'Masukan password.',
                inputType: 'password',
                size: 'small',
                callback: function(result){
                    if (result != null ) {
                        bootbox.prompt({
                            title: 'Masukan kembali password anda.',
                            inputType: 'password',
                            size: 'small',
                            callback: function(password){
                                if (password != null) {
                                    if (result == password) {
                                        $('#password').val(password);
                                        $('#formPassword').attr('action', '{{url('admin/user/password')}}/'+id);
                                        $('#formPassword').submit();
                                    }else {
                                        bootbox.alert("Password tidak sama. Silakan ulangi !");
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
        $(function(){
            $('#tabelUser').dataTable()
        });
    </script>
@endsection
