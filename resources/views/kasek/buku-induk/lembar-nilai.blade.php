<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('backend/dist/css/AdminLTE.min.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
      @media print {
        * { overflow: visible !important; }
    }
  </style>
</head>
<body onload="window.print();">
{{-- <body> --}}
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> SMK TI BALI GLOBAL DENPASAR
          <small class="pull-right">LAPORAN HASIL BELAJAR SISWA</small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-xs-6">
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th style="width:50%">NOMOR INDUK</th>
                    <td>{{ $siswa->nis }}</td>
                </tr>
                <tr>
                    <th style="width:50%">NAMA PESERTA DIDIK</th>
                    <td>{{ $siswa->nama }}</td>
                </tr>
            </table>
          </div>
      </div>
      <!-- /.col -->
      <div class="col-xs-6">
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th>KELAS</th>
                    <td>{{ $rombel->nama_rombel }}</td>
                </tr>
                <tr>
                    <th>SEMESTER</th>
                    <td>{{ $semester }}</td>
                </tr>
            </table>
          </div>
      </div>
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
        <p class="lead">NILAI AKADEMIK</p>
          @php
              $no = 1;
          @endphp
        <table class="table table-striped">
          <thead>
          <tr>
            <th rowspan="2" style="vertical-align: middle;">No</th>
            <th rowspan="2" style="vertical-align: middle;">Mata Pelajarn</th>
            <th rowspan="2" style="vertical-align: middle;">KKM</th>
            <th>Nilai Hasil Belajar</th>
          </tr>
          <tr>
            <th>Pengetahuan</th>
            <th>Keterampilan</th>
            <th>Sikap</th>
          </tr>
          </thead>
          <tbody>
              @foreach ($nilai as $item)
                <tr>
                    <td>{{ $no++ }}</td>
                    <td>{{ $item['nama_mapel'] }}</td>
                    <td>{{ $item['kkm'] }}</td>
                    <td>{{ $item['pengetahuan'] }}</td>
                    <td>{{ $item['keterampilan'] }}</td>
                    <td>{{ $item['sikap'] }}</td>
                </tr>
              @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <div class="col-xs-12 table-responsive">
          @php
              $no = 1;
          @endphp
            <p class="lead">EKSTRAKURIKULER</p>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>NO</td>
                        <td>KEGIATAN</td>
                        <td>DESKRIPSI</td>
                        <td>NILAI</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($ekstras as $item)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $item->ekstras['nama_ekstra'] }}</td>
                            <td>{{ $item->ekstras['deskripsi_ekstra'] }}</td>
                            <td>{{ $item->nilai_siswa }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <!-- /.col -->
      <div class="col-xs-12">
        <p class="lead">KETIDAKHADIRAN</p>

        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Sakit:</th>
              <td>{{ $kehadiran->total_sakit }} HARI</td>
            </tr>
            <tr>
              <th>Ijin:</th>
              <td>{{ $kehadiran->total_ijin }} HARI</td>
            </tr>
            <tr>
              <th>Tanpa Keterangan:</th>
              <td>{{ $kehadiran->total_tanpaket }} HARI</td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
