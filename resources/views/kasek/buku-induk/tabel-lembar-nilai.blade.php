@extends('layouts.master',['activeMenu' => 'siswa'])
@section('title','Daftar Siswa')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Hasil Belajar Siswa
        <small>Daftar Hasil Belajar</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Hasil Belajar</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {{-- <a href="{{ route('create:siswa') }}" class="btn btn-primary btn-md" style="margin-bottom: 5px">
                        <i class="fa fa-plus"></i>
                        Tambah Data Siswa
                    </a> --}}
                    <div class="table-responsive">
                        <div class="form-group form-inline">
                            <select class="form-control input-sm col-sm-6" id="rombelField" style="margin-right: 5px">
                              <option value="">Rombel</option>
                              @foreach ($rombels as $item)
                                <option value="{{ $item->id_data_jenis_rombel }}">{{ $item->nama_rombel }}</option>  
                              @endforeach
                            </select>
                            <input id="semesterField" value="" class="form-control input-sm" type="number" placeholder="Semester" style="max-width: 90px" min="1">
                          </div>
                        <table id="tabelKehadiran" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Nama</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Tempat Tanggal Lahir</th>
                                    <th>Alamat</th>
                                    <th>Telepon</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($siswas as $siswa)
                                    <tr>
                                        <td>
                                            <button onclick="lembarNilaiPdf({{ $siswa->id_user }})" class="btn btn-primary btn-sm">
                                                <i class="fa fa-print"></i>
                                            </button>
                                        </td>
                                        <td>{{$siswa->nama_siswa_lengkap}}</td>
                                        <td>{{$siswa->jenis_kelamin}}</td>
                                        <td>{{$siswa->ttgl_lahir}}</td>
                                        <td>{{$siswa->alamat}}</td>
                                        <td>{{$siswa->no_tlp}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- <form class="hidden" action="" method="post" id="formDelete">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="delete">
</form> --}}
@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelKehadiran').dataTable()


        });
        function lembarNilaiPdf(id) {
            var rombel = $('#rombelField').val()
            var semester = $('#semesterField').val()
            
            if (rombel.length === 0 || semester.length === 0) {
                swal({
                    title: "Perhatian!",
                    text: "Rombel dan Semester harus diisi!",
                    icon: "warning",
                })
            } else {
                window.open(`export-lembar-nilai/${id}/${rombel}/${semester}`, '_blank');
            }

        }
        // function deleteSiswa(id){
        //     swal({
        //         title: "Anda yakin?",
        //         text: "Siswa akan terhapus secara permanen!",
        //         icon: "warning",
        //         buttons: true,
        //         dangerMode: true,
        //     })
        //     .then((willDelete) => {
        //         if (willDelete) {
        //             swal("Berhasil! Siswa yang anda pilih berhasil terhapus!", {
        //                 icon: "success",
        //             }).then((res) => {
        //                 $('#formDelete').attr('action', '{{url('admin/siswa/delete/')}}/'+id);
        //                 $('#formDelete').submit();
        //             }); 
        //         }
        //     });
        // }

    </script>
@endsection
