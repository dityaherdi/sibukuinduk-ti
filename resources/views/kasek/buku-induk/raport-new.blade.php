<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Laporan Hasil Belajar Siswa </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<style>
    .tb{
        border: 1px solid black;
    }
    body{
        background-image: url('images/Logo.png');
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center; 
        height: 100%;
    }
</style>
<body>
	<center>
		<h5>LAPORAN HASIL
            <br>
            PENILAIAN BELAJAR SISWA</h4>
    </center>
    <br>
    <small>
        <table>
            <tbody>
                <tr>
                    <td>Nama Peserta Didik</td><td> : </td><td>{{$siswa->nama}}</td>
                </tr>
                <tr>
                    <td>NIS</td><td> : </td><td>{{$siswa->nis}}</td>
                </tr>
                <tr>
                    <td>Kelas</td><td> : </td><td>{{$rombel->jurusan}}</td>
                </tr>
                <tr>
                    <td>Semester</td><td> : </td><td>{{$semester}}</td>
                </tr>
            </tbody>
        </table>
    </small>

    <br>
    <br>
    <h5>A. Nilai Akademik</h5>

	<small>
        <table style="width: 100%; border-collapse: collapse; border: 1px solid black; text-align: center">
            <thead>
                <tr>
                    <th style="border: 1px solid black"><center>No</center></th>
                    <th style="border: 1px solid black"><center>Mata Pelajaran</center></th>
                    <th style="border: 1px solid black"><center>Pengetahuan</center></th>
                    <th style="border: 1px solid black"><center>Keterampilan</center></th>
                    <th style="border: 1px solid black"><center>Nilai Sikap</center></th>
                </tr>
            </thead>
            <tbody>
                @php
                    $no = 1;
                @endphp
                @foreach ($nilai as $item)
                    <tr>
                        <td style="border: 1px solid black">{{$no++}}</td>
                        <td style="border: 1px solid black; text-align: left">{{$item['nama_mapel']}}</td>
                        <td style="border: 1px solid black">{{$item['pengetahuan']}}</td>
                        <td style="border: 1px solid black">{{$item['keterampilan']}}</td>
                        <td style="border: 1px solid black">{{$item['sikap']}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </small>

    <br>
    <h5>B. Ekstrakurikuler</h5>

	<small>
        <table style="width: 100%; border-collapse: collapse; border: 1px solid black; text-align: center">
            <thead>
                <tr>
                    <th style="border: 1px solid black"><center>No</center></th>
                    <th style="border: 1px solid black"><center>Kegiatan Ekstrakurikuler</center></th>
                    <th style="border: 1px solid black"><center>Nilai</center></th>
                </tr>
            </thead>
            <tbody>
                @php
                    $n = 1;
                @endphp
                @foreach ($ekstras as $extra)
                    <tr>
                        <td style="border: 1px solid black">{{$n++}}</td>
                        <td style="border: 1px solid black">{{$extra->ekstras->nama_ekstra}}</td>
                        <td style="border: 1px solid black">{{$extra->nilai_siswa}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </small>

    <br>
    <h5>C. Ketidakhadiran</h5>

	<small>
        <table style="width: 100%; border-collapse: collapse; border: 1px solid black; text-align: center">
            <tbody>
                <tr>
                    <td style="border: 1px solid black">Sakit</td>
                    <td style="border: 1px solid black">{{$kehadiran->total_sakit}} Hari</td>
                </tr>
                <tr>
                    <td style="border: 1px solid black">Izin</td>
                    <td style="border: 1px solid black">{{$kehadiran->total_ijin}} Hari</td>
                </tr>
                <tr>
                    <td style="border: 1px solid black">Tanpa Keterangan</td>
                    <td style="border: 1px solid black">{{$kehadiran->total_tanpaket}} Hari</td>
                </tr>
            </tbody>
        </table>
    </small>

</body>
</html>