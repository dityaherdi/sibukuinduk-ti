@extends('layouts.master',['activeMenu' => 'buku-induk'])
@section('title','Daftar Rombel')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Siswa Semester {{$search}}
        <small>Daftar Siswa Semester {{$search}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Siswa Semester {{$search}}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="row" style="margin-bottom: 5px">
                        <form action="{{url('admin/laporan-hasil-belajar/semester')}}" method="GET">
                            <div class="col-md-2">
                                <label for="">Pilih Semester</label>
                                <select name="semester" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                            <div class="col-md-10">
                                <button class="btn btn-md btn-primary" style="margin-top: 25px">Submit</button>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table id="tabelRombel" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Jenis Rombel</th>
                                    <th>Tahun Ajaran</th>
                                    <th>NIS</th>
                                    <th>Nama Siswa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($rombels as $rombel)
                                    <tr>
                                        <td>
                                            {{-- <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Option <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="javascript:void(0);">Edit Rombel </a></li>

                                                    <li class="divider"></li>
                                                    <li><a href="javascript:void(0);">Hapus Rombel </a></li>
                                                </ul>
                                            </div> --}}
                                            <a 
                                                href="{{ url("admin/export-lembar-nilai/$rombel->id_user/$rombel->id_data_jenis_rombel") }}"
                                                class="btn btn-primary btn-sm"
                                                target="_blank"
                                                title="Cetak hasil belajar">
                                                <i class="fa fa-print"></i>
                                            </a>
                                        </td>
                                        <td>{{ $rombel->nama_rombel}}</td>
                                        <td>{{ $rombel->thn_ajaran }}</td>
                                        <td>{{ $rombel->nis }}</td>
                                        <td>{{ $rombel->nama }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $('#jenRombel').select2()
        $('#siswaAll').select2()
        $(function(){
            $('#tabelRombel').dataTable()
        });
        function lembarNilaiPdf(url) {
            // var url = '{{ route("export:lembarnilai", ":id") }}';
            // url = url.replace(':id', id);

            console.log(url)
            // var rombel = $('#rombelField').val()
            // var semester = $('#semesterField').val()
            
            // if (id.length === 0 || rombelId.length === 0) {
            //     swal({
            //         title: "Perhatian!",
            //         text: "Rombel dan Semester harus diisi!",
            //         icon: "warning",
            //     })
            // } else {
            //     window.open(`admin/export-lembar-nilai/${id}/${rombelId}`, '_blank');
            //     // window.open(`export-lembar-nilai/${id}/${rombel}/${semester}`, '_blank');
            // }

        }
    </script>
@endsection
