@extends('layouts.master',['activeMenu' => 'buku-induk'])
@section('title','Daftar Nilai ')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Nilai 
        <small>Daftar Nilai </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Nilai </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2">
                            <h4>Nama</h4>
                            <h4>NIS</h4>
                        </div>
                        <div class="col-md-4">
                            <h4>: {{$user->nama}}</h4>
                            <h4>: {{$user->nis}}</h4>
                        </div>
                        <form action="{{url('admin/laporan-hasil-belajar/search')}}" method="GET">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="hidden" name="id_user" value="{{$user->id_user}}">
                                    <label for="">Pilih semester</label>
                                    <select name="search" class="form-control" id="">
                                        <option value="">Pilih Semester</option>
                                        <option value="1" {{$semester == '1' ? 'selected' : ''}}>1</option>
                                        <option value="2" {{$semester == '2' ? 'selected' : ''}}>2</option>
                                        <option value="3" {{$semester == '3' ? 'selected' : ''}}>3</option>
                                        <option value="4" {{$semester == '4' ? 'selected' : ''}}>4</option>
                                        <option value="5" {{$semester == '5' ? 'selected' : ''}}>5</option>
                                        <option value="6" {{$semester == '6' ? 'selected' : ''}}>6</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <input type="submit" class="btn btn-primary btn-md" name="submit" id="" value="Submit" style="margin-top: 25px">
                                @php
                                    $s = null;
                                    foreach ($nilais as $key) {
                                        $mp = $key->id_data_mata_pelajaran;
                                        $s = substr($mp,0,1);
                                    }
                                @endphp
                                @if ($s)
                                
                                    <a 
                                        href="{{ url("admin/export-lembar-nilai/$rombel->id_user/$rombel->id_data_jenis_rombel/$semester") }}"
                                        class="btn btn-primary btn-md"
                                        target="_blank"
                                        title="Cetak hasil belajar" style="margin-top: 25px">
                                        <i class="fa fa-print"></i>
                                        Export
                                    </a>
                                @else
                                    
                                @endif
                            </div>
                        </form>
                        
                    </div>
                    <div class="table-responsive">
                        <table id="tabelNilai" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Semester</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Kode Rombel</th>
                                    <th>Nilai Pengetahuan</th>
                                    <th>Nilai Keterampilan</th>
                                    <th>Nilai Sikap</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($nilais as $nilai)
                                @php
                                    $mapel = $nilai->id_data_mata_pelajaran;
                                    $str = str_replace("_", " ",($nilai->id_data_mata_pelajaran));
                                    $str = strtoupper($str);
                                    $string = substr($str, 0,1);
                                    $str = substr($str,2);
                                    $sm = substr($mapel,0,1);
                                @endphp
                                    @if ($sm == $semester)
                                        <tr>
                                            <td>
                                                {{$no++}}
                                            </td>
                                            <td>{{ $string }}</td>
                                            <td>{{ $str }}</td>
                                            <td>{{ $nilai->kode_rombel }}</td>
                                            <td>{{ $nilai->nilai_pengetahuan }}</td>
                                            <td>{{ $nilai->nilai_keterampilan }}</td>
                                            <td>{{ $nilai->nilai_sikap }}</td>
                                        </tr>
                                    @else
                                        
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelNilai').dataTable()
        });
    </script>
@endsection
