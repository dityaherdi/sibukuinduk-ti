<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Lembar Buku Induk - {{ $siswa->nama_siswa_lengkap }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('backend/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('backend/dist/css/AdminLTE.min.css') }}">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style type="text/css">
    @media print {
        * { overflow: visible !important; }
    }
</style>
</head>
<body onload="window.print();">
    <div class="container">
        <div class="wrapper">
        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                SMK TI Bali Global
                <small class="pull-right">Lembar Buku Induk Siswa</small>
                </h2>
            </div>
            <!-- /.col -->
            </div>
            <!-- info row -->
            {{-- <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                <strong>Admin, Inc.</strong><br>
                795 Folsom Ave, Suite 600<br>
                San Francisco, CA 94107<br>
                Phone: (804) 123-5432<br>
                Email: info@almasaeedstudio.com
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address>
                <strong>John Doe</strong><br>
                795 Folsom Ave, Suite 600<br>
                San Francisco, CA 94107<br>
                Phone: (555) 539-1037<br>
                Email: john.doe@example.com
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Invoice #007612</b><br>
                <br>
                <b>Order ID:</b> 4F3S8J<br>
                <b>Payment Due:</b> 2/22/2014<br>
                <b>Account:</b> 968-34567
            </div>
            <!-- /.col -->
            </div> --}}
            <!-- /.row -->
            <!-- Table row -->
            @php
            $nomor = 1;
            @endphp
            <div class="row">
            <div class="col-xs-6 table-responsive">
                {{-- KETERANGAN TENTANG DIRI SISWA --}}
                <table class="table table-striped">
                <thead>
                <tr>
                    {{-- <th></th> --}}
                    <th colspan="3">A. KETERANGAN TENTANG DIRI SISWA</th>
                    {{-- <th></th> --}}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Nama Siswa</td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Nama Lengkap</td>
                    <td>{{ $siswa->nama_siswa_lengkap }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Nama Panggilan</td>
                    <td>{{ $siswa->nama_siswa_panggilan }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Jenis Kelamin</td>
                    <td>{{ $siswa->jenis_kelamin }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Tempat, Tanggal Lahir</td>
                    <td>{{ $siswa->ttgl_lahir }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Agama</td>
                    <td>{{ $siswa->agama }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Kewarganegaraan</td>
                    <td>{{ $siswa->kewarganegaraan }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Anak Keberapa</td>
                    <td>{{ $siswa->anak_keberapa }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Jumlah Saudara Kandung</td>
                    <td>{{ $siswa->jum_saudara_kandung }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Jumlah Saudara Tiri</td>
                    <td>{{ $siswa->jum_saudara_tiri }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Jumlah Saudara Angkat</td>
                    <td>{{ $siswa->jum_saudara_angkat }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Status Anak</td>
                    <td>{{ $siswa->status_anak }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Bahasa Sehari-hari</td>
                    <td>{{ $siswa->bahasa_seharihari }}</td>
                </tr>
                </tbody>
                </table>
                {{-- KETERANGAN TEMPAT TINGGAL --}}
                <table class="table table-striped">
                <thead>
                <tr>
                    {{-- <th>B.</th> --}}
                    <th colspan="3">B. KETERANGAN TEMPAT TINGGAL</th>
                    {{-- <th></th> --}}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Alamat</td>
                    <td>{{ $siswa->alamat }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Nomor Telepon</td>
                    <td>{{ $siswa->no_tlp }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Tinggal Bersama</td>
                    <td>{{ $siswa->tinggal_dgn }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Jarak Tempat Tinggal Ke Sekolah</td>
                    <td>{{ $siswa->jarak_kesekolah }}</td>
                </tr>
                </tbody>
                </table>
                {{-- KETERANGAN KESEHATAN --}}
                <table class="table table-striped">
                <thead>
                <tr>
                    {{-- <th>C.</th> --}}
                    <th colspan="3">C. KETERANGAN KESEHATAN</th>
                    {{-- <th></th> --}}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Golongan Darah</td>
                    <td>{{ $siswa->golongan_darah }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Nomor Telepon</td>
                    <td>{{ $siswa->penyakit_yang_pernah_diderita }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Kelainan Jasmani</td>
                    <td>{{ $siswa->kelainan_jasmani }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Tinggi / Berat Badan</td>
                    <td>{{ $siswa->tinggi. '/' .$siswa->berat_badan }}</td>
                </tr>
                </tbody>
                </table>
                {{-- KETERANGAN PENDIDIKAN --}}
                <table class="table table-striped">
                <thead>
                <tr>
                    {{-- <th>D.</th> --}}
                    <th colspan="3">D. KETERANGAN PENDIDIKAN</th>
                    {{-- <th></th> --}}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Pendidikan Sebelumnya</td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Lulusan Dari</td>
                    <td>{{ $siswa->lulusan_dari }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Tanggal</td>
                    <td>{{ $siswa->tgl_lulus }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>No. STTB</td>
                    <td>{{ $siswa->no_sttb }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Lama Belajar</td>
                    <td>{{ $siswa->lama_belajar }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Pindahan</td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>Dari Sekolah</td>
                    <td>{{ $siswa->pindahan_sekolah }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Alasan</td>
                    <td>{{ $siswa->alasan }}</td>
                </tr>
                </tbody>
                </table>
                {{-- KETERANGAN TENTANG AYAH KANDUNG --}}
                <table class="table table-striped">
                <thead>
                <tr>
                    {{-- <th>E.</th> --}}
                    <th colspan="3">E. KETERANGAN TENTANG AYAH KANDUNG</th>
                    {{-- <th></th> --}}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Nama</td>
                    <td>{{ $keluarga->nama_ayah }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Tempat, Tanggal Lahir</td>
                    <td>{{ $keluarga->ttgl_lahir_ayah }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Agama</td>
                    <td>{{ $keluarga->agama_ayah }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Kewarganegaraan</td>
                    <td>{{ $keluarga->kewarganegaraan_ayah }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Pendidikan</td>
                    <td>{{ $keluarga->pendidikan_ayah }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Pekerjaan</td>
                    <td>{{ $keluarga->pekerjaan_ayah }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Penghasilan per bulan</td>
                    <td>{{ $keluarga->penghasilan_bln_ayah }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Alamat / Telp</td>
                    <td>{{ $keluarga->alamat_ayah. '/' .$keluarga->tlp_ayah }}</td>
                </tr>
                <tr>
                    <td>{{$nomor++}}</td>
                    <td>Masih Hidup / Meninggal Dunia</td>
                    <td>{{ $keluarga->ket_ayah }}</td>
                </tr>
                </tbody>
                </table>
                
            </div>
            <!-- /.col -->
            <div class="col-xs-6 table-responsive">
                {{-- KETERANGAN TENTANG IBU KANDUNG --}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                    {{-- <th>F.</th> --}}
                    <th colspan="3">F. KETERANGAN TENTANG IBU KANDUNG</th>
                    {{-- <th></th> --}}
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Nama</td>
                    <td>{{ $keluarga->nama_ibu }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Tempat, Tanggal Lahir</td>
                    <td>{{ $keluarga->ttgl_lahir_ibu }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Agama</td>
                    <td>{{ $keluarga->agama_ibu }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Kewarganegaraan</td>
                    <td>{{ $keluarga->kewarganegaraan_ibu }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Pendidikan</td>
                    <td>{{ $keluarga->pendidikan_ibu }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Pekerjaan</td>
                    <td>{{ $keluarga->pekerjaan_ibu }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Penghasilan per bulan</td>
                    <td>{{ $keluarga->penghasilan_bln_ibu }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Alamat / Telp</td>
                    <td>{{ $keluarga->alamat_ibu. '/' .$keluarga->tlp_ibu }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Masih Hidup / Meninggal Dunia</td>
                    <td>{{ $keluarga->ket_ibu }}</td>
                    </tr>
                    </tbody>
                </table>
                {{-- KETERANGAN TENTANG WALI --}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                    {{-- <th>G.</th> --}}
                    <th colspan="3">G. KETERANGAN TENTANG WALI</th>
                    {{-- <th></th> --}}
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Nama</td>
                    <td>{{ $keluarga->nama_wali }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Tempat, Tanggal Lahir</td>
                    <td>{{ $keluarga->ttgl_lahir_wali }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Agama</td>
                    <td>{{ $keluarga->agama_wali }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Kewarganegaraan</td>
                    <td>{{ $keluarga->kewarganegaraan_wali }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Pendidikan</td>
                    <td>{{ $keluarga->pendidikan_wali }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Pekerjaan</td>
                    <td>{{ $keluarga->pekerjaan_wali }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Penghasilan per bulan</td>
                    <td>{{ $keluarga->penghasilan_bln_wali }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Alamat / Telp</td>
                    <td>{{ $keluarga->alamat_wali. '/' .$keluarga->tlp_wali }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Keterangan</td>
                    <td>{{ $keluarga->ket_wali }}</td>
                    </tr>
                    </tbody>
                </table>
                {{-- KEGEMARAN SISWA --}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                    {{-- <th>H.</th> --}}
                    <th colspan="3">H. KEGEMARAN SISWA</th>
                    {{-- <th></th> --}}
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Kesenian</td>
                    <td>{{ $siswa->kesenian }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Olahraga</td>
                    <td>{{ $siswa->olahraga }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Kemasyarakatan</td>
                    <td>{{ $siswa->kemasyarakatan }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Lain - lain</td>
                    <td>{{ $siswa->lainya }}</td>
                    </tr>
                    </tbody>
                </table>
                {{-- KETERANGAN PERKEMBANGAN SISWA --}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                    {{-- <th>I.</th> --}}
                    <th colspan="3">I. KETERANGAN PERKEMBANGAN SISWA</th>
                    {{-- <th></th> --}}
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Menerima Beasiswa</td>
                    <td>{{ $perkembangan->menerima_beasiswa }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Meninggalkan Sekolah</td>
                    <td></td>
                    </tr>
                    <tr>
                    <td></td>
                    <td>Tanggal</td>
                    <td>{{ $perkembangan->tgl_meninggalkan_sekolah }}</td>
                    </tr>
                    <tr>
                    <td></td>
                    <td>Alasan</td>
                    <td>{{ $perkembangan->alasan_keluar }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Akhir Pendidikan</td>
                    <td></td>
                    </tr>
                    <tr>
                    <td></td>
                    <td>Tamat Belajar</td>
                    <td>{{ $perkembangan->thn_tamat_belajar }}</td>
                    </tr>
                    <tr>
                    <td></td>
                    <td>No. STTB<</td>
                    <td>{{ $perkembangan->no_sttb_tamat }}</td>
                    </tr>
                    </tbody>
                </table>
                {{-- KETERANGAN SETELAH SELESAI PENDIDIKAN --}}
                <table class="table table-striped">
                    <thead>
                    <tr>
                    {{-- <th>J.</th> --}}
                    <th colspan="3">J. KETERANGAN SETELAH SELESAI PENDIDIKAN</th>
                    {{-- <th></th> --}}
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Melanjutkan / Bekerja di</td>
                    <td class="values">{{ $setPendidikan->melanjutkan_di_bekerja }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Tanggal mulai kerja</td>
                    <td>{{ $setPendidikan->tgl_kerja }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Perusahaan/Instansi/Lembaga</td>
                    <td>{{ $setPendidikan->nama_perusahaan_lembaga_dll }}</td>
                    </tr>
                    <tr>
                    <td>{{$nomor++}}</td>
                    <td>Penghasilan</td>
                    <td>{{ $setPendidikan->penghasilan }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->

            {{-- <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
                <p class="lead">Payment Methods:</p>
                <img src="../../dist/img/credit/visa.png" alt="Visa">
                <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                <img src="../../dist/img/credit/american-express.png" alt="American Express">
                <img src="../../dist/img/credit/paypal2.png" alt="Paypal">

                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr
                jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                </p>
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                <p class="lead">Amount Due 2/22/2014</p>

                <div class="table-responsive">
                <table class="table">
                    <tr>
                    <th style="width:50%">Subtotal:</th>
                    <td>$250.30</td>
                    </tr>
                    <tr>
                    <th>Tax (9.3%)</th>
                    <td>$10.34</td>
                    </tr>
                    <tr>
                    <th>Shipping:</th>
                    <td>$5.80</td>
                    </tr>
                    <tr>
                    <th>Total:</th>
                    <td>$265.24</td>
                    </tr>
                </table>
                </div>
            </div>
            <!-- /.col -->
            </div> --}}
            <!-- /.row -->
        </section>
        <!-- /.content -->
        </div>
        <!-- ./wrapper -->

</div>
</body>
</html>
