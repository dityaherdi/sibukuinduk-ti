<form action="{{url('siswa/data-pribadi-siswa/edit')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="id_user" value="{{$siswa->id_user}}">
    <div class="row">
        <div class="col-xs-6">
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama_siswa_lengkap" placeholder="Masukkan Nama Lengkap"
                        value="{{ $siswa->nama_siswa_lengkap }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Jenis Kelamin</label>
                    <select name="jenis_kelamin" class="form-control" value="{{ $siswa->jenis_kelamin }}">
                        <option value="0">Pilih jenis kelamin</option>
                        <option value="Laki-laki" {{$siswa->jenis_kelamin == 'Laki-laki' ? 'selected' : ''}}>Laki-laki</option>
                        <option value="Perempuan" {{$siswa->jenis_kelamin == 'Perempuan' ? 'selected' : ''}}>Perempuan</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12" id="ttl">
                <label class="col-form-label">Tempat & Tanggal Lahir</label>
                <div class="form-inline">
                    <input type="text" class="form-control" name="tempatLahir" placeholder="Tempat" value="{{ strtok($siswa->ttgl_lahir, ',') }}">
                    <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="tanggalLahir" id="tanggalLahir" value="{{substr($siswa->ttgl_lahir, strpos($siswa->ttgl_lahir, ",") + 1)}}">
                    </div>
                </div>
            </div>

            <div class="col-xs-12" style="margin-top: 15px">
                <div class="form-group">
                    <label class="col-form-label">Anak Ke</label>
                    <input type="number" min="0" class="form-control" name="anak_keberapa" value="{{$siswa->anak_keberapa}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Jumlah Saudara Kandung</label>
                    <input type="number" min="0" class="form-control" name="jum_saudara_kandung" value="{{$siswa->jum_saudara_kandung}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Jumlah Saudara Tiri</label>
                    <input type="number" min="0" class="form-control" name="jum_saudara_tiri" value="{{$siswa->jum_saudara_tiri}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Jumlah Saudara Angkat</label>
                    <input type="number" min="0" class="form-control" name="jum_saudara_angkat" value="{{$siswa->jum_saudara_angkat}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Status Anak</label>
                    <select name="status_anak" id="status_anak" class="form-control" value="{{$siswa->status_anak}}">
                        <option value="0">Pilih Status Anak</option>
                        <option value="kandung" {{$siswa->status_anak == 'kandung' ? 'selected' : ''}}>Kandung</option>
                        <option value="tiri" {{$siswa->status_anak == 'tiri' ? 'selected' : ''}}>Tiri</option>
                        <option value="angkat" {{$siswa->status_anak == 'angkat' ? 'selected' : ''}}>Angkat</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Golongan Darah</label>
                    <select name="golongan_darah" id="golongan_darah" class="form-control" value="{{$siswa->golongan_darah}}">
                        <option value="0">Pilih Golongan Darah</option>
                        <option value="A" {{$siswa->golongan_darah == 'A' ? 'selected' : ''}}>A</option>
                        <option value="B" {{$siswa->golongan_darah == 'B' ? 'selected' : ''}}>B</option>
                        <option value="O" {{$siswa->golongan_darah == 'O' ? 'selected' : ''}}>O</option>
                        <option value="AB" {{$siswa->golongan_darah == 'AB' ? 'selected' : ''}}>AB</option>
                    </select>
                </div>
            </div>
            
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Tinggi Badan (Sentimeter)</label>
                    <input type="number" min="0" class="form-control" name="tinggi" value="{{$siswa->tinggi}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Berat Badan (Kilogram)</label>
                    <input type="number" min="0" class="form-control" min="0" name="berat_badan" value="{{$siswa->berat_badan}}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label>Penyakit Yang Pernah Diderita</label>
                    <input type="text" name="penyakit_yang_pernah_diderita" class="form-control" value="{{$siswa->penyakit_yang_pernah_diderita}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Kelainan Jasmani</label>
                    <input type="text" name="kelainan_jasmani" class="form-control" value="{{$siswa->kelainan_jasmani}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Alasan Pindah</label>
                    <textarea style="width: 100%" class="form-control" name="alasan" rows="3" id="alasan">{{$siswa->alasan}}</textarea>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Kesenian</label>
                    <input type="text" name="kesenian" value="{{$siswa->kesenian}}" class="form-control">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Olahraga</label>
                    <input type="text" name="olahraga" value="{{$siswa->olahraga}}" class="form-control">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Foto</label>
                    <input type="file" id="fotoSiswa" name="fotoSiswa">
                </div>
            </div>
           
        </div>

        <div class="col-xs-6">
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Nama Panggilan</label>
                    <input type="text" class="form-control" name="nama_siswa_panggilan" placeholder="Masukkan Nama Panggilan"
                    value="{{$siswa->nama_siswa_panggilan}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Agama</label>
                    <select name="agama" id="agama" class="form-control" value="{{$siswa->agama}}">
                        <option value="0">Pilih Agama</option>
                        <option value="buddha" {{$siswa->agama == 'buddha' ? 'selected' : ''}}>Buddha</option>
                        <option value="hindu" {{$siswa->agama == 'hindu' ? 'selected' : ''}}>Hindu</option>
                        <option value="islam" {{$siswa->agama == 'islam' ? 'selected' : ''}}>Islam</option>
                        <option value="katolik" {{$siswa->agama == 'katolik' ? 'selected' : ''}}>Katolik</option>
                        <option value="protestan" {{$siswa->agama == 'protestan' ? 'selected' : ''}}>Protestan</option>
                        <option value="konghucu" {{$siswa->agama == 'konghucu' ? 'selected' : ''}}>Konghucu</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12" id="kewarganegaraan">
                <div class="form-group">
                    <label class="col-form-label">Kewarganegaraan</label>
                    <input type="text" class="form-control" name="kewarganegaraan" placeholder="Masukkan kewarganegaraan" value="{{$siswa->kewarganegaraan}}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Bahasa Sehari-hari</label>
                    <input type="text" class="form-control" name="bahasa_seharihari" value="{{$siswa->bahasa_seharihari}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Alamat</label>
                    <input type="text" class="form-control" name="alamat" value="{{$siswa->alamat}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Telepon</label>
                    <input type="text" class="form-control" name="no_tlp" value="{{$siswa->no_tlp}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Tinggal Bersama</label>
                    <input type="text" class="form-control" name="tinggal_dgn" value="{{$siswa->tinggal_dgn}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Jarak Kesekolah (Kilometer)</label>
                    <input type="number" min="0" class="form-control" name="jarak_kesekolah" value="{{$siswa->jarak_kesekolah}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Lulusan Dari</label>
                    <input type="text" class="form-control" min="0" name="lulusan_dari" value="{{$siswa->lulusan_dari}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Tanggal Lulus</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="tgl_lulus" name="tgl_lulus" value="{{date('m/d/Y', strtotime($siswa->tgl_lulus))}}">
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">No. STTB</label>
                    <input type="text" class="form-control" name="no_sttb" value="{{$siswa->no_sttb}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Lama Belajar (Tahun)</label>
                    <input type="number" min="0" class="form-control" name="lama_belajar" value="{{$siswa->lama_belajar}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Pindahan Sekolah</label>
                    <input type="text" class="form-control" name="pindahan_sekolah" value="{{$siswa->pindahan_sekolah}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Diterima Kelas</label>
                    <select name="diterima_kls" id="diterima_kls" class="form-control" value="{{$siswa->diterima_kls}}">
                        <option value="X" {{$siswa->diterima_kls == 'X' ? 'selected' : ''}}>X</option>
                        <option value="XI" {{$siswa->diterima_kls == 'XI' ? 'selected' : ''}}>XI</option>
                        <option value="XII" {{$siswa->diterima_kls == 'XII' ? 'selected' : ''}}>XII</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Kelompok</label>
                    <input type="text" class="form-control" name="kelompok" value="{{$siswa->kelompok}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Jurusan</label>
                    <input type="text" class="form-control" name="jurusan" value="{{$siswa->jurusan}}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Tanggal Diterima</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="tgl_diterima" name="tgl_diterima" value="{{date('m/d/Y',strtotime($siswa->tgl_diterima))}}">
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Lainya</label> <br>
                    <textarea style="width: 100%" class="form-control" name="lainya" rows="3" id="lainya">{{$siswa->lainya}}</textarea>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label>Kemasyarakatan</label> <br>
                    <textarea style="width: 100%" class="form-control" name="kemasyarakatan" rows="3" id="kemasyarakatan">{{$siswa->kemasyarakatan}}</textarea>
                </div>
            </div>
            

            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>

        </div>
    </div>
</form>