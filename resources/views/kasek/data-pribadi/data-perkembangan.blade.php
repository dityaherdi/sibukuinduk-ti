<!-- UNTUK URL FORM GUNAKAN -- admin/data-pribadi-siswa/editPerkembangan -- -->
<form action="{{url('admin/data-pribadi-siswa/editperkembangan')}}" method="post">
    {{csrf_field()}}
<input type="hidden" name="_method" value="put">
<input type="hidden" name="id_user" value="{{$siswa->id_user}}">
    <div class="row">
    <div class="col-xs-6">
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Menerima Beasiswa</label>
                    <input type="text" class="form-control" name="menerima_beasiswa" placeholder="Menerima Beasiswa"
                    value="{{ $perkembangan->menerima_beasiswa }}">
            </div>
        </div>

        <div class="col-xs-12" id="ttl">
            <label class="col-form-label">Tanggal Meninggalkan Sekolah</label>
            <div class="form-inline">
                <div class="input-group date">
                    <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" name="tgl_meninggalkan_sekolah" id="tgl_meninggalkan_sekolah" value="{{$perkembangan->tgl_meninggalkan_sekolah}}">
                </div>
            </div>
        </div>

        <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Alasan Keluar</label>
                    <input type="text" class="form-control" name="alasan_keluar" placeholder="Alasan Keluar"
                        value="{{ $perkembangan->alasan_keluar }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Tahun Tamat Belajar</label>
                    <input type="text" class="form-control" name="thn_tamat_belajar" placeholder="Tahun Tamat Belajar"
                        value="{{ $perkembangan->thn_tamat_belajar }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">No STTB</label>
                    <input type="text" class="form-control" name="no_sttb_tamat" placeholder="No STTB"
                        value="{{ $perkembangan->no_sttb_tamat }}">
                </div>
            </div>

            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
    </div>
    </div>
</form>