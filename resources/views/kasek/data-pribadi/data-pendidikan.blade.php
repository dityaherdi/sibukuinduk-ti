<!-- UNTUK URL FORM GUNAKAN -- admin/data-pribadi-siswa/editPendidikan -- -->
<form action="{{url('admin/data-pribadi-siswa/editPendidikan')}}" method="post">
{{csrf_field()}}
<input type="hidden" name="_method" value="put">
<input type="hidden" name="id_user" value="{{$siswa->id_user}}">
    <div class="row">

        <div class="col-xs-6">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="col-form-label">Melanjutkan / Bekerja di</label>
                        <input type="text" class="form-control" name="melanjutkan_di_bekerja" placeholder="Melanjutkan / Bekerja di"
                        value="{{ $pendidikan->melanjutkan_di_bekerja }}">
                </div>
        </div>


        <div class="col-xs-12" id="ttl">
                <label class="col-form-label">Tanggal Mulai Bekerja</label>
            <div class="form-inline">
                <div class="input-group date">
                    <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                    </div>
                <input type="text" class="form-control pull-right" name="tgl_kerja" id="tgl_kerja" value="{{$pendidikan->tgl_kerja}}">
            </div>
                </div>
        </div>

        <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Nama Perusahaan/Lembaga/dll</label>
                    <input type="text" class="form-control" name="nama_perusahaan_lembaga_dll" placeholder="Nama Perusahaan/Lembaga/dll"
                        value="{{ $pendidikan->nama_perusahaan_lembaga_dll }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Pengahasilan</label>
                    <input type="text" class="form-control" name="penghasilan" placeholder="Penghasilan"
                        value="{{ $pendidikan->penghasilan }}">
                </div>
            </div>

            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>

    </div>
</div>
</form>