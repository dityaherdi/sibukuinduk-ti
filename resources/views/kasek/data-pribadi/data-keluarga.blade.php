<form action="{{url('siswa/data-pribadi-siswa/editkeluarga')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="id_user" value="{{$siswa->id_user}}">
    <div class="row">
        <div class="col-xs-6">
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Nama Ayah</label>
                    <input type="text" class="form-control" name="nama_ayah" placeholder="Masukkan Nama Ayah"
                        value="{{ $keluarga->nama_ayah }}">
                </div>
            </div>
        
            <div class="col-xs-12" id="ttl">
                <label class="col-form-label">Tempat & Tanggal Lahir Ayah</label>
                <div class="form-inline">
                    <input type="text" class="form-control" name="tempatLahirAyah" placeholder="Tempat" value="{{ strtok($keluarga->ttgl_lahir_ayah, ',') }}">
                    <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="tanggalLahirAyah" id="tanggalLahirAyah" value="{{substr($keluarga->ttgl_lahir_ayah, strpos($keluarga->ttgl_lahir_ayah, ",") + 1)}}">
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Agama Ayah</label>
                    <select name="agama_ayah" id="agama_ayah" class="form-control" value="{{$keluarga->agama_ayah}}">
                        <option value="buddha" {{$keluarga->agama_ayah == 'buddha' ? 'selected' : ''}}>Buddha</option>
                        <option value="hindu" {{$keluarga->agama_ayah == 'hindu' ? 'selected' : ''}}>Hindu</option>
                        <option value="islam" {{$keluarga->agama_ayah == 'islam' ? 'selected' : ''}}>Islam</option>
                        <option value="katolik" {{$keluarga->agama_ayah == 'katolik' ? 'selected' : ''}}>Katolik</option>
                        <option value="protestan" {{$keluarga->agama_ayah == 'protestan' ? 'selected' : ''}}>Protestan</option>
                        <option value="konghucu" {{$keluarga->agama_ayah == 'konghucu' ? 'selected' : ''}}>Konghucu</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Kewarganegaraan Ayah</label>
                    <input type="text" class="form-control" name="kewarganegaraan_ayah" placeholder="Kewarganegaraan Ayah"
                        value="{{ $keluarga->kewarganegaraan_ayah }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Pendidikan Ayah</label>
                    <input type="text" class="form-control" name="pendidikan_ayah" placeholder="Pendidikan Ayah"
                        value="{{ $keluarga->pendidikan_ayah }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Pekerjaan Ayah</label>
                    <input type="text" class="form-control" name="pekerjaan_ayah" placeholder="Pekerjaan Ayah"
                        value="{{ $keluarga->pekerjaan_ayah }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Penghasilan Ayah</label>
                    <input type="text" class="form-control" name="penghasilan_bln_ayah" placeholder="Penghasilan Ayah"
                        value="{{ $keluarga->penghasilan_bln_ayah }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Alamat Ayah</label>
                    <input type="text" class="form-control" name="alamat_ayah" placeholder="Alamat Ayah"
                        value="{{ $keluarga->alamat_ayah }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Telepon Ayah</label>
                    <input type="text" class="form-control" name="tlp_ayah" placeholder="Telepon Ayah"
                        value="{{ $keluarga->tlp_ayah }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Keterangan Ayah</label>
                    <input type="text" class="form-control" name="ket_ayah" placeholder="Keterangan Ayah"
                        value="{{ $keluarga->ket_ayah }}">
                </div>
            </div>




            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Nama Ibu</label>
                    <input type="text" class="form-control" name="nama_ibu" placeholder="Masukkan Nama Ibu"
                        value="{{ $keluarga->nama_ibu }}">
                </div>
            </div>
        
            <div class="col-xs-12" id="ttl">
                <label class="col-form-label">Tempat & Tanggal Lahir Ibu</label>
                <div class="form-inline">
                    <input type="text" class="form-control" name="tempatLahirIbu" placeholder="Tempat" value="{{ strtok($keluarga->ttgl_lahir_ibu, ',') }}">
                    <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="tanggalLahirIbu" id="tanggalLahirIbu" value="{{substr($keluarga->ttgl_lahir_ibu, strpos($keluarga->ttgl_lahir_ibu, ",") + 1)}}">
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Agama Ibu</label>
                    <select name="agama_ibu" id="agama_ibu" class="form-control" value="{{$keluarga->agama_ibu}}">
                        <option value="buddha" {{$keluarga->agama_ibu == 'buddha' ? 'selected' : ''}}>Buddha</option>
                        <option value="hindu" {{$keluarga->agama_ibu == 'hindu' ? 'selected' : ''}}>Hindu</option>
                        <option value="islam" {{$keluarga->agama_ibu == 'islam' ? 'selected' : ''}}>Islam</option>
                        <option value="katolik" {{$keluarga->agama_ibu == 'katolik' ? 'selected' : ''}}>Katolik</option>
                        <option value="protestan" {{$keluarga->agama_ibu == 'protestan' ? 'selected' : ''}}>Protestan</option>
                        <option value="konghucu" {{$keluarga->agama_ibu == 'konghucu' ? 'selected' : ''}}>Konghucu</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Kewarganegaraan Ibu</label>
                    <input type="text" class="form-control" name="kewarganegaraan_ibu" placeholder="Kewarganegaraan Ibu"
                        value="{{ $keluarga->kewarganegaraan_ibu }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Pendidikan Ibu</label>
                    <input type="text" class="form-control" name="pendidikan_ibu" placeholder="Pendidikan Ibu"
                        value="{{ $keluarga->pendidikan_ibu }}">
                </div>
            </div>

            

           

        </div>
        <div class="col-xs-6">
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Pekerjaan Ibu</label>
                    <input type="text" class="form-control" name="pekerjaan_ibu" placeholder="Pekerjaan Ibu"
                        value="{{ $keluarga->pekerjaan_ibu }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Penghasilan Ibu</label>
                    <input type="text" class="form-control" name="penghasilan_bln_ibu" placeholder="Penghasilan Ibu"
                        value="{{ $keluarga->penghasilan_bln_ibu }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Alamat Ibu</label>
                    <input type="text" class="form-control" name="alamat_ibu" placeholder="Alamat Ibu"
                        value="{{ $keluarga->alamat_ibu }}">
                </div>
            </div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Telepon Ibu</label>
                    <input type="text" class="form-control" name="tlp_ibu" placeholder="Telepon Ibu"
                        value="{{ $keluarga->tlp_ibu }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Keterangan Ibu</label>
                    <input type="text" class="form-control" name="ket_ibu" placeholder="Keterangan Ibu"
                        value="{{ $keluarga->ket_ibu }}">
                </div>
            </div>





            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Nama Wali</label>
                    <input type="text" class="form-control" name="nama_wali" placeholder="Masukkan Nama Wali"
                        value="{{ $keluarga->nama_wali }}">
                </div>
            </div>
        
            <div class="col-xs-12" id="ttl">
                <label class="col-form-label">Tempat & Tanggal Lahir Wali</label>
                <div class="form-inline">
                    <input type="text" class="form-control" name="tempatLahirWali" placeholder="Tempat" value="{{ strtok($keluarga->ttgl_lahir_wali, ',') }}">
                    <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" name="tanggalLahirWali" id="tanggalLahirWali" value="{{substr($keluarga->ttgl_lahir_wali, strpos($keluarga->ttgl_lahir_wali, ",") + 1)}}">
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Agama Wali</label>
                    <select name="agama_wali" id="agama_wali" class="form-control" value="{{$keluarga->agama_wali}}">
                        <option value="buddha" {{$keluarga->agama_wali == 'buddha' ? 'selected' : ''}}>Buddha</option>
                        <option value="hindu" {{$keluarga->agama_wali == 'hindu' ? 'selected' : ''}}>Hindu</option>
                        <option value="islam" {{$keluarga->agama_wali == 'islam' ? 'selected' : ''}}>Islam</option>
                        <option value="katolik" {{$keluarga->agama_wali == 'katolik' ? 'selected' : ''}}>Katolik</option>
                        <option value="protestan" {{$keluarga->agama_wali == 'protestan' ? 'selected' : ''}}>Protestan</option>
                        <option value="konghucu" {{$keluarga->agama_wali == 'konghucu' ? 'selected' : ''}}>Konghucu</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Kewarganegaraan Wali</label>
                    <input type="text" class="form-control" name="kewarganegaraan_wali" placeholder="Kewarganegaraan Wali"
                        value="{{ $keluarga->kewarganegaraan_wali }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Pendidikan Wali</label>
                    <input type="text" class="form-control" name="pendidikan_wali" placeholder="Pendidikan Wali"
                        value="{{ $keluarga->pendidikan_wali }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Pekerjaan Wali</label>
                    <input type="text" class="form-control" name="pekerjaan_wali" placeholder="Pekerjaan Wali"
                        value="{{ $keluarga->pekerjaan_wali }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Penghasilan Wali</label>
                    <input type="text" class="form-control" name="penghasilan_bln_wali" placeholder="Penghasilan Wali"
                        value="{{ $keluarga->penghasilan_bln_wali }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Alamat Wali</label>
                    <input type="text" class="form-control" name="alamat_wali" placeholder="Alamat Wali"
                        value="{{ $keluarga->alamat_wali }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Telepon Wali</label>
                    <input type="text" class="form-control" name="tlp_wali" placeholder="Telepon Wali"
                        value="{{ $keluarga->tlp_wali }}">
                </div>
            </div>

            <div class="col-xs-12">
                <div class="form-group">
                    <label class="col-form-label">Keterangan Wali</label>
                    <input type="text" class="form-control" name="ket_wali" placeholder="Keterangan Wali"
                        value="{{ $keluarga->ket_wali }}">
                </div>
            </div>


            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</form>