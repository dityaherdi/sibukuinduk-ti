@extends('layouts.master',['activeMenu' => 'data-pribadi'])
@section('title','Data Pribadi Siswa')
@section('css')
<link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">    
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Data Pribadi Siswa
            <small>Data Pribadi Siswa</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Pribadi Siswa</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab">Data Pribadi Siswa</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Data Keluarga Siswa</a></li>
                                <li><a href="#tab_3" data-toggle="tab">Data Perkembangan Siswa</a></li>
                                <li><a href="#tab_4" data-toggle="tab">Data Setelah Pendidikan</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    @include('kasek.data-pribadi.data-pribadi')
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    @include('kasek.data-pribadi.data-keluarga')
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    @include('kasek.data-pribadi.data-perkembangan')
                                </div>
                                <div class="tab-pane" id="tab_4">
                                    @include('kasek.data-pribadi.data-pendidikan')
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
<script src="{{ asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
    $(function(){
        $('#tabelKehadiran').dataTable()
    });
    $('#tanggalLahir').datepicker({
        autoclose: true
    });
    $('#tgl_lulus').datepicker({
        autoclose: true
    });
    $('#tgl_diterima').datepicker({
        autoclose: true
    });
    $('#tgl_meninggalkan_sekolah').datepicker({
        autoclose: true
    });
    $('#tgl_kerja').datepicker({
        autoclose: true
    });
</script>
@endsection
