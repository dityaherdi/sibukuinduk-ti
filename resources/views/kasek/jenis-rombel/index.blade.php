@extends('layouts.master',['activeMenu' => 'jenis_rombel'])
@section('title','Daftar Jenis Rombel')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Jenis Rombel
        <small>Daftar Jenis Rombel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Jenis Rombel</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <a href="javascript:void(0);" class="btn btn-primary btn-md" style="margin-bottom: 5px" data-target="#tambahRombelModal" data-toggle="modal">
                        <i class="fa fa-plus"></i>
                        Tambah Rombel Baru
                    </a>
                    <div class="modal fade" id="tambahRombelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel">Tambah Jenis Rombel Baru</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{url('admin/jenis-rombel/tambah')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Nama Rombel</label>
                                                    <input type="text" class="form-control" name="nama_rombel" placeholder="Masukkan Nama Rombel" value="{{old('nama_rombel')}}">
                                                    <small>Cth: AN, DKV, MM 1, TKJ 2, RPL 3</small>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Jurusan</label>
                                                    <select name="jurusan" class="form-control" value="{{old('jurusan')}}" id="">
                                                        <option value="">Pilih Jurusan</option>
                                                        <option value="MM">MM</option>
                                                        <option value="TKJ">TKJ</option>
                                                        <option value="RPL">RPL</option>
                                                        <option value="ANIMASI">ANIMASI</option>
                                                        <option value="DKV">DKV</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-form-label">Tahun Ajaran</label>
                                                    <input type="text" name="thn_ajaran" class="form-control" value="{{old('thn_ajaran')}}" placeholder="Masukan Tahun Ajaran">
                                                    <small>Contoh: 2020/2021</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                        <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="tabelJenisRombel" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Nama Jenis Rombel</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Jurusan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($jenisRombels as $jenis)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Option <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="javascript:void(0);" data-target="#editRombelModal{{$jenis->id_data_jenis_rombel}}" data-toggle="modal">Edit Jenis Rombel</a></li>
                                                    
                                                </ul>
                                            </div>
                                        </td>
                                        <td>{{ $jenis->nama_rombel }}</td>
                                        <td>{{$jenis->thn_ajaran}}</td>
                                        <td>{{ $jenis->jurusan }}</td>
                                    </tr>

                                    <div class="modal fade" id="editRombelModal{{$jenis->id_data_jenis_rombel}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h3 class="modal-title" id="exampleModalLabel">Edit Data Jenis Rombel</h3>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form action="{{url('admin/jenis-rombel/'.$jenis->id_data_jenis_rombel.'/edit')}}" method="post" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="put">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-form-label">Nama Rombel</label>
                                                                    <input type="text" class="form-control" name="nama_rombel" placeholder="Masukkan Nama Rombel" value="{{$jenis->nama_rombel}}" disabled>
                                                                    <small>Cth: AN, DKV, MM 1, TKJ 2, RPL 3</small>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-form-label">Jurusan</label>
                                                                    <select name="jurusan" class="form-control" value="{{$jenis->jurusan}}" id="">
                                                                        <option value="">Pilih Jurusan</option>
                                                                        <option value="MM" {{$jenis->jurusan == 'MM' ? 'selected' : ''}}>MM</option>
                                                                        <option value="TKJ" {{$jenis->jurusan == 'TKJ' ? 'selected' : ''}}>TKJ</option>
                                                                        <option value="RPL" {{$jenis->jurusan == 'RPL' ? 'selected' : ''}}>RPL</option>
                                                                        <option value="ANIMASI" {{$jenis->jurusan == 'ANIMASI' ? 'selected' : ''}}>ANIMASI</option>
                                                                        <option value="DKV" {{$jenis->jurusan == 'DKV' ? 'selected' : ''}}>DKV</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label class="col-form-label">Tahun Ajaran</label>
                                                                    <input type="text" name="thn_ajaran" class="form-control" value="{{$jenis->thn_ajaran}}" placeholder="Masukan Tahun Ajaran">
                                                                    <small>Contoh: 2020/2021</small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                        <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelJenisRombel').dataTable()
        });
        function editRombel() {
            $('#editRombelModal').modal('show');
        }
    </script>
@endsection
