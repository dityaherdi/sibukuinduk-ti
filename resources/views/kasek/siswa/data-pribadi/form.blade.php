@extends('layouts.master',['activeMenu' => 'siswa'])
@section('title','Daftar Siswa')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Siswa
        <small>{{ isset($dataPribadiSiswa) ? 'Edit Siswa' : 'Tambah Siswa' }}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{url('admin/siswa')}}"><i class="fa fa-dashboard"></i> Siswa</a></li>
        <li class="active">{{ isset($dataPribadiSiswa) ? 'Edit Siswa' : 'Daftar Siswa' }}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="box">
                <div class="box-body">
                    <form
                    action="{{ isset($dataPribadiSiswa) ? route('update:siswa', $dataPribadiSiswa) : route('store:siswa') }}"
                    method="POST" name="formSiswa" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ isset($dataPribadiSiswa) ? method_field('PUT') : ''}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Nama Lengkap</label>
                                        <input type="text" class="form-control" name="nama_siswa_lengkap" placeholder="Masukkan Nama Lengkap"
                                            value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->nama_siswa_lengkap : old('nama_siswa_lengkap') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Nama Panggilan</label>
                                        <input type="text" class="form-control" name="nama_siswa_panggilan" placeholder="Masukkan Nama Panggilan"
                                        value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->nama_siswa_panggilan : old('nama_siswa_panggilan') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jenis Kelamin</label>
                                        <select name="jenis_kelamin" class="form-control" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->jenis_kelamin : old('jenis_kelamin') }}">
                                            <option value="Laki-laki">Laki-laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12" id="ttl">
                                    <label class="col-form-label">Tempat & Tanggal Lahir</label>
                                    <div class="form-inline">
                                        <input type="text" class="form-control" name="tempatLahir" placeholder="Tempat" value="{{ isset($dataPribadiSiswa) ? explode(" ", $dataPribadiSiswa->ttgl_lahir)[0] : old('tempatLahir') }}">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" name="tanggalLahir" id="tanggalLahir" value="{{ isset($dataPribadiSiswa) ? explode(" ", $dataPribadiSiswa->ttgl_lahir)[1] : old('tanggalLahir') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Agama</label>
                                        <select name="agama" id="agama" class="form-control" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->agama : old('agama') }}">
                                            <option value="buddha">Buddha</option>
                                            <option value="hindu">Hindu</option>
                                            <option value="islam">Islam</option>
                                            <option value="katolik">Katolik</option>
                                            <option value="protestan">Protestan</option>
                                            <option value="konghucu">Konghucu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12" id="kewarganegaraan">
                                    <div class="form-group">
                                        <label class="col-form-label">Kewarganegaraan</label>
                                        <input type="text" class="form-control" name="kewarganegaraan" placeholder="Masukkan kewarganegaraan" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->kewarganegaraan : old('kewarganegaraan') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Anak Ke</label>
                                        <input type="number" min="0" class="form-control" name="anak_keberapa" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->anak_keberapa : old('anak_keberapa') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jumlah Saudara Kandung</label>
                                        <input type="number" min="0" class="form-control" name="jum_saudara_kandung" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->jum_saudara_kandung : old('jum_saudara_kandung') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jumlah Saudara Tiri</label>
                                        <input type="number" min="0" class="form-control" name="jum_saudara_tiri" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->jum_saudara_tiri : old('jum_saudara_tiri') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jumlah Saudara Angkat</label>
                                        <input type="number" min="0" class="form-control" name="jum_saudara_angkat" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->jum_saudara_angkat : old('jum_saudara_angkat') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Status Anak</label>
                                        <select name="status_anak" id="status_anak" class="form-control" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->status_anak : old('status_anak') }}">
                                            <option value="kandung">Kandung</option>
                                            <option value="tiri">Tiri</option>
                                            <option value="angkat">Angkat</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Bahasa Sehari-hari</label>
                                        <input type="text" class="form-control" name="bahasa_seharihari" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->bahasa_seharihari : old('bahasa_seharihari') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Alamat</label>
                                        <input type="text" class="form-control" name="alamat" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->alamat : old('alamat') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Telepon</label>
                                        <input type="text" class="form-control" name="no_tlp" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->no_tlp : old('no_tlp') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Tinggal Bersama</label>
                                        <input type="text" class="form-control" name="tinggal_dgn" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->tinggal_dgn : old('tinggal_dgn') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jarak Kesekolah (Kilometer)</label>
                                        <input type="number" min="0" class="form-control" name="jarak_kesekolah" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->jarak_kesekolah : old('jarak_kesekolah') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Golongan Darah</label>
                                        <select name="golongan_darah" id="golongan_darah" class="form-control" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->golongan_darah : old('golongan_darah') }}">
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="O">O</option>
                                            <option value="AB">AB</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Penyakit Yang Pernah Diderita</label> <br>
                                    <textarea style="width: 100%" name="penyakit_yang_pernah_diderita" rows="3" id="penyakit_yang_pernah_diderita">{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->penyakit_yang_pernah_diderita : old('penyakit_yang_pernah_diderita') }}
                                    </textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Kelainan Jasmani</label> <br>
                                        <textarea style="width: 100%" name="kelainan_jasmani" rows="3" id="kelainan_jasmani">{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->kelainan_jasmani : old('kelainan_jasmani') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Tinggi Badan (Sentimeter)</label>
                                        <input type="number" min="0" class="form-control" name="tinggi" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->tinggi : old('tinggi') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Berat Badan (Kilogram)</label>
                                        <input type="number" min="0" class="form-control" min="0" name="berat_badan" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->berat_badan : old('berat_badan') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Lulusan Dari</label>
                                        <input type="text" class="form-control" min="0" name="lulusan_dari" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->lulusan_dari : old('lulusan_dari') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Tanggal Lulus</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="tgl_lulus" name="tgl_lulus" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->tgl_lulus : old('tgl_lulus') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">No. STTB</label>
                                        <input type="text" class="form-control" name="no_sttb" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->no_sttb : old('no_sttb') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Lama Belajar (Tahun)</label>
                                        <input type="number" min="0" class="form-control" name="lama_belajar" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->lama_belajar : old('lama_belajar') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Pindahan Sekolah</label>
                                        <input type="text" class="form-control" name="pindahan_sekolah" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->pindahan_sekolah : old('pindahan_sekolah') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alasan Pindah</label> <br>
                                        <textarea style="width: 100%" name="alasan" rows="3" id="alasan">{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->alasan : old('alasan') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Diterima Kelas</label>
                                        <select name="diterima_kls" id="diterima_kls" class="form-control" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->diterima_kls : old('diterima_kls') }}">
                                            <option value="X">X</option>
                                            <option value="XI">XI</option>
                                            <option value="XII">XII</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Kelompok</label>
                                        <input type="text" class="form-control" name="kelompok" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->kelompok : old('kelompok') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Jurusan</label>
                                        <input type="text" class="form-control" name="jurusan" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->jurusan : old('jurusan') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label">Tanggal Diterima</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="tgl_diterima" name="tgl_diterima" value="{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->tgl_diterima : old('tgl_diterima') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Kesenian</label> <br>
                                        <textarea style="width: 100%" name="kesenian" rows="3" id="kesenian">{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->kesenian : old('kesenian') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Olahraga</label> <br>
                                        <textarea style="width: 100%" name="olahraga" rows="3" id="olahraga">{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->olahraga : old('olahraga') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Kemasyarakatan</label> <br>
                                        <textarea style="width: 100%" name="kemasyarakatan" rows="3" id="kemasyarakatan">{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->kemasyarakatan : old('kemasyarakatan') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Lainya</label> <br>
                                        <textarea style="width: 100%" name="lainya" rows="3" id="lainya">{{ isset($dataPribadiSiswa) ? $dataPribadiSiswa->lainya : old('lainya') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input type="file" id="fotoSiswa" name="fotoSiswa">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{ asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelKehadiran').dataTable()
        });
        $('#tanggalLahir').datepicker({
            autoclose: true
        });
        $('#tgl_lulus').datepicker({
            autoclose: true
        });
        $('#tgl_diterima').datepicker({
            autoclose: true
        });
    </script>
@endsection
