@extends('layouts.master',['activeMenu' => 'nilai'])
@section('title','Edit Nilai')
@section('content')
    <section class="content-header">
        <h1>
            Nilai {{$user->nama}}
            <small>Edit Nilai {{$user->nama}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Edit Nilai {{$user->nama}}</li>
        </ol>
    </section>
    <section class="content">
        <form class="" action="{{url('admin/data-nilai/'.$nilai->id_data_nilai.'/edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Mata Pelajaran: {{$str}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Nilai Pengetahuan</label>
                                <input type="text" class="form-control" name="nilai_pengetahuan" value="{{$nilai->nilai_pengetahuan}}" placeholder="Masukkan Nilai Pengetahuan">
                            </div>
                            <div class="form-group">
                                <label for="">Nilai Keterampilan</label>
                                <input type="number" class="form-control" name="nilai_keterampilan" value="{{$nilai->nilai_keterampilan}}">
                            </div>
                            <div class="form-group">
                                <label for="">Nilai Sikap</label>
                                <input type="text" class="form-control" name="nilai_sikap" value="{{$nilai->nilai_sikap}}">
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
