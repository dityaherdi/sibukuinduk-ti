@extends('layouts.master',['activeMenu' => 'keluarga'])
@section('title','Daftar Data Keluarga Siswa')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Keluarga Siswa
        <small>Daftar Keluarga Siswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Keluarga Siswa</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelDataKeluarga" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>User / Siswa</th>
                                    <th>Nama Ayah</th>
                                    <th>Tempat Tanggal Lahir Ayah</th>
                                    <th>Telepon Ayah</th>
                                    <th>Nama Ibu</th>
                                    <th>Tempat Tanggal Lahir Ibu</th>
                                    <th>Telepon Ibu</th>
                                    <th>Nama Wali</th>
                                    <th>Tempat Tanggal Lahir Wali</th>
                                    <th>Telepon Wali</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($keluargas as $keluarga)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Option <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="javascript:void(0);">Edit Keluarga</a></li>

                                                    <li class="divider"></li>
                                                    <li><a href="javascript:void(0);">Hapus Keluarga</a></li>
                                                    
                                                </ul>
                                            </div>
                                        </td>
                                        <td>{{ $keluarga->id_user }}</td>
                                        <td>{{ $keluarga->nama_ayah }}</td>
                                        <td>{{ $keluarga->ttgl_lahir_ayah }}</td>
                                        <td>{{ $keluarga->tlp_ayah }}</td>
                                        <td>{{ $keluarga->nama_ibu }}</td>
                                        <td>{{ $keluarga->ttgl_lahir_ibu }}</td>
                                        <td>{{ $keluarga->tlp_ibu }}</td>
                                        <td>{{ $keluarga->nama_wali }}</td>
                                        <td>{{ $keluarga->ttgl_lahir_wali }}</td>
                                        <td>{{ $keluarga->tlp_wali }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelDataKeluarga').dataTable()
        });
    </script>
@endsection
