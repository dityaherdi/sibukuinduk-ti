@extends('layouts.master',['activeMenu' => 'setelah_pendidikan'])
@section('title','Daftar Data Setelah Pendidikan')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Setelah Pendidikan
        <small>Daftar Setelah Pendidikan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Setelah Pendidikan</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelDataPerkembangan" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Siswa</th>
                                    <th>Melanjutkan di Bekerja</th>
                                    <th>Tgl Kerja</th>
                                    <th>Nama Perusahaan / Lembaga</th>
                                    <th>Penghasilan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($setelahPendidikans as $setelahPendidikan)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Option <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="javascript:void(0);">Edit Setelah Pendidikan</a></li>

                                                    <li class="divider"></li>
                                                    <li><a href="javascript:void(0);">Hapus Setelah Pendidikan</a></li>
                                                    
                                                </ul>
                                            </div>
                                        </td>
                                        <td>{{ $setelahPendidikan->id_data_pribadi_siswa }}</td>
                                        <td>{{ $setelahPendidikan->melanjutkan_di_bekerja }}</td>
                                        <td>{{ $setelahPendidikan->tgl_kerja }}</td>
                                        <td>{{ $setelahPendidikan->nama_perusahaan_lembaga_dll }}</td>
                                        <td>{{ $setelahPendidikan->penghasilan }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelDataPerkembangan').dataTable()
        });
    </script>
@endsection
