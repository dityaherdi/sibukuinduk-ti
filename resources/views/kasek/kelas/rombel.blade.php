@extends('layouts.master',['activeMenu' => 'kelas'])
@section('title','Daftar Rombel')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Rombel
        <small>Daftar Rombel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Rombel</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelRombel" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Jenis Rombel</th>
                                    <th>Tahun Ajaran</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($jenis as $rombel)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Opsi <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    {{-- <li><a href="javascript:void(0);" onclick="updatePassword('{{$rombel->id_data_jenis_rombel}}')">Detail</a></li>

                                                    <li class="divider"></li> --}}
                                                    @if ($rombel->status == 1)
                                                        <li><a href="javascript:void(0);" onclick="nonaktifStatus('{{$rombel->id_data_jenis_rombel}}')">Non Aktifkan Kelas</a></li>
                                                    @else
                                                        <li><a href="javascript:void(0);" onclick="aktifStatus('{{$rombel->id_data_jenis_rombel}}')">Aktifkan Kelas</a></li>
                                                    @endif
                                                    
                                                </ul>
                                            </div>
                                        </td>
                                        <td>{{ $rombel->nama_rombel}}</td>
                                        <td>{{ $rombel->thn_ajaran }}</td>
                                        <td>
                                            @if ($rombel->status == 1)
                                                <span class="label label-success">Aktif</span>
                                            @else
                                                <span class="label label-warning">Non Aktif</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <small>
                            <strong>
                                NB: Kelas aktif maka siswa dapat login kedalam sistem.
                            </strong>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<form class="hidden" action="" method="post" id="formActive">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
</form>
<form class="hidden" action="" method="post" id="formInnactive">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
</form>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $('#jenRombel').select2()
        $('#siswaAll').select2()
        $(function(){
            $('#tabelRombel').dataTable()
        });

        function nonaktifStatus(id){
            swal({
                title: "Anda yakin?",
                text: "Kelas akan di non aktifkan, seluruh siswa yang berada pada rombel ini akan di nonaktifkan sehingga tidak dapat mengakses sistem!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willInnactive) => {
                if (willInnactive) {
                    swal("Berhasil! Kelas berhasil dinonaktifkan!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formInnactive').attr('action', '{{url('admin/kelas/nonaktif')}}/'+id);
                        $('#formInnactive').submit();
                    }); 
                }
            });
        }
        function aktifStatus(id){
            swal({
                title: "Anda yakin?",
                text: "Kelas akan di aktifkan, seluruh siswa yang berada pada rombel ini akan di aktifkan sehingga siswa dapat mengakses sistem!",
                icon: "warning",
                buttons: true,
                dangerMode: false,
            })
            .then((willActive) => {
                if (willActive) {
                    swal("Berhasil! Kelas berhasil diaktifkan!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formActive').attr('action', '{{url('admin/kelas/aktif')}}/'+id);
                        $('#formActive').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
