@extends('layouts.master',['activeMenu' => 'nilai-ekstra'])
@section('title','Edit Nilai Ekstra')
@section('content')
    <section class="content-header">
        <h1>
            Nilai {{$user->nama}}
            <small>Edit Nilai {{$user->nama}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Edit Nilai Ekstra{{$user->nama}}</li>
        </ol>
    </section>
    <section class="content">
        <form class="" action="{{url('admin/nilai-ekstra/'.$nilaiekstra->id_data_nilai_ekstra.'/edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Nilai Ekstra</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Semester</label>
                                <input type="text" class="form-control" name="semester" value="{{$nilaiekstra->semester}}" placeholder="Masukan Semester" readonly>
                            </div>
                            <div class="form-group">
                                <label for="">Nilai Siswa</label>
                                <input type="text" class="form-control" name="nilai_siswa" value="{{$nilaiekstra->nilai_siswa}}">
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
