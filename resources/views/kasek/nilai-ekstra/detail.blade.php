@extends('layouts.master',['activeMenu' => 'nilai_ekstra'])
@section('title','Daftar Rombel')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Rombel {{$jenis->nama_rombel}}
        <small>Daftar Rombel {{$jenis->nama_rombel}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Rombel</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelRombel" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Jenis Rombel</th>
                                    <th>Tahun Ajaran</th>
                                    <th>NIS</th>
                                    <th>Nama Siswa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($rombels as $rombel)
                                    <tr>
                                        <td>
                                            <a href="{{url('admin/nilai-ekstra/detail-nilai/'.$rombel->nis)}}" class="btn btn-primary btn-sm">
                                                Detail Nilai
                                            </a>
                                        </td>
                                        <td>{{ $jenis->nama_rombel}}</td>
                                        <td>{{ $jenis->thn_ajaran }}</td>
                                        <td>{{ $rombel->nis }}</td>
                                        <td>{{ $rombel->user->nama }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $('#jenRombel').select2()
        $('#siswaAll').select2()
        $(function(){
            $('#tabelRombel').dataTable()
        });
    </script>
@endsection
