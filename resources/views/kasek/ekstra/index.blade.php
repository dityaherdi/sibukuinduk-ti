@extends('layouts.master',['activeMenu' => 'ekstra'])
@section('title','Daftar Ekstra')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Ekstra
        <small>Daftar Ekstra</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Ekstra</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelEkstra" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Nama Ekstra</th>
                                    <th>Pembina Ekstra</th>
                                    <th>Deskripsi Ekstra</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($ekstras as $ekstra)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Option <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{url('admin/ekstra/'.$ekstra->id_data_ekstra.'/edit')}}">Edit Ekstra</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>{{$ekstra->nama_ekstra}}</td>
                                        <td>
                                            @if($ekstra->pembina_ekstra == null)
                                                -
                                            @else
                                                {{$ekstra->pembina_ekstra}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($ekstra->deskripsi_ekstra == null)
                                                -
                                            @else
                                                {{$ekstra->deskripsi_ekstra}}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelEkstra').dataTable()
        });
    </script>
@endsection
