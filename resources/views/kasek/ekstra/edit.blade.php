@extends('layouts.master',['activeMenu' => 'ekstra'])
@section('title','Edit Ekstra')
@section('content')
    <section class="content-header">
        <h1>
            Ekstra
            <small>Edit Ekstra {{$ekstra->nama_ekstra}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Edit Ekstra {{$ekstra->nama_ekstra}}</li>
        </ol>
    </section>
    <section class="content">
        <form class="" action="{{url('admin/ekstra/'.$ekstra->id_data_ekstra.'/edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Ekstra {{$ekstra->nama_ekstra}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Nama Ekstra</label>
                                <input type="text" class="form-control" name="nama_ekstra" value="{{$ekstra->nama_ekstra}}" placeholder="Masukan Nama Ekstra">
                            </div>
                            <div class="form-group">
                                <label for="">Pembina Ekstra</label>
                                <input type="text" class="form-control" name="pembina_ekstra" value="{{$ekstra->pembina_ekstra}}" placeholder="Masukan Pembina Ekstra">
                            </div>
                            <div class="form-group">
                                <label for="">Deskripsi Ekstra</label>
                                <textarea name="deskripsi_ekstra" id="" cols="30" rows="5" class="form-control">{{$ekstra->deskripsi_ekstra}}</textarea>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
