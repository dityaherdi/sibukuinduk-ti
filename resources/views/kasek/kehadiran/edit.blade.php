@extends('layouts.master',['activeMenu' => 'mapel'])
@section('title','Edit Kehadiran')
@section('content')
    <section class="content-header">
        <h1>
            Kehadiran {{$siswa->nama}}
            <small>Edit Kehadiran {{$siswa->nama}}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Edit Kehadiran {{$siswa->nama}}</li>
        </ol>
    </section>
    <section class="content">
        <form class="" action="{{url('admin/kehadiran/'.$kehadiran->id_data_kehadiran_siswa.'/edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Kehadiran</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Nama Siswa</label>
                                <input type="text" class="form-control" name="nama" value="{{$siswa->nama}}" placeholder="Masukan Nama Siswa" readonly>
                            </div>
                            <div class="form-group">
                                <label for="">Semester</label>
                                <input type="number" class="form-control" name="semester" value="{{$kehadiran->semester}}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="">Total Sakit</label>
                                <input type="number" class="form-control" name="total_sakit" value="{{$kehadiran->total_sakit}}">
                            </div>
                            <div class="form-group">
                                <label for="">Total Ijin</label>
                                <input type="number" class="form-control" name="total_ijin" value="{{$kehadiran->total_ijin}}">
                            </div>
                            <div class="form-group">
                                <label for="">Total Tanpa Keterangan</label>
                                <input type="number" class="form-control" name="total_tanpaket" value="{{$kehadiran->total_tanpaket}}">
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
