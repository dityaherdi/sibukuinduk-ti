@extends('layouts.master',['activeMenu' => 'perkembangan'])
@section('title','Daftar Data Perkembangan Siswa')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Perkembangan Siswa
        <small>Daftar Perkembangan Siswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Perkembangan Siswa</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelDataPerkembangan" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Siswa</th>
                                    <th>Menerima Beasiswa</th>
                                    <th>Tgl Meninggalkan Sekolah</th>
                                    <th>Alasan Keluar</th>
                                    <th>Tahun Tamat Belajar</th>
                                    <th>No STTB</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($perkembangans as $perkembangan)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Option <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="javascript:void(0);">Edit Perkembangan</a></li>

                                                    <li class="divider"></li>
                                                    <li><a href="javascript:void(0);">Hapus Perkembangan</a></li>
                                                    
                                                </ul>
                                            </div>
                                        </td>
                                        <td>{{ $perkembangan->id_data_pribadi_siswa }}</td>
                                        <td>{{ $perkembangan->menerima_beasiswa }}</td>
                                        <td>{{ $perkembangan->tgl_meninggalkan_sekolah }}</td>
                                        <td>{{ $perkembangan->alasan_keluar }}</td>
                                        <td>{{ $perkembangan->thn_tamat_belajar }}</td>
                                        <td>{{ $perkembangan->no_sttb_tamat }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelDataPerkembangan').dataTable()
        });
    </script>
@endsection
