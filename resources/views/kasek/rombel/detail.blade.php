@extends('layouts.master',['activeMenu' => 'rombel'])
@section('title','Daftar Rombel')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    {{-- {{ dd($jenis->id_data_jenis_rombel) }} --}}
    <h1>
        Rombel {{$jenis->nama_rombel}}
        <small>Daftar Rombel {{$jenis->nama_rombel}}</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Rombel</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <a href="{{url('admin/rombel/tambah')}}" class="btn btn-primary btn-md" style="margin-bottom: 5px">
                        <i class="fa fa-plus"></i>
                        Tambah Data Rombel Baru
                    </a>
                    <a href="javascript:void(0);" class="btn btn-success btn-md" style="margin-bottom: 5px" data-target="#importRombelModal" data-toggle="modal">
                        <i class="fa fa-upload"></i>
                        Import Excel
                    </a>
                    <div class="table-responsive">
                        <table id="tabelRombel" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Jenis Rombel</th>
                                    <th>Tahun Ajaran</th>
                                    <th>NIS</th>
                                    <th>Nama Siswa</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($rombels as $rombel)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Option <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="javascript:void(0);" onclick="deleteRombel('{{$rombel->id_data_rombel}}')">Hapus Siswa </a></li>
                                                </ul>
                                            </div>
                                            {{-- <a 
                                                href="{{ url("admin/export-lembar-nilai/$rombel->id_user/$rombel->id_data_jenis_rombel") }}"
                                                class="btn btn-primary btn-sm"
                                                target="_blank"
                                                title="Cetak hasil belajar">
                                                <i class="fa fa-print"></i>
                                            </a> --}}
                                        </td>
                                        <td>{{ $jenis->nama_rombel}}</td>
                                        <td>{{ $jenis->thn_ajaran }}</td>
                                        <td>{{ $rombel->nis }}</td>
                                        <td>{{ $rombel->user->nama }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- IMPORT USER MODAL --}}
    <div class="modal fade" id="importRombelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('import:rombel') }}" method="post" name="importRombel" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="jenisRombel" value="{{ $jenis->id_data_jenis_rombel }}">
                                <div class="form-group">
                                    <label class="col-form-label">Pilih file excel</label>
                                    <input type="file" name="rombelExcel" id="rombelExcel">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-form-label">Pastikan format Excel sesuai contoh di bawah ini</label>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <th>NIS</th>
                                    </tr>
                                    <tr>
                                        <td>9999</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<form class="hidden" action="" method="post" id="formDelete">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="delete">
</form>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $('#jenRombel').select2()
        $('#siswaAll').select2()
        $(function(){
            $('#tabelRombel').dataTable()
        });
        function deleteRombel(id_data_rombel){
            swal({
                title: "Anda yakin?",
                text: "Anda akan menghapus siswa dari rombel ini!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data rombel siswa berhasil terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('admin/rombel/delete')}}/'+id_data_rombel);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
