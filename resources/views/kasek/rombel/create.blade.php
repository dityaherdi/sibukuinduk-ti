@extends('layouts.master',['activeMenu' => 'rombel'])
@section('title','Tambah Data Rombel')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Data Rombel
            <small>Tambah Data Rombel Baru</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Tambah Data Rombel Baru</li>
        </ol>
    </section>
    <section class="content">
        <form class="" action="{{url('admin/rombel/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Data Rombel Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-form-label">Nama Rombel</label>
                                <select name="id_data_jenis_rombel" id="jenRombel" value="{{old('id_data_jenis_rombel')}}" style="width: 100%">
                                    @foreach ($jenis as $j)
                                        <option value="{{$j->id_data_jenis_rombel}}">{{$j->nama_rombel}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Siswa</label>
                                <select name="id_user" id="siswaAll" value="{{old('id_user')}}" style="width: 100%">
                                    @foreach ($siswas as $siswa)
                                        <option value="{{$siswa->id_user}}">{{$siswa->nis}} - {{$siswa->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{-- <div class="form-group">
                                <label class="col-form-label">Tahun Ajaran</label>
                                <input type="text" name="thn_ajaran" class="form-control" value="{{old('thn_ajaran')}}" placeholder="Masukan Tahun Ajaran">
                                <small>Contoh: 2020/2021</small>
                            </div> --}}
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $('#jenRombel').select2()
        $('#siswaAll').select2()
        $(function(){
            $('#tabelRombel').dataTable()
        });
    </script>
@endsection
