@extends('layouts.master',['activeMenu' => 'mapel'])
@section('title','Daftar Mata Pelajaran')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Mata Pelajaran
        <small>Daftar Mata Pelajaran</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Mata Pelajaran</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelMapel" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Nama Mata Pelajaran</th>
                                    <th>Semester</th>
                                    <th>Jurusan</th>
                                    <th>KKM</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($mapels as $mapel)
                                    <tr>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
                                                    <span class="">Option <i class="caret"></i></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="{{url('admin/mata-pelajaran/'.$mapel->id_data_mata_pelajaran.'/edit')}}">Edit Mapel</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td><?= strtoupper(str_replace('_', ' ', $mapel->nama_mp)) ?></td>
                                        <td>{{$mapel->semester}}</td>
                                        <td>
                                            @if($mapel->jurusan == null)
                                                -
                                            @else
                                                {{$mapel->jurusan}}
                                            @endif
                                        </td>
                                        <td>
                                            @if($mapel->kkm == null)
                                                -
                                            @else
                                                {{$mapel->kkm}}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelMapel').dataTable()
        });
    </script>
@endsection
