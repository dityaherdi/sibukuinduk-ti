@extends('layouts.master',['activeMenu' => 'mapel'])
@section('title','Tambah Mata Pelajaran')
@section('content')
    <section class="content-header">
        <h1>
            Mata Pelajaran
            <small>Tambah Mata Pelajaran Baru</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Tambah Mata Pelajaran Baru</li>
        </ol>
    </section>
    <section class="content">
        <form class="" action="{{url('admin/mata-pelajaran/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Mata Pelajaran Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Nama Mata Pelajaran</label>
                                <input type="text" class="form-control" name="nama_mp" value="{{old('nama_mp')}}" placeholder="Masukan Nama Mapel">
                            </div>
                            <div class="form-group">
                                <label for="">Semester</label>
                                <input type="number" class="form-control" name="semester" value="{{old('semester')}}" placeholder="Masukan Semester">
                            </div>
                            <div class="form-group">
                                <label for="">Jurusan</label>
                                <input type="text" class="form-control" name="jurusan" value="{{old('jurusan')}}" placeholder="Masukan jurusan">
                            </div>
                            <div class="form-group">
                                <label for="">Nilai KKM</label>
                                <input type="number" class="form-control" name="kkm" value="{{old('kkm')}}" placeholder="Masukan KKM">
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
