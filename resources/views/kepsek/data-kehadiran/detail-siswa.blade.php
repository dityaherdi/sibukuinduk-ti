@extends('layouts.master',['activeMenu' => 'kehadiran'])
@section('title','Daftar Kehadiran Siswa')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Kehadiran Siswa
        <small>Daftar Kehadiran Siswa</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Kehadiran Siswa</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelKehadiran" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>NIS</th>
                                    <th>Nama</th>
                                    <th>Semester</th>
                                    <th>Total Sakit</th>
                                    <th>Total Ijin</th>
                                    <th>Total Tanpa Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                    $nama_siswa = \App\User::where('nis',$rombels->nis)->first();
                                @endphp
                                @foreach($siswas as $kehadiran)
                                    <tr>
                                        <td>{{$kehadiran->nis}}</td>
                                        <td>{{$nama_siswa->nama}}</td>
                                        <td>{{$kehadiran->semester}}</td>
                                        <td>{{$kehadiran->total_sakit}}</td>
                                        <td>{{$kehadiran->total_ijin}}</td>
                                        <td>{{$kehadiran->total_tanpaket}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelKehadiran').dataTable()
        });
    </script>
@endsection
