@extends('layouts.master',['activeMenu' => 'data-pribadi'])
@section('title','Data Pribadi Siswa')
@section('css')
<link rel="stylesheet" href="{{ asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">    
@endsection
@section('content')
    <section class="content-header">
        <h1>
            Data Pribadi Siswa
            <small>Data Pribadi Siswa</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{url('/')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Pribadi Siswa</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1" data-toggle="tab">Data Pribadi Siswa</a></li>
                                <li><a href="#tab_2" data-toggle="tab">Data Keluarga Siswa</a></li>
                                <li><a href="#tab_3" data-toggle="tab">Data Perkembangan Siswa</a></li>
                                <li><a href="#tab_4" data-toggle="tab">Data Setelah Pendidikan</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">
                                    <form action="{{url('siswa/data-pribadi-siswa/edit')}}" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="put">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Nama Lengkap</label>
                                                        <input type="text" class="form-control" name="nama_siswa_lengkap" placeholder="Masukkan Nama Lengkap"
                                                            value="{{ $siswa->nama_siswa_lengkap }}" readonly>
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jenis Kelamin</label>
                                                        <select name="jenis_kelamin" class="form-control" value="{{ $siswa->jenis_kelamin }}" readonly>
                                                            <option value="0">Pilih jenis kelamin</option>
                                                            <option value="Laki-laki" {{$siswa->jenis_kelamin == 'Laki-laki' ? 'selected' : ''}}>Laki-laki</option>
                                                            <option value="Perempuan" {{$siswa->jenis_kelamin == 'Perempuan' ? 'selected' : ''}}>Perempuan</option>
                                                        </select>
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12" id="ttl">
                                                    <label class="col-form-label">Tempat & Tanggal Lahir</label>
                                                    <div class="form-inline">
                                                        <input type="text" class="form-control" name="tempatLahir" placeholder="Tempat" value="{{ strtok($siswa->ttgl_lahir, ',') }}" readonly>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                              <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" name="tanggalLahir" id="tanggalLahir" value="{{substr($siswa->ttgl_lahir, strpos($siswa->ttgl_lahir, ",") + 1)}}" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12" style="margin-top: 15px">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Anak Ke</label>
                                                        <input type="number" min="0" class="form-control" name="anak_keberapa" value="{{$siswa->anak_keberapa}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jumlah Saudara Kandung</label>
                                                        <input type="number" min="0" class="form-control" name="jum_saudara_kandung" value="{{$siswa->jum_saudara_kandung}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jumlah Saudara Tiri</label>
                                                        <input type="number" min="0" class="form-control" name="jum_saudara_tiri" value="{{$siswa->jum_saudara_tiri}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jumlah Saudara Angkat</label>
                                                        <input type="number" min="0" class="form-control" name="jum_saudara_angkat" value="{{$siswa->jum_saudara_angkat}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Status Anak</label>
                                                        <select name="status_anak" id="status_anak" class="form-control" value="{{$siswa->status_anak}}" readonly>
                                                            <option value="kandung" {{$siswa->status_anak == 'kandung' ? 'selected' : ''}}>Kandung</option>
                                                            <option value="tiri" {{$siswa->status_anak == 'tiri' ? 'selected' : ''}}>Tiri</option>
                                                            <option value="angkat" {{$siswa->status_anak == 'angkat' ? 'selected' : ''}}>Angkat</option>
                                                        </select>
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Golongan Darah</label>
                                                        <select name="golongan_darah" id="golongan_darah" class="form-control" value="{{$siswa->golongan_darah}}" readonly>
                                                            <option value="A" {{$siswa->golongan_darah == 'A' ? 'selected' : ''}}>A</option>
                                                            <option value="B" {{$siswa->golongan_darah == 'B' ? 'selected' : ''}}>B</option>
                                                            <option value="O" {{$siswa->golongan_darah == 'O' ? 'selected' : ''}}>O</option>
                                                            <option value="AB" {{$siswa->golongan_darah == 'AB' ? 'selected' : ''}}>AB</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Tinggi Badan (Sentimeter)</label>
                                                        <input type="number" min="0" class="form-control" name="tinggi" value="{{$siswa->tinggi}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Berat Badan (Kilogram)</label>
                                                        <input type="number" min="0" class="form-control" min="0" name="berat_badan" value="{{$siswa->berat_badan}}" readonly>
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Penyakit Yang Pernah Diderita</label> <br>
                                                    <textarea style="width: 100%" class="form-control" name="penyakit_yang_pernah_diderita" rows="3" id="penyakit_yang_pernah_diderita" readonly>{{$siswa->penyakit_yang_pernah_diderita}}
                                                    </textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Kelainan Jasmani</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="kelainan_jasmani" rows="3" id="kelainan_jasmani" readonly>{{$siswa->kelainan_jasmani}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Alasan Pindah</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="alasan" rows="3" id="alasan" readonly>{{$siswa->alasan}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Kesenian</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="kesenian" rows="3" id="kesenian" readonly>{{$siswa->kesenian}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Olahraga</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="olahraga" rows="3" id="olahraga" readonly>{{$siswa->olahraga}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Foto</label>
                                                        <input type="file" id="fotoSiswa" name="fotoSiswa">
                                                    </div>
                                                </div>
                                               
                                            </div>
                
                                            <div class="col-xs-6">
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Nama Panggilan</label>
                                                        <input type="text" class="form-control" name="nama_siswa_panggilan" placeholder="Masukkan Nama Panggilan"
                                                        value="{{$siswa->nama_siswa_panggilan}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Agama</label>
                                                        <select name="agama" id="agama" class="form-control" value="{{$siswa->agama}}" readonly>
                                                            <option value="buddha" {{$siswa->agama == 'buddha' ? 'selected' : ''}}>Buddha</option>
                                                            <option value="hindu" {{$siswa->agama == 'hindu' ? 'selected' : ''}}>Hindu</option>
                                                            <option value="islam" {{$siswa->agama == 'islam' ? 'selected' : ''}}>Islam</option>
                                                            <option value="katolik" {{$siswa->agama == 'katolik' ? 'selected' : ''}}>Katolik</option>
                                                            <option value="protestan" {{$siswa->agama == 'protestan' ? 'selected' : ''}}>Protestan</option>
                                                            <option value="konghucu" {{$siswa->agama == 'konghucu' ? 'selected' : ''}}>Konghucu</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" id="kewarganegaraan">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Kewarganegaraan</label>
                                                        <input type="text" class="form-control" name="kewarganegaraan" placeholder="Masukkan kewarganegaraan" value="{{$siswa->kewarganegaraan}}" readonly>
                                                    </div>
                                                </div>
                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Bahasa Sehari-hari</label>
                                                        <input type="text" class="form-control" name="bahasa_seharihari" value="{{$siswa->bahasa_seharihari}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Alamat</label>
                                                        <input type="text" class="form-control" name="alamat" value="{{$siswa->alamat}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Telepon</label>
                                                        <input type="text" class="form-control" name="no_tlp" value="{{$siswa->no_tlp}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Tinggal Bersama</label>
                                                        <input type="text" class="form-control" name="tinggal_dgn" value="{{$siswa->tinggal_dgn}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jarak Kesekolah (Kilometer)</label>
                                                        <input type="number" min="0" class="form-control" name="jarak_kesekolah" value="{{$siswa->jarak_kesekolah}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Lulusan Dari</label>
                                                        <input type="text" class="form-control" min="0" name="lulusan_dari" value="{{$siswa->lulusan_dari}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Tanggal Lulus</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                              <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" id="tgl_lulus" name="tgl_lulus" value="{{$siswa->tgl_lulus}}" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">No. STTB</label>
                                                        <input type="text" class="form-control" name="no_sttb" value="{{$siswa->no_sttb}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Lama Belajar (Tahun)</label>
                                                        <input type="number" min="0" class="form-control" name="lama_belajar" value="{{$siswa->lama_belajar}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Pindahan Sekolah</label>
                                                        <input type="text" class="form-control" name="pindahan_sekolah" value="{{$siswa->pindahan_sekolah}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Diterima Kelas</label>
                                                        <select name="diterima_kls" id="diterima_kls" class="form-control" value="{{$siswa->diterima_kls}}" readonly>
                                                            <option value="X" {{$siswa->diterima_kls == 'X' ? 'selected' : ''}}>X</option>
                                                            <option value="XI" {{$siswa->diterima_kls == 'XI' ? 'selected' : ''}}>XI</option>
                                                            <option value="XII" {{$siswa->diterima_kls == 'XII' ? 'selected' : ''}}>XII</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Kelompok</label>
                                                        <input type="text" class="form-control" name="kelompok" value="{{$siswa->kelompok}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Jurusan</label>
                                                        <input type="text" class="form-control" name="jurusan" value="{{$siswa->jurusan}}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Tanggal Diterima</label>
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                              <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" class="form-control pull-right" id="tgl_diterima" name="tgl_diterima" value="{{$siswa->tgl_diterima}}" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Lainya</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="lainya" rows="3" id="lainya" readonly>{{$siswa->lainya}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label>Kemasyarakatan</label> <br>
                                                        <textarea style="width: 100%" class="form-control" name="kemasyarakatan" rows="3" id="kemasyarakatan" readonly>{{$siswa->kemasyarakatan}}</textarea>
                                                    </div>
                                                </div>
                                                
                
                                                <div class="col-xs-12">
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </div>
                
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    The European languages are members of the same family. Their separate existence is a myth.
                                    For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ
                                    in their grammar, their pronunciation and their most common words. Everyone realizes why a
                                    new common language would be desirable: one could refuse to pay expensive translators. To
                                    achieve this, it would be necessary to have uniform grammar, pronunciation and more common
                                    words. If several languages coalesce, the grammar of the resulting language is more simple
                                    and regular than that of the individual languages.
                                </div>
                                <div class="tab-pane" id="tab_3">
                                    Data Perkembangan Siswa
                                </div>
                                <div class="tab-pane" id="tab_4">
                                    Data Setelah Pendidikan
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
<script src="{{ asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
    $(function(){
        $('#tabelKehadiran').dataTable()
    });
    $('#tanggalLahir').datepicker({
        autoclose: true
    });
    $('#tgl_lulus').datepicker({
        autoclose: true
    });
    $('#tgl_diterima').datepicker({
        autoclose: true
    });
</script>
@endsection
