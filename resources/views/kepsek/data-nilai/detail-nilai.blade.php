@extends('layouts.master',['activeMenu' => 'nilai'])
@section('title','Daftar Nilai ')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
<section class="content-header">
    <h1>
        Nilai 
        <small>Daftar Nilai </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('admin')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Nilai </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="tabelNilai" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>NIS</th>
                                    <th>Siswa</th>
                                    <th>Mata Pelajaran</th>
                                    <th>Kode Rombel</th>
                                    <th>Nilai Pengetahuan</th>
                                    <th>Nilai Keterampilan</th>
                                    <th>Nilai Sikap</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($nilais as $nilai)
                                @php
                                    $mapel = $nilai->id_data_mata_pelajaran;
                                    $str = str_replace("_", " ",($nilai->id_data_mata_pelajaran));
                                    $str = strtoupper($str);
                                    $str = substr($str,2);

                                    $user = \App\User::where('nis',$nilai->nis)->first();
                                @endphp
                                    <tr>
                                        <td>
                                            {{$no++}}
                                        </td>
                                        <td>{{ $nilai->nis }}</td>
                                        <td>{{ $user->nama }}</td>
                                        <td>{{ $str }}</td>
                                        <td>{{ $nilai->kode_rombel }}</td>
                                        <td>{{ $nilai->nilai_pengetahuan }}</td>
                                        <td>{{ $nilai->nilai_keterampilan }}</td>
                                        <td>{{ $nilai->nilai_sikap }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelNilai').dataTable()
        });
    </script>
@endsection
