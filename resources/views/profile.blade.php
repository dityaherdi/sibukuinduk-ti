@extends('layouts.master',['activeMenu' => ''])
@section('title','Profile')

@section('content')
  <section class="content-header">
    <h1>
      Profile
      <small>My Profile</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{url('home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">My Profile</li>
    </ol>
  </section>
  @php
    $level = null;
    if ($user->level == 1) {
        $level = 'Kaur Kesiswaan';
    }elseif($user->level == 2){
        $level = 'Bimbingan Konseling';
    }elseif ($user->level == 3) {
        $level = 'Siswa';
    }elseif ($user->level == 4) {
        $level = 'Kepala Sekolah';
    }
  @endphp
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
          <div class="box-body box-profile">
            @if($user->avatar == null)
              <img src="{{asset('images/avatar5.png')}}" class="profile-user-img img-responsive img-circle" alt="User Image">
            @else
                <a href="javascript:void(0);" onClick="showImage('{{$user->avatar}}');">
                    <img src="{{asset('images/avatar/'.$user->avatar)}}" class="profile-user-img img-responsive img-circle"  alt="Lihat avatar">
                </a>
            @endif

            <h3 class="profile-username text-center">{{$user->nama}}</h3>

            <p class="text-muted text-center">{{$level}}</p>

            <ul class="list-group list-group-unbordered">
              @if ($user->level == 3)
                <li class="list-group-item">
                    <b>NIS</b> <a class="pull-right">{{$user->nis}}</a>
                </li>
              @else
                  
              @endif
              <li class="list-group-item">
                <b>Username</b> <a class="pull-right">{{$user->username}}</a>
              </li>
              <li class="list-group-item">
                <b>Level</b> <a class="pull-right">{{$level}}</a>
              </li>
            </ul>

            <form class="" action="{{url('profile/image/'.$user->id_user)}}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="_method" value="put">
              <div class="form-group">
                  <input type="file" class="form-control" name="avatar" style="height: 25%">
              </div>
              <button type="submit" class="btn btn-primary btn-block" onclick="saveBtn(this)">Ganti Foto Profile</button>
            </form>
          </div>
        </div>
      </div>

      <div class="col-md-9">
        <div class="box box-primary">
            <div class="nav-tabs-custom">
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
        
                    <div class="tab-pane">
                        <form class="form-horizontal" action="{{url('profile/'.$user->id_user)}}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="{{$user->nama}}" name="nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="{{$user->username}}" name="username">
                                </div>
                            </div>
            
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a href="javascript:void(0);" onclick="updatePassword('{{$user->id_user}}')" class="btn btn-default">Change Password</a>
                                    <button type="submit" class="btn btn-primary" onclick="saveBtn(this)"> <i class="fa fa-check"></i> Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</section>
<form class="hidden" action="" method="post" id="formPassword">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="password" value="" id="password">
</form>
@endsection
@section('js')
    <script src="{{asset('backend/plugins/bootbox/bootbox.min.js')}}"></script>
    <script type="text/javascript">
        function showImage(avatar){
            bootbox.dialog({
                message: '<img src="{{asset('images/avatar')}}/'+avatar+'" class="img-responsive">',
                closeButton: true,
                size: 'medium'
            });
        }
        function updatePassword(id_user){
            bootbox.prompt({
                title: 'Masukan password',
                inputType: 'password',
                size: 'small',
                callback: function(result){
                    if (result != null ) {
                        bootbox.prompt({
                        title: 'Masukan kembali password anda.',
                        inputType: 'password',
                        size: 'small',
                            callback: function(password){
                                if (password != null) {
                                    if (result == password) {
                                        $('#password').val(password);
                                        $('#formPassword').attr('action', '{{url('profile/password')}}/'+id_user);
                                        $('#formPassword').submit();
                                    }else {
                                        bootbox.alert("Password tidak sama. Silakan ulangi !");
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    </script>
@endsection
