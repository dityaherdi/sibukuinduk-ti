<?php

namespace App\Http\Controllers;

use Validator;
use App\DataRombel;
use App\User;
use App\DataJenisRombel;
use App\DataNilaiIjazah;
use App\DataMataPelajaran;
use Illuminate\Http\Request;

class IjazahController extends Controller
{
    public function index(){
        $rombels = DataJenisRombel::all();
        $siswas = User::where('level',3)->get();
        $jenis = DataJenisRombel::all();
        return view('kasek.ijazah.rombel', compact('rombels', 'siswas','jenis'));
    }

    public function detail($id) {
        $jenis = DataJenisRombel::where('id_data_jenis_rombel', $id)->first();
        $rombel = DataRombel::where('id_data_jenis_rombel', $id)->get();
        $siswas = User::whereIn('nis', $rombel->pluck('nis'))->get();
        
        return view('kasek.ijazah.detail', compact('siswas', 'jenis'));
    }

    public function nilai($nis) {
        $siswa = User::where('nis', $nis)->first();
        $nilaiIjazah = DataMataPelajaran::select('data_mata_pelajarans.*', 'data_nilai_ijazahs.*')
            ->join('data_nilai_ijazahs', 'data_nilai_ijazahs.id_data_mata_pelajaran', '=', 'data_mata_pelajarans.kode_mapel')
            ->where('data_nilai_ijazahs.nis', $nis)
            ->get();
        $mapels = DataMataPelajaran::where('kode_mapel', 'LIKE', '6_%')->get();

        // dd($nilaiIjazah);

        return view('kasek.ijazah.nilai', compact('siswa', 'nilaiIjazah', 'mapels'));
    }

    public function insert(Request $request) {
        $validator = Validator::make($request->all(), [
            'nis' => 'required',
            'id_data_mata_pelajaran' => 'required',
            'nilai_rata_rata' => 'required|numeric',
            'nilai_ujian' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        } else {
            $nilaiIjazah = DataNilaiIjazah::updateOrCreate([
                'id_data_mata_pelajaran' => $request->id_data_mata_pelajaran,
                'nis' => $request->nis
            ], [
                'nis' => $request->nis,
                'id_data_mata_pelajaran' => $request->id_data_mata_pelajaran,
                'nilai_rata_rata' => $request->nilai_rata_rata,
                'nilai_ujian' => $request->nilai_ujian,
            ]);
            toastr()->success('Data Nilai Ijazah baru berhasil ditambahkan!');
            return redirect(url('admin/ijazah/nilai/'.$request->nis));
        }
    }

    public function deleteOneNilai($id, $nis) {
        DataNilaiIjazah::where('id_data_nilai_ijazah', $id)->delete();
        toastr()->success('Nilai Ijazah berhasil dihapus!');
        return redirect(url('admin/ijazah/nilai/'.$nis));
    }
}
