<?php

namespace App\Http\Controllers;

use Validator;
use App\DataRombel;
use App\User;
use App\DataJenisRombel;
use App\DataNilai;
use Illuminate\Http\Request;

class DataNilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rombels = DataJenisRombel::all();
        $siswas = User::where('level',3)->get();
        $jenis = DataJenisRombel::all();
        return view('kasek.nilai.data-nilai', compact('rombels', 'siswas','jenis'));
    }

    public function detail($id){
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$id)->first();
        $rombels = DataRombel::where('id_data_jenis_rombel',$id)->get();
        return view('kasek.nilai.detail', compact('rombels', 'jenis'));
    }

    public function detailNilai($id){
        $rombel = DataRombel::where('nis',$id)->first();
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$rombel->id_data_jenis_rombel)->first();
        $nilais = DataNilai::where('nis',$id)->get();
        $user = User::where('nis',$id)->first();
        return view('kasek.nilai.detail-nilai', compact('rombel', 'jenis', 'nilais', 'user'));
    }

    public function editNilai($id){
        $nilai = DataNilai::where('id_data_nilai',$id)->first();
        $user = User::where('nis',$nilai->nis)->first();
        $mapel = $nilai->id_data_mata_pelajaran;
        $str = str_replace("_", " ",($nilai->id_data_mata_pelajaran));
        $str = strtoupper($str);
        $str = substr($str,2);
        return view('kasek.nilai.edit', compact('nilai','user','str'));
    }

    public function updateNilai(Request $r, $id){
        $val = Validator::make($r->all(),[
            'nilai_pengetahuan' => 'required|numeric',
            'nilai_keterampilan' => 'required|numeric',
            'nilai_sikap' => 'required'
        ]);

        if ($val->fails()) {
            toastError($val->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user = DataNilai::where('id_data_nilai',$id)->first();
            $nilai = DataNilai::where('id_data_nilai',$id)->update([
                'nilai_pengetahuan' => $r->nilai_pengetahuan,
                'nilai_keterampilan' => $r->nilai_keterampilan,
                'nilai_sikap' => $r->nilai_sikap
            ]);
            toastSuccess('Data Nilai berhasil diubah');
            return redirect(url('admin/data-nilai/detail-nilai/'.$user->nis));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataNilai  $dataNilai
     * @return \Illuminate\Http\Response
     */
    public function show(DataNilai $dataNilai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataNilai  $dataNilai
     * @return \Illuminate\Http\Response
     */
    public function edit(DataNilai $dataNilai)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataNilai  $dataNilai
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataNilai $dataNilai)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataNilai  $dataNilai
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataNilai $dataNilai)
    {
        //
    }

    public function kepsekDataNilai(){
        $rombels = DataJenisRombel::all();
        $jenis = DataJenisRombel::all();
        return view('kepsek.data-nilai.index', compact('rombels','jenis'));
    }

    public function kepsekDetailNilai($id){
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$id)->first();
        $rombels = DataRombel::where('id_data_jenis_rombel',$id)->get();
        return view('kepsek.data-nilai.detail', compact('rombels', 'jenis'));
    }
}
