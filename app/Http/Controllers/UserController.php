<?php

namespace App\Http\Controllers;

use Artisan;
use Image;
use Auth;
use Hash;
use Validator;
use App\User;
use App\DataPribadiSiswa;
use Illuminate\Http\Request;
use App\Imports\UserImport;
use App\Imports\DataPribadiSiswaImport;
use App\Imports\DataKeluargaSiswaImport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function resetDB(){
        Artisan::call('migrate:fresh');
        Artisan::call('db:seed');
        toastSuccess('Database berhasil di reset!');
        return redirect()->back();
    }
    public function index()
    {
        if (Auth::user()->level == 1) {
            $users = User::orderBy('level', 'ASC')->get();
            return view('kasek.user.index', compact('users'));
        }elseif (Auth::user()->level == 2) {
            $users = User::where('level',3)->get();
            return view('kasek.user.index', compact('users'));
        }
    }

    public function updateBio(Request $r, $id_user){
        $v = Validator::make($r->all(),[
            'nama' => 'required',
            'username' => 'required'
        ]);
        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user = User::where('id_user',$id_user)->update([
                'nama' => $r->nama,
                'username' => $r->username
            ]);
            toastSuccess('Profile anda berhasil di ubah!');
            return redirect()->back();
        }
    }

    public function changePasswordAuth(Request $r,$id_user){
        $user = User::where('id_user',$id_user)->update([
            'password' => Hash::make($r->password)
        ]);
        toastSuccess('Password berhasil diubah!');
        return redirect()->back();
    }

    public function changeAvatar(Request $r, $id_user){
        $v = Validator::make($r->all(),[
            'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back();
        }else{
            $a = User::where('id_user',$id_user)->first();
            if ($a->avatar == null) {
                $avatar = $r->file('avatar');
                $filename = time() . '.' . $avatar->getClientOriginalExtension();
                Image::make($avatar)->save(public_path('/images/avatar/'.$filename));
                $c = User::where('id_user',$id_user)->update([
                    'avatar' => $filename
                ]);
                toastSuccess('Avatar berhasil diganti!');
                return redirect()->back();
            }else{
                $avatar = $r->file('avatar');
                $filename = time() . '.' . $avatar->getClientOriginalExtension();
                unlink(public_path('/images/avatar/').$a->avatar);
                Image::make($avatar)->save(public_path('/images/avatar/'.$filename));
                $c = User::where('id_user',$id_user)->update([
                    'avatar' => $filename
                ]);
                toastSuccess('Avatar berhasil diganti!');
                return redirect()->back();
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $v = Validator::make($r->all(), [
            'nama' => 'required',
            'username' => 'required|unique:users',
            'password' => 'required',
            'level' => 'required',
            'status' => 'required'
        ]);

        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->nis == null) {
                $users = User::create([
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'password' => Hash::make($r->password),
                    'level' => $r->level,
                    'status' => $r->status
                ]);
                toastr()->success('User baru berhasil ditambahkan!');
                return redirect()->back();
            }else{
                $val = Validator::make($r->all(),[
                    'nis' => 'required|numeric|unique:users'
                ]);
                if ($val->fails()) {
                    toastError($val->messages()->first());
                    return redirect()->back()->withInput();
                }else{
                    $users = User::create([
                        'nama' => $r->nama,
                        'username' => $r->username,
                        'nis' => $r->nis,
                        'password' => Hash::make($r->password),
                        'level' => $r->level,
                        'status' => $r->status
                    ]);
                    $prib = DataPribadiSiswa::create([
                        'id_user' => $users->id_user,
                        'nama_siswa_lengkap' => $r->nama
                    ]);
                    toastr()->success('User baru berhasil ditambahkan!');
                    return redirect()->back();
                }
            }
        }
    }

    public function changePassword(Request $r, $id){
        $user = User::where('id_user',$id)->update([
            'password' => Hash::make($r->password)
        ]);
        toastSuccess('Password berhasil diubah!');
        return redirect()->back();
    }

    public function changeActive($id){
        $user = User::where('id_user',$id)->update([
            'status' => 1
        ]);
        toastSuccess('User berhasil diaktifkan!');
        return redirect()->back();
    }

    public function changeInnactive($id){
        $user = User::where('id_user',$id)->update([
            'status' => 0
        ]);
        toastWarning('User dinonaktifkan!');
        return redirect()->back();
    }

    public function indexProfile(){
        $user = User::where('id_user',Auth::user()->id_user)->first();
        return view('profile', compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function importExcel(Request $request) {
        try {
            Excel::import(new UserImport(), $request->file('userExcel'));
            Excel::import(new DataPribadiSiswaImport(), $request->file('userExcel'));
            // Excel::import(new DataKeluargaSiswaImport(), $request->file('userExcel'));
            
            toastSuccess('User berhasil diimport!');
        } catch (\Exception $e) {
            // Exception ketika gagal import
            // dd($e);
            toastError('Gagal import user, periksa kembali file excel anda!');
        }

        return redirect()->back();
    }
}
