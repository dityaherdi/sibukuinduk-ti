<?php

namespace App\Http\Controllers;

use Validator;
use App\DataEkstra;
use Illuminate\Http\Request;

class DataEkstraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ekstras = DataEkstra::all();
        return view('kasek.ekstra.index', compact('ekstras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kasek.ekstra.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nama_ekstra' => 'required',
            'pembina_ekstra' => 'required',
            'deskripsi_ekstra' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $ekstra = DataEkstra::create($request->all());
            toastr()->success('Ekstra baru berhasil ditambahkan!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataEkstra  $dataEkstra
     * @return \Illuminate\Http\Response
     */
    public function show(DataEkstra $dataEkstra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataEkstra  $dataEkstra
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ekstra = DataEkstra::where('id_data_ekstra',$id)->first();
        return view('kasek.ekstra.edit', compact('ekstra'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataEkstra  $dataEkstra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataEkstra $dataEkstra,$id)
    {
        $validator = Validator::make($request->all(),[
            'nama_ekstra' => 'required',
            'pembina_ekstra' => 'required',
            'deskripsi_ekstra' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            //dd($id);
            $ekstra = DataEkstra::where('id_data_ekstra',$id)->update([
                'nama_ekstra' => $request->nama_ekstra,
                'pembina_ekstra' => $request->pembina_ekstra,
                'deskripsi_ekstra' => $request->deskripsi_ekstra
            ]);
            toastr()->success('Ekstra berhasil diubah!');
            return redirect(url('admin/ekstra'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataEkstra  $dataEkstra
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataEkstra $dataEkstra)
    {
        //
    }
}
