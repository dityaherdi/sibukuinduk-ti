<?php

namespace App\Http\Controllers;

use App\DataSetelahPendidikan;
use Illuminate\Http\Request;

class DataSetelahPendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setelahPendidikans = DataSetelahPendidikan::all();
        return view('kasek.setelah-pendidikan.index', compact('setelahPendidikans'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataSetelahPendidikan  $dataSetelahPendidikan
     * @return \Illuminate\Http\Response
     */
    public function show(DataSetelahPendidikan $dataSetelahPendidikan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataSetelahPendidikan  $dataSetelahPendidikan
     * @return \Illuminate\Http\Response
     */
    public function edit(DataSetelahPendidikan $dataSetelahPendidikan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataSetelahPendidikan  $dataSetelahPendidikan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataSetelahPendidikan $dataSetelahPendidikan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataSetelahPendidikan  $dataSetelahPendidikan
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataSetelahPendidikan $dataSetelahPendidikan)
    {
        //
    }

    public function updatePendidikan(Request $r){
        $pendidikan = DataSetelahPendidikan::where('id_user', $r->id_user)->update([
            // masukkan field nya
            'melanjutkan_di_bekerja' => $r->melanjutkan_di_bekerja,
            'tgl_kerja' => $r->tgl_kerja,
            'nama_perusahaan_lembaga_dll' => $r->nama_perusahaan_lembaga_dll,
            'penghasilan' => $r->penghasilan
            
        ]);
        toastr()->success('Data berhasil diperbaharui!');
        return redirect()->back();
    }
}
