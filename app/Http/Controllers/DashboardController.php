<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\DataPribadiSiswa;
use App\DataJenisRombel;
use App\DataRombel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $dataSiswa = DataPribadiSiswa::all();
        $dataJenisKelamin = $dataSiswa->groupBy('jenis_kelamin')->map->count()->toArray();

        $countPane = [
            'siswas' => DataPribadiSiswa::all()->count(),
            'mm' => DataRombel::select('data_rombels.*','data_jenis_rombels.*')
                    ->join('data_jenis_rombels','data_jenis_rombels.id_data_jenis_rombel','=','data_rombels.id_data_jenis_rombel')
                    ->where('data_jenis_rombels.jurusan','=','MM')
                    ->count(),
            'rpl' => DataRombel::select('data_rombels.*','data_jenis_rombels.*')
                    ->join('data_jenis_rombels','data_jenis_rombels.id_data_jenis_rombel','=','data_rombels.id_data_jenis_rombel')
                    ->where('data_jenis_rombels.jurusan','=','RPL')
                    ->count(),
            'tkj' => DataRombel::select('data_rombels.*','data_jenis_rombels.*')
                    ->join('data_jenis_rombels','data_jenis_rombels.id_data_jenis_rombel','=','data_rombels.id_data_jenis_rombel')
                    ->where('data_jenis_rombels.jurusan','=','TKJ')
                    ->count(),
            'an' => DataRombel::select('data_rombels.*','data_jenis_rombels.*')
                    ->join('data_jenis_rombels','data_jenis_rombels.id_data_jenis_rombel','=','data_rombels.id_data_jenis_rombel')
                    ->where('data_jenis_rombels.jurusan','=','AN')
                    ->count(),
            'dkv' => DataRombel::select('data_rombels.*','data_jenis_rombels.*')
                    ->join('data_jenis_rombels','data_jenis_rombels.id_data_jenis_rombel','=','data_rombels.id_data_jenis_rombel')
                    ->where('data_jenis_rombels.jurusan','=','DKV')
                    ->count(),
        ];

        $dataAgama = $dataSiswa->groupBy('agama')->map->count()->toArray();
        


        // $rombels = DataJenisRombel::with(['rombel' => function($query) {
        //     $query->groupBy('nis')->count();
        // }])->get();
        // dd($rombels);


        $user = User::where('id_user',Auth::user()->id_user)->first();
        return view('dashboard', compact('user', 'dataJenisKelamin', 'dataAgama'), $countPane);
    }
}
