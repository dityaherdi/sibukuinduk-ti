<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\DataNilaiImport;
use App\Imports\DataMataPelajaranImport;
use App\Imports\DataEkstraImport;
use App\Imports\DataNilaiEkstraImport;
use App\Imports\DataKehadiranSiswaImport;
use Maatwebsite\Excel\Facades\Excel;

class UtilitiesImportController extends Controller
{
    public function index(){
        return view('kasek.master.index');
    }
    public function importExcel(Request $request) {
        // untuk testing
        // Excel::import(new DataNilaiEkstraImport(), $request->file('fileExcel'));

        try {
            // hilangkan komen jika mau import atau disesuaikan sesuai kebutuhan

            // Import mapel diperlukan utk master data ke tabel data nilai jadi harus di import / input manual dahulu
            // Excel::import(new DataMataPelajaranImport(), $request->file('fileExcel'));
            // Import mapel diperlukan utk master data ke tabel data nilai jadi harus di import / input manual dahulu

            Excel::import(new DataEkstraImport(), $request->file('fileExcel'));

            Excel::import(new DataNilaiImport(), $request->file('fileExcel'));
            Excel::import(new DataNilaiEkstraImport(), $request->file('fileExcel'));
            Excel::import(new DataKehadiranSiswaImport(), $request->file('fileExcel'));

            toastSuccess('Import berhasil!');
        } catch (\Exception $e) {
            // Exception ketika gagal import
            //dd($e);
            toastError('Gagal import, periksa kembali file excel anda!');
        }

        return redirect()->back();
    }
}
