<?php

namespace App\Http\Controllers;

use App\DataKeluargaSiswa;
use Illuminate\Http\Request;

class DataKeluargaSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keluargas = DataKeluargaSiswa::all();
        return view('kasek.keluarga.index', compact('keluargas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataKeluargaSiswa  $dataKeluargaSiswa
     * @return \Illuminate\Http\Response
     */
    public function show(DataKeluargaSiswa $dataKeluargaSiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataKeluargaSiswa  $dataKeluargaSiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(DataKeluargaSiswa $dataKeluargaSiswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataKeluargaSiswa  $dataKeluargaSiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataKeluargaSiswa $dataKeluargaSiswa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataKeluargaSiswa  $dataKeluargaSiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataKeluargaSiswa $dataKeluargaSiswa)
    {
        //
    }

    public function updateKeluarga(Request $r){
        $ttlAyah = $r->tempatLahirAyah.','.$r->tanggalLahirAyah;
        $ttlIbu = $r->tempatLahirIbu.','.$r->tanggalLahirIbu;
        $ttlWali = $r->tempatLahirWali.','.$r->tanggalLahirWali;
        $keluarga = DataKeluargaSiswa::where('id_user',$r->id_user)->update([
            'ttgl_lahir_ayah' => $ttlAyah,
            'ttgl_lahir_ibu' => $ttlIbu,
            'ttgl_lahir_wali' => $ttlWali,         
            'nama_ayah' => $r->nama_ayah,
            'agama_ayah' => $r->agama_ayah,
            'kewarganegaraan_ayah' => $r->kewarganegaraan_ayah,
            'pendidikan_ayah' => $r->pendidikan_ayah,
            'pekerjaan_ayah' => $r->pekerjaan_ayah,
            'penghasilan_bln_ayah' => $r->penghasilan_bln_ayah,
            'alamat_ayah' => $r->alamat_ayah,
            'tlp_ayah' => $r->tlp_ayah,
            'ket_ayah' => $r->ket_ayah,
            'nama_ibu' => $r->nama_ibu,
            'agama_ibu' => $r->agama_ibu,
            'kewarganegaraan_ibu' => $r->kewarganegaraan_ibu,
            'pendidikan_ibu' => $r->pendidikan_ibu,
            'pekerjaan_ibu' => $r->pekerjaan_ibu,
            'penghasilan_bln_ibu' => $r->penghasilan_bln_ibu,
            'alamat_ibu' => $r->alamat_ibu,
            'tlp_ibu' => $r->tlp_ibu,
            'ket_ibu' => $r->ket_ibu,
            'nama_wali' => $r->nama_wali,
            'agama_wali' => $r->agama_wali,
            'kewarganegaraan_wali' => $r->kewarganegaraan_wali,
            'pendidikan_wali' => $r->pendidikan_wali,
            'pekerjaan_wali' => $r->pekerjaan_wali,
            'penghasilan_bln_wali' => $r->penghasilan_bln_wali,
            'alamat_wali' => $r->alamat_wali,
            'tlp_wali' => $r->tlp_wali,
            'ket_wali' => $r->ket_wali

        ]);
        toastr()->success('Data berhasil diperbaharui!');
        return redirect()->back();
    }
}
