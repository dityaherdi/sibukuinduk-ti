<?php

namespace App\Http\Controllers;

use App\DataPerkembanganSiswa;
use Illuminate\Http\Request;

class DataPerkembanganSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perkembangans = DataPerkembanganSiswa::all();
        return view('kasek.perkembangan.index', compact('perkembangans'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataPerkembanganSiswa  $dataPerkembanganSiswa
     * @return \Illuminate\Http\Response
     */
    public function show(DataPerkembanganSiswa $dataPerkembanganSiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataPerkembanganSiswa  $dataPerkembanganSiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(DataPerkembanganSiswa $dataPerkembanganSiswa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataPerkembanganSiswa  $dataPerkembanganSiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataPerkembanganSiswa $dataPerkembanganSiswa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataPerkembanganSiswa  $dataPerkembanganSiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataPerkembanganSiswa $dataPerkembanganSiswa)
    {
        //
    }

    public function updatePerkembangan(Request $r){
        $perkembangan = DataPerkembanganSiswa::where('id_user', $r->id_user)->update([
            // masukkan field nya
            'menerima_beasiswa' => $r->menerima_beasiswa,
            'tgl_meninggalkan_sekolah' => date($r->tgl_meninggalkan_sekolah),
            'alasan_keluar' => $r->alasan_keluar,
            'thn_tamat_belajar' => $r->thn_tamat_belajar,
            'no_sttb_tamat' => $r->no_sttb_tamat
        ]);

        toastr()->success('Data berhasil diperbaharui!');
        return redirect()->back();
         // Masukan alert nya liat di controller lain nama nya toast
    }
}
