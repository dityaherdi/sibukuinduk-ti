<?php

namespace App\Http\Controllers;

use App\DataRombel;
use App\DataJenisRombel;
use App\DataPribadiSiswa;
use App\DataKeluargaSiswa;
use App\DataPerkembanganSiswa;
use App\DataSetelahPendidikan;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Auth;
use Image;

class DataPribadiSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswas = DataPribadiSiswa::all();
        return view('kasek.siswa.data-pribadi.index', compact('siswas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kasek.siswa.data-pribadi.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ttgl = $request->tempatLahir." ".$request->tanggalLahir;
        $tglLulus = Carbon::parse($request->tgl_lulus);
        $tglDiterima = Carbon::parse($request->tgl_diterima);

        $request->merge([
            'id_user' => Auth::user()->id, // disesuaikan jika siswa daftar sendiri
            'ttgl_lahir' => $ttgl,
            'tgl_lulus' => $tglLulus,
            'tgl_diterima' => $tglDiterima
        ]);

        if ($request->file('fotoSiswa')) {
            $v = Validator::make($request->all(), [
                'fotoSiswa' => 'mimes:jpeg,jpg,png|max:2000'
            ]);

            if ($v->fails()) {
                toastError('Gagal upload foto');
                return redirect()->back()->withInput();
            } else {
                $path = Storage::putFile('public/foto', $request->file('fotoSiswa'));
                $request->merge([
                    'foto' => $path
                ]);
            }
        }

        $v = Validator::make($request->all(), [
            'nama_siswa_lengkap' => 'required',
            'nama_siswa_panggilan' => 'required',
            'jenis_kelamin' => 'required',
            'ttgl_lahir' => 'required',
            'agama' => 'required',
            'kewarganegaraan' => 'required',
            'anak_keberapa' => 'required',
            'jum_saudara_kandung' => 'required|numeric',
            'jum_saudara_tiri' => 'required|numeric',
            'jum_saudara_angkat' => 'required|numeric',
            'status_anak' => 'required',
            'bahasa_seharihari' => 'required',
            'alamat' => 'required',
            'no_tlp' => 'required|numeric',
            'tinggal_dgn' => 'required',
            'jarak_kesekolah' => 'required|numeric',
            'golongan_darah' => 'required',
            'tinggi' => 'required|numeric',
            'berat_badan' => 'required|numeric',
            'lulusan_dari' => 'required',
            'tgl_lulus' => 'required',
            'no_sttb' => 'required',
            'lama_belajar' => 'required|numeric',
        ]);

        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        } else {
            DataPribadiSiswa::create($request->except(['tempatLahir', 'tanggalLahir', 'id_user']));

            toastr()->success('Siswa baru berhasil ditambahkan!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataPribadiSiswa  $dataPribadiSiswa
     * @return \Illuminate\Http\Response
     */
    public function show(DataPribadiSiswa $dataPribadiSiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataPribadiSiswa  $dataPribadiSiswa
     * @return \Illuminate\Http\Response
     */
    public function edit(DataPribadiSiswa $dataPribadiSiswa)
    {
        return view('kasek.siswa.data-pribadi.form', compact('dataPribadiSiswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataPribadiSiswa  $dataPribadiSiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataPribadiSiswa $dataPribadiSiswa)
    {
        // dd($request->all(), $dataPribadiSiswa);
        $ttgl = $request->tempatLahir." ".$request->tanggalLahir;
        $tglLulus = Carbon::parse($request->tgl_lulus);
        $tglDiterima = Carbon::parse($request->tgl_diterima);

        $request->merge([
            // 'id_user' => Auth::user()->id,
            'ttgl_lahir' => $ttgl,
            'tgl_lulus' => $tglLulus,
            'tgl_diterima' => $tglDiterima
        ]);

        if ($request->file('fotoSiswa')) {
            $v = Validator::make($request->all(), [
                'fotoSiswa' => 'mimes:jpeg,jpg,png|max:2000'
            ]);

            if ($v->fails()) {
                toastError('Gagal upload foto');
                return redirect()->back()->withInput();
            } else {
                $path = Storage::putFile('public/foto', $request->file('fotoSiswa'));
                $request->merge([
                    'foto' => $path
                ]);
            }
        }

        $v = Validator::make($request->all(), [
            'nama_siswa_lengkap' => 'required',
            'nama_siswa_panggilan' => 'required',
            'jenis_kelamin' => 'required',
            'ttgl_lahir' => 'required',
            'agama' => 'required',
            'kewarganegaraan' => 'required',
            'anak_keberapa' => 'required',
            'jum_saudara_kandung' => 'required|numeric',
            'jum_saudara_tiri' => 'required|numeric',
            'jum_saudara_angkat' => 'required|numeric',
            'status_anak' => 'required',
            'bahasa_seharihari' => 'required',
            'alamat' => 'required',
            'no_tlp' => 'required|numeric',
            'tinggal_dgn' => 'required',
            'jarak_kesekolah' => 'required|numeric',
            'golongan_darah' => 'required',
            'tinggi' => 'required|numeric',
            'berat_badan' => 'required|numeric',
            'lulusan_dari' => 'required',
            'tgl_lulus' => 'required',
            'no_sttb' => 'required',
            'lama_belajar' => 'required|numeric',
        ]);

        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        } else {
            DataPribadiSiswa::where('id_data_pribadi_siswa', $dataPribadiSiswa->id_data_pribadi_siswa)
                ->update($request->except(['tempatLahir', 'tanggalLahir', '_token', '_method', 'id_user']));

            toastr()->success('Siswa baru berhasil diperbarui!');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataPribadiSiswa  $dataPribadiSiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DataPribadiSiswa::where('id_data_pribadi_siswa', $id)->delete();

        return redirect()->back();
    }

    
    /**
     * 
     * SISWA FUNCTION
     * 
     */
    public function dataSiswa(){
        return view('siswa.data-pribadi');
    }

    public function dataPribadiSiswa(){
        $rombels = DataJenisRombel::all();
        $jenis = DataJenisRombel::all();
        return view('kasek.data-pribadi.rombel', compact('rombels','jenis'));
    }

    public function detail($id){
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$id)->first();
        $siswas = DataRombel::select('data_rombels.*','data_pribadi_siswas.*')
                  ->join('data_pribadi_siswas','data_pribadi_siswas.id_user','=','data_rombels.id_user')
                  ->where('data_rombels.id_data_jenis_rombel','=',$id)
                  ->get();
        return view('kasek.data-pribadi.index', compact('siswas', 'jenis'));
    }

    public function editDataPribadiSiswa($id){
        $siswa = DataPribadiSiswa::where('id_user',$id)->first();
        $keluarga = DataKeluargaSiswa::where('id_user',$id)->first();
        $perkembangan = DataPerkembanganSiswa::where('id_user',$id)->first();
        $pendidikan = DataSetelahPendidikan::where('id_user',$id)->first();
        return view('kasek.data-pribadi.edit', compact('siswa', 'keluarga', 'perkembangan', 'pendidikan'));
    }

    public function updateSiswa(Request $r){
        $ttl = $r->tempatLahir.','.$r->tanggalLahir;
        if ($r->hasFile('fotoSiswa')) {
            $fotoSiswa = $r->file('fotoSiswa');
            $filename = time() . '.' . $fotoSiswa->getClientOriginalExtension();
            Image::make($fotoSiswa)->save(public_path('/images/fotosiswa/'.$filename));
            $siswa = DataPribadiSiswa::where('id_user', $r->id_user)->update([
                'nama_siswa_lengkap' => $r->nama_siswa_lengkap,
                'nama_siswa_panggilan' => $r->nama_siswa_panggilan,
                'ttgl_lahir' => $ttl,
                'jenis_kelamin' => $r->jenis_kelamin,
                'agama' => $r->agama,
                'kewarganegaraan' => $r->kewarganegaraan,
                'anak_keberapa' => $r->anak_keberapa,
                'jum_saudara_kandung' => $r->jum_saudara_kandung,
                'jum_saudara_tiri' => $r->jum_saudara_tiri,
                'jum_saudara_angkat' => $r->jum_saudara_angkat,
                'status_anak' => $r->status_anak,
                'bahasa_seharihari' => $r->bahasa_seharihari,
                'alamat' => $r->alamat,
                'no_tlp' => $r->no_tlp,
                'tinggal_dgn' => $r->tinggal_dgn,
                'jarak_kesekolah' => $r->jarak_kesekolah,
                'golongan_darah' => $r->golongan_darah,
                'penyakit_yang_pernah_diderita' => $r->penyakit_yang_pernah_diderita,
                'kelainan_jasmani' => $r->kelainan_jasmani,
                'tinggi' => $r->tinggi,
                'berat_badan' => $r->berat_badan,
                'lulusan_dari' => $r->lulusan_dari,
                'tgl_lulus' => date('Y-m-d',strtotime($r->tgl_lulus)),
                'no_sttb' => $r->no_sttb,
                'lama_belajar' => $r->lama_belajar,
                'pindahan_sekolah' => $r->pindahan_sekolah,
                'alasan' => $r->alasan,
                'diterima_kls' => $r->diterima_kls,
                'kelompok' => $r->kelompok,
                'jurusan' => $r->jurusan,
                'tgl_diterima' => date('Y-m-d',strtotime($r->tgl_diterima)),
                'kesenian' => $r->kesenian,
                'olahraga' => $r->olahraga,
                'kemasyarakatan' => $r->kemasyarakatan,
                'lainya' => $r->lainya,
                'foto' => $filename

            ]);

            toastr()->success('Data berhasil diperbaharui!');
            return redirect()->back();
        }else{
            $siswa = DataPribadiSiswa::where('id_user', $r->id_user)->update([
                'nama_siswa_lengkap' => $r->nama_siswa_lengkap,
                'nama_siswa_panggilan' => $r->nama_siswa_panggilan,
                'ttgl_lahir' => $ttl,
                'jenis_kelamin' => $r->jenis_kelamin,
                'agama' => $r->agama,
                'kewarganegaraan' => $r->kewarganegaraan,
                'anak_keberapa' => $r->anak_keberapa,
                'jum_saudara_kandung' => $r->jum_saudara_kandung,
                'jum_saudara_tiri' => $r->jum_saudara_tiri,
                'jum_saudara_angkat' => $r->jum_saudara_angkat,
                'status_anak' => $r->status_anak,
                'bahasa_seharihari' => $r->bahasa_seharihari,
                'alamat' => $r->alamat,
                'no_tlp' => $r->no_tlp,
                'tinggal_dgn' => $r->tinggal_dgn,
                'jarak_kesekolah' => $r->jarak_kesekolah,
                'golongan_darah' => $r->golongan_darah,
                'penyakit_yang_pernah_diderita' => $r->penyakit_yang_pernah_diderita,
                'kelainan_jasmani' => $r->kelainan_jasmani,
                'tinggi' => $r->tinggi,
                'berat_badan' => $r->berat_badan,
                'lulusan_dari' => $r->lulusan_dari,
                'tgl_lulus' => date('Y-m-d',strtotime($r->tgl_lulus)),
                'no_sttb' => $r->no_sttb,
                'lama_belajar' => $r->lama_belajar,
                'pindahan_sekolah' => $r->pindahan_sekolah,
                'alasan' => $r->alasan,
                'diterima_kls' => $r->diterima_kls,
                'kelompok' => $r->kelompok,
                'jurusan' => $r->jurusan,
                'tgl_diterima' => date('Y-m-d',strtotime($r->tgl_diterima)),
                'kesenian' => $r->kesenian,
                'olahraga' => $r->olahraga,
                'kemasyarakatan' => $r->kemasyarakatan,
                'lainya' => $r->lainya

            ]);

            toastr()->success('Data berhasil diperbaharui!');
            return redirect()->back();
        }
    }

   

    // KEPSEK PUNYA

    public function kepsekDataSiswa(){
        $siswas = DataPribadiSiswa::all();
        return view('kepsek.data-pribadi.index', compact('siswas'));
    }

    public function showDataPribadiSiswa($id){
        $siswa = DataPribadiSiswa::where('id_user',$id)->first();
        return view('kepsek.data-pribadi.data-pribadi', compact('siswa'));
    }
}
