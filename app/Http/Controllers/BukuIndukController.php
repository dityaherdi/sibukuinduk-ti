<?php

namespace App\Http\Controllers;

use PDF;
use DB;
use App\User;
use App\DataPribadiSiswa;
use App\DataKeluargaSiswa;
use App\DataPerkembanganSiswa;
use App\DataSetelahPendidikan;
use App\DataKehadiranSiswa;
use App\DataMataPelajaran;
use App\DataNilai;
use App\DataRombel;
use App\DataNilaiEkstra;
use App\DataJenisRombel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Http\Request;

class BukuIndukController extends Controller
{
    public function tabelBukuInduk(){
        $rombels = DataJenisRombel::all();
        return view('kasek.buku-induk.tabel-buku-induk', compact('rombels'));
    }

    public function downloadTabelBukuInduk($id_jenis_rombel){
        $spreadsheet = new Spreadsheet();
        $siswas = DataRombel::select('data_rombels.*','users.*','data_pribadi_siswas.*')
        ->join('users', 'users.id_user','=','data_rombels.id_user')
        ->join('data_pribadi_siswas', 'data_pribadi_siswas.id_user','=','data_rombels.id_user')
        ->where('data_rombels.id_data_jenis_rombel',$id_jenis_rombel)->get();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'NIS');
        $sheet->setCellValue('C1', 'Nama Lengkap');
        $sheet->setCellValue('D1', 'Nama Panggilan');
        $sheet->setCellValue('E1', 'Jenis Kelamin');
        $sheet->setCellValue('F1', 'Tempat Tanggal Lahir');
        $sheet->setCellValue('G1', 'Agama');
        $sheet->setCellValue('H1', 'Kewarganegaraan');
        $sheet->setCellValue('I1', 'Anak Keberapa');
        $sheet->setCellValue('J1', 'Jumlah Saudara Kandung');
        $sheet->setCellValue('K1', 'Jumlah Saudara Tiri');
        $sheet->setCellValue('L1', 'Jumlah Saudara Angkat');
        $sheet->setCellValue('M1', 'Status Anak');
        $sheet->setCellValue('N1', 'Bahasa Sehari-hari');
        $sheet->setCellValue('O1', 'Alamat');
        $sheet->setCellValue('P1', 'No Telp');
        $sheet->setCellValue('Q1', 'Tinggal Dengan');
        $sheet->setCellValue('R1', 'Jarak Kesekolah');
        $sheet->setCellValue('S1', 'Golongan Darah');
        $sheet->setCellValue('T1', 'Penyakit Yang Pernah Diderita');
        $sheet->setCellValue('U1', 'Kelainan Jasmani');
        $sheet->setCellValue('V1', 'Tinggi');
        $sheet->setCellValue('W1', 'Berat Badan');
        $sheet->setCellValue('X1', 'Lulusan Dari');
        $sheet->setCellValue('Y1', 'Tanggal Lulus');
        $sheet->setCellValue('Z1', 'No STTB');
        $sheet->setCellValue('AA1', 'Lama Belajar');
        $sheet->setCellValue('AB1', 'Pindahan Sekolah');
        $sheet->setCellValue('AC1', 'Alasan');
        $sheet->setCellValue('AD1', 'Diterima Kelas');
        $sheet->setCellValue('AE1', 'Kelompok');
        $sheet->setCellValue('AF1', 'Jurusan');
        $sheet->setCellValue('AG1', 'Tanggal Diterima');
        $sheet->setCellValue('AH1', 'Kesenian');
        $sheet->setCellValue('AI1', 'Olah Raga');
        $sheet->setCellValue('AJ1', 'Kemasyarakatan');
        $sheet->setCellValue('AK1', 'Lainnya');
        $sheet->setCellValue('AL1', 'Foto');

        $row = 2;
        $nomor = 1;
        foreach($siswas as $siswa){
            $sheet->setCellValue('A'.$row,$nomor++);
            $sheet->setCellValue('B'.$row,$siswa->nis);
            $sheet->setCellValue('C'.$row,$siswa->nama_siswa_lengkap);
            $sheet->setCellValue('D'.$row,$siswa->nama_siswa_panggilan);
            $sheet->setCellValue('E'.$row,$siswa->jenis_kelamin);
            $sheet->setCellValue('F'.$row,$siswa->ttgl_lahir);
            $sheet->setCellValue('G'.$row,$siswa->agama);
            $sheet->setCellValue('H'.$row,$siswa->kewarganegaraan);
            $sheet->setCellValue('I'.$row,$siswa->anak_keberapa);
            $sheet->setCellValue('J'.$row,$siswa->jum_saudara_kandung);
            $sheet->setCellValue('K'.$row,$siswa->jum_saudara_tiri);
            $sheet->setCellValue('L'.$row,$siswa->jum_saudara_angkat);
            $sheet->setCellValue('M'.$row,$siswa->status_anak);
            $sheet->setCellValue('N'.$row,$siswa->bahasa_seharihari);
            $sheet->setCellValue('O'.$row,$siswa->alamat);
            $sheet->setCellValue('P'.$row,$siswa->no_tlp);
            $sheet->setCellValue('Q'.$row,$siswa->tinggal_dgn);
            $sheet->setCellValue('R'.$row,$siswa->jarak_sekolah);
            $sheet->setCellValue('S'.$row,$siswa->golongan_darah);
            $sheet->setCellValue('T'.$row,$siswa->penyakit_yang_pernah_diderita);
            $sheet->setCellValue('U'.$row,$siswa->kelainan_jasmani);
            $sheet->setCellValue('V'.$row,$siswa->tinggi);
            $sheet->setCellValue('W'.$row,$siswa->berat_badan);
            $sheet->setCellValue('X'.$row,$siswa->lulusan_dari);
            $sheet->setCellValue('Y'.$row,$siswa->tgl_lulus);
            $sheet->setCellValue('Z'.$row,$siswa->no_sttb);
            $sheet->setCellValue('AA'.$row,$siswa->lama_belajar);
            $sheet->setCellValue('AB'.$row,$siswa->pindahan_sekolah);
            $sheet->setCellValue('AC'.$row,$siswa->alasan);
            $sheet->setCellValue('AD'.$row,$siswa->diterima_kls);
            $sheet->setCellValue('AE'.$row,$siswa->kelompok);
            $sheet->setCellValue('AF'.$row,$siswa->jurusan);
            $sheet->setCellValue('AG'.$row,$siswa->tgl_diterima);
            $sheet->setCellValue('AH'.$row,$siswa->kesenian);
            $sheet->setCellValue('AI'.$row,$siswa->olahraga);
            $sheet->setCellValue('AJ'.$row,$siswa->kemasyarakatan);
            $sheet->setCellValue('AK'.$row,$siswa->lainya);
            $sheet->setCellValue('AL'.$row,$siswa->foto);
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('TabelBukuInduk.xlsx');
        return response()->download(public_path('TabelBukuInduk.xlsx'))->deleteFileAfterSend();
    }
    public function lembarBukuInduk() {
        $rombels = DataJenisRombel::all();
        $jenis = DataJenisRombel::all();
        return view('kasek.buku-induk.rombel', compact('rombels', 'jenis'));
    }

    public function detailLembarBukuInduk($id) {
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$id)->first();
        $siswas = DataRombel::select('data_rombels.*','data_pribadi_siswas.*')
                  ->join('data_pribadi_siswas','data_pribadi_siswas.id_user','=','data_rombels.id_user')
                  ->where('data_rombels.id_data_jenis_rombel','=',$id)
                  ->get();
        return view('kasek.buku-induk.lembar-buku-induk', compact('siswas', 'jenis'));
    }

    public function exportBukuInduk($id_user) {
        $siswa = DataPribadiSiswa::where('id_user', $id_user)->first();
        $keluarga = DataKeluargaSiswa::where('id_user', $id_user)->first();
        $perkembangan = DataPerkembanganSiswa::where('id_user', $id_user)->first();
        $setPendidikan = DataSetelahPendidikan::where('id_user', $id_user)->first();

        // cek is null
        // dd($siswa,$keluarga,$perkembangan,$setPendidikan);

        return view('kasek.buku-induk.buku-induk', compact('siswa', 'keluarga', 'perkembangan', 'setPendidikan'));
    }

    public function hasilBelajarSiswa() {
        $siswas = DataPribadiSiswa::all();
        $rombels = DataJenisRombel::orderBy('nama_rombel', 'ASC')->get();
        // dd($rombels);
        return view('kasek.buku-induk.tabel-lembar-nilai', compact('siswas', 'rombels'));
    }

    public function laporanHasilBelajar()
    {

        $rombels = DataJenisRombel::all();
        // dd($rombels);
        $siswas = User::where('level',3)->get();
        $jenis = DataJenisRombel::all();
        return view('kasek.buku-induk.laporan-hasil-belajar', compact('rombels', 'siswas','jenis'));
    }

    public function printLaporanHasilBelajar($id){
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$id)->first();
        $rombels = DataRombel::where('id_data_jenis_rombel',$id)->get();
        $nilais = DataNilai::where('kode_rombel',$jenis->nama_rombel)->get();
        return view('kasek.buku-induk.print-hasil-belajar', compact('rombels', 'jenis','nilais'));
    }

    public function searchSemester(){
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$id)->first();
        $rombels = DataRombel::where('id_data_jenis_rombel',$id)->get();
        return view('kasek.buku-induk.print-hasil-belajar', compact('rombels', 'jenis'));
    }

    public function detailNilai($id){
        $rombel = DataRombel::where('nis',$id)->first();
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$rombel->id_data_jenis_rombel)->first();
        $nilais = DataNilai::where('nis',$id)->get();
        $user = User::where('nis',$id)->first();
        return view('kasek.buku-induk.detail-nilai', compact('rombel', 'jenis', 'nilais', 'user'));
    }

    public function dsearch(Request $r){
        $user = User::where('id_user',$r->id_user)->first();
        $rombel = DataRombel::where('nis',$user->nis)->first();
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$rombel->id_data_jenis_rombel)->first();
        $nilais = DataNilai::where('nis',$user->nis)->get();
        $semester = $r->search;
        return view('kasek.buku-induk.export-nilai', compact('rombel', 'jenis', 'nilais', 'user', 'semester'));
    }

    public function exportLembarNilai($id_user, $rombelId, $semesterMapel) {
        $rombel = DataJenisRombel::where('id_data_jenis_rombel', $rombelId)->first();
        $siswa = User::where('id_user', $id_user)->first();
        
        $nilaiSiswa = DataNilai::where('nis', $siswa->nis)
                    ->where('kode_rombel', $rombel->nama_rombel)
                    ->where('id_data_mata_pelajaran', 'LIKE', $semesterMapel.'%')
                    ->get();

        $kehadiran = DataKehadiranSiswa::where('nis', $siswa->nis)->first();
        $ekstras = DataNilaiEkstra::where('nis', $siswa->nis)->with('ekstras')->get();
        $mapelSearchKkm = $nilaiSiswa->pluck('id_data_mata_pelajaran');
        $mapel = DataMataPelajaran::whereIn('kode_mapel', $mapelSearchKkm)->get();

        // dd($mapelSearchKkm, $mapel->pluck('kode_mapel'));

        $nilai = [];
        $semester = null;
        foreach ($nilaiSiswa as $key => $value) {
            $semester = explode('_', $value['id_data_mata_pelajaran'])[0];
            $kkm = $mapel->where('kode_mapel', $value['id_data_mata_pelajaran'])->first()['kkm'];
            $namaMapel = $mapel->where('kode_mapel', $value['id_data_mata_pelajaran'])->first()['nama_mp'];

            $nilaiPerMapel = [
                'nama_mapel' => $namaMapel,
                'kkm' => $kkm,
                'pengetahuan' => $value['nilai_pengetahuan'],
                'keterampilan' => $value['nilai_keterampilan'],
                'sikap' => $value['nilai_sikap']
            ];

            array_push($nilai, $nilaiPerMapel);
        }

        // dd($nilai, $kehadiran, $siswa, $rombel, $semester, $ekstras);
        // return view('kasek.buku-induk.raport', compact('siswa','nilaiSiswa','rombel'));

        $pdf = PDF::loadview('kasek.buku-induk.raport-new', 
        [
            'nilai' => $nilai,
            'kehadiran' => $kehadiran,
            'siswa' => $siswa,
            'rombel' => $rombel,
            'semester' => $semester,
            'ekstras' => $ekstras
        ]);
        return $pdf->stream('raport-siswa');
        // compact('nilai', 'kehadiran', 'siswa', 'rombel', 'semester', 'ekstras'));
    }
}
