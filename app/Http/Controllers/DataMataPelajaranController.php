<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Str;
use App\DataMataPelajaran;
use Illuminate\Http\Request;

class DataMataPelajaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mapels = DataMataPelajaran::all();
        return view('kasek.mapel.index', compact('mapels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kasek.mapel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nama_mp' => 'required',
            'semester' => 'required|numeric',
            'jurusan' => 'required',
            'kkm' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $kode_mapel = $request->semester.'_'.Str::snake($request->nama_mp);
            $mapel = DataMataPelajaran::where('kode_mapel', $kode_mapel)->first();
            if ($mapel == null) {
                $mp = DataMataPelajaran::create([
                    'kode_mapel' => $kode_mapel,
                    'nama_mp' => $request->nama_mp,
                    'semester' => $request->semester,
                    'jurusan' => $request->jurusan,
                    'kkm' => $request->kkm
                ]);

                toastr()->success('Mata Pelajaran baru berhasil ditambahkan!');
                return redirect()->back();
            }else{
                toastError('Data mata pelajaran sudah ada!');
                return redirect()->back()->withInput();
            }
            // dd($kode_mapel);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataMataPelajaran  $dataMataPelajaran
     * @return \Illuminate\Http\Response
     */
    public function show(DataMataPelajaran $dataMataPelajaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataMataPelajaran  $dataMataPelajaran
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mapel = DataMataPelajaran::where('id_data_mata_pelajaran',$id)->first();
        return view('kasek.mapel.edit', compact('mapel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataMataPelajaran  $dataMataPelajaran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'nama_mp' => 'required',
            'semester' => 'required|numeric',
            'jurusan' => 'required',
            'kkm' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $kode_mapel = $request->semester.'_'.Str::snake($request->nama_mp);
            $mp = DataMataPelajaran::where('id_data_mata_pelajaran',$id)->update([
                'kode_mapel' => $kode_mapel,
                'nama_mp' => $request->nama_mp,
                'semester' => $request->semester,
                'jurusan' => $request->jurusan,
                'kkm' => $request->kkm
            ]);

            toastr()->success('Mata Pelajaran baru berhasil diubah!');
            return redirect(url('admin/mata-pelajaran'));
        }
            // dd($kode_mapel);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataMataPelajaran  $dataMataPelajaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataMataPelajaran $dataMataPelajaran)
    {
        //
    }
}
