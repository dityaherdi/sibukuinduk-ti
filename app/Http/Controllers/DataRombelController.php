<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\DataRombel;
use App\User;
use App\DataJenisRombel;
use Illuminate\Http\Request;
use App\Imports\DataRombelImport;
use Maatwebsite\Excel\Facades\Excel;

class DataRombelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $rombels = DataRombel::select('data_rombels.*','users.*','data_jenis_rombels.*')
        //             ->join('users','users.nis','=','data_rombels.nis')
        //             ->join('data_jenis_rombels','data_jenis_rombels.id_data_jenis_rombel','=','data_rombels.id_data_jenis_rombel')
        //             ->groupBy('data_rombels.id_data_jenis_rombel')
        //             // ->having('data_rombels.id_data_jenis_rombel','<',50)
        //             ->get();

        $rombels = DataJenisRombel::all();
        // dd($rombels);
        $siswas = User::where('level',3)->get();
        $jenis = DataJenisRombel::all();
        return view('kasek.rombel.index', compact('rombels', 'siswas','jenis'));
    }

    public function detail($id){
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$id)->first();
        $rombels = DataRombel::where('id_data_jenis_rombel',$id)->get();
        return view('kasek.rombel.detail', compact('rombels', 'jenis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $siswas = User::where('level',3)->get();
        $jenis = DataJenisRombel::all();
        return view('kasek.rombel.create', compact('siswas','jenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id_data_jenis_rombel' => 'required',
            'id_user' => 'required'
            // 'thn_ajaran' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user = User::where('id_user',$request->id_user)->first();
            $rombel = DataRombel::create([
                'id_data_jenis_rombel' => $request->id_data_jenis_rombel,
                'id_user' => $request->id_user,
                'nis' => $user->nis
                // 'thn_ajaran' => $request->thn_ajaran
            ]);
            toastr()->success('Data Rombel baru berhasil ditambahkan!');
            return redirect()->back();

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataRombel  $dataRombel
     * @return \Illuminate\Http\Response
     */
    public function show(DataRombel $dataRombel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataRombel  $dataRombel
     * @return \Illuminate\Http\Response
     */
    public function edit(DataRombel $dataRombel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataRombel  $dataRombel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataRombel $dataRombel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataRombel  $dataRombel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_data_rombel)
    {
        DataRombel::where('id_data_rombel', $id_data_rombel)->delete();
        toastr()->success('Data Rombel berhasil dihapus!');
        return redirect()->back();
    }

    public function import(Request $request) {
        // dd($request->all());
        try {
            Excel::import(new DataRombelImport($request->jenisRombel), $request->file('rombelExcel'));
            toastSuccess('Rombel berhasil diimport!');
        } catch (Exception $e) {
            // dd($e);
            toastError('Gagal Import Rombel!');
        }

        return redirect()->back();
    }
}
