<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {
       $loginField = \is_numeric($request['email']) ? 'nis' : 'username';
       
       return [
           $loginField => $request->get($this->username()),
           'password' => $request['password'],
           'status' => 1
       ];
    }

    // Kode alternatif untuk fungsi credentials
    // protected function credentials(Request $request)
    // {
    //     if(is_numeric($request->get('email'))){
    //         return ['nis'=>$request->get('email'),'password'=>$request->get('password')];
    //     }else{
    //         return ['username' => $request->get('email'), 'password'=>$request->get('password')];
    //     }
    //     return $request->only($this->username(), 'password');
    // }
}
