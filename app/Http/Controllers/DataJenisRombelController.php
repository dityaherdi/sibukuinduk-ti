<?php

namespace App\Http\Controllers;

use Validator;
use Str;
use App\DataJenisRombel;
use Illuminate\Http\Request;

class DataJenisRombelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenisRombels = DataJenisRombel::all();
        return view('kasek.jenis-rombel.index', compact('jenisRombels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $val = Validator::make($request->all(),[
            'nama_rombel' => 'required',
            'jurusan' => 'required',
            'thn_ajaran' => 'required'
        ]);

        if ($val->fails()) {
            toastError($val->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $rombel = new DataJenisRombel;
            $rombel->nama_rombel = $request->nama_rombel;
            $rombel->jurusan = $request->jurusan;
            $rombel->thn_ajaran = $request->thn_ajaran;
            $rombel->save();
            toastr()->success('Jenis Rombel baru berhasil ditambahkan!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataJenisRombel  $dataJenisRombel
     * @return \Illuminate\Http\Response
     */
    public function show(DataJenisRombel $dataJenisRombel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataJenisRombel  $dataJenisRombel
     * @return \Illuminate\Http\Response
     */
    public function edit(DataJenisRombel $dataJenisRombel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataJenisRombel  $dataJenisRombel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $val = Validator::make($r->all(),[
            'nama_rombel' => 'required',
            'jurusan' => 'required',
            'thn_ajaran' => 'required'
        ]);

        if ($val->fails()) {
            toastError($val->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $rom = DataJenisRombel::where('id_data_jenis_rombel',$id)->update([
                'jurusan' => $r->jurusan,
                'thn_ajaran' => $r->thn_ajaran
            ]);
            toastr()->success('Jenis rombel berhasil diubah!');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataJenisRombel  $dataJenisRombel
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataJenisRombel $dataJenisRombel)
    {
        //
    }
}
