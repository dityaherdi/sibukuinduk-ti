<?php

namespace App\Http\Controllers;

use Validator;
use App\DataRombel;
use App\User;
use App\DataJenisRombel;
use Illuminate\Http\Request;

class DataKelasController extends Controller
{
    public function index(){
        $rombels = DataRombel::all();
        $jenis = DataJenisRombel::all();
        return view('kasek.kelas.rombel', compact('rombels', 'jenis'));
    }

    public function detailSiswa(){

    }

    public function activeStatus($id){
        $rombels = DataRombel::where('id_data_jenis_rombel', $id)->get();

        foreach ($rombels as $rombel) {
            $user = User::where('id_user', $rombel->id_user)->update(['status' => 1]);
        }

        $dataJenisRombel = DataJenisRombel::where('id_data_jenis_rombel', $id)->update(['status' => 1]);

        toastSuccess('Kelas yang anda pilih berhasil diaktifkan');
        return redirect()->back();
    }

    public function innactiveStatus($id){
        $rombels = DataRombel::where('id_data_jenis_rombel', $id)->get();

        foreach ($rombels as $rombel) {
            $user = User::where('id_user', $rombel->id_user)->update(['status' => 0]);
        }

        $dataJenisRombel = DataJenisRombel::where('id_data_jenis_rombel', $id)->update(['status' => 0]);
        
        toastSuccess('Kelas yang anda pilih berhasil diaktifkan');
        return redirect()->back();
    }
}
