<?php

namespace App\Http\Controllers;

use Validator;
use App\DataRombel;
use App\User;
use App\DataJenisRombel;
use App\DataNilaiEkstra;
use Illuminate\Http\Request;

class DataNilaiEkstraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rombels = DataJenisRombel::all();
        $jenis = DataJenisRombel::all();
        return view('kasek.nilai-ekstra.index', compact('rombels','jenis'));
    }

    public function detail($id){
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$id)->first();
        $rombels = DataRombel::where('id_data_jenis_rombel',$id)->get();
        return view('kasek.nilai-ekstra.detail', compact('rombels', 'jenis'));
    }

    public function detailNilai($id){
        $rombel = DataRombel::where('nis',$id)->first();
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$rombel->id_data_jenis_rombel)->first();
        $nilais = DataNilaiEkstra::where('nis',$id)->get();
        $user = User::where('nis',$id)->first();
        return view('kasek.nilai-ekstra.detail-nilai', compact('rombel', 'jenis', 'nilais', 'user'));
    }

    public function editNilai($id){
        $nilaiekstra = DataNilaiEkstra::where('id_data_nilai_ekstra',$id)->first();
        $user = User::where('nis',$nilaiekstra->nis)->first();
        return view('kasek.nilai-ekstra.edit', compact('nilaiekstra','user'));
    }

    public function updateNilai(Request $r, $id){
        $val = Validator::make($r->all(),[
            'nilai_siswa' => 'required'
        ]);

        if ($val->fails()) {
            toastError($val->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user = DataNilaiEkstra::where('id_data_nilai_ekstra',$id)->first();
            $nilai = DataNilaiEkstra::where('id_data_nilai_ekstra',$id)->update([
                'nilai_siswa' => $r->nilai_siswa
            ]);
            toastSuccess('Data Nilai berhasil diubah');
            return redirect(url('admin/nilai-ekstra/detail-nilai/'.$user->nis));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataNilaiEkstra  $dataNilaiEkstra
     * @return \Illuminate\Http\Response
     */
    public function show(DataNilaiEkstra $dataNilaiEkstra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataNilaiEkstra  $dataNilaiEkstra
     * @return \Illuminate\Http\Response
     */
    public function edit(DataNilaiEkstra $dataNilaiEkstra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataNilaiEkstra  $dataNilaiEkstra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataNilaiEkstra $dataNilaiEkstra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataNilaiEkstra  $dataNilaiEkstra
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataNilaiEkstra $dataNilaiEkstra)
    {
        //
    }
}
