<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\DataRombel;
use App\User;
use App\DataJenisRombel;
use App\DataKehadiranSiswa;
use Illuminate\Http\Request;

class DataKehadiranSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rombels = DataJenisRombel::all();
        $jenis = DataJenisRombel::all();
        return view('kasek.kehadiran.rombel', compact('rombels','jenis'));
    }

    public function detail($id){
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$id)->first();
        $kehadirans = DataRombel::select('data_rombels.*','data_kehadiran_siswas.*')
                  ->join('data_kehadiran_siswas','data_kehadiran_siswas.nis','=','data_rombels.nis')
                  ->where('data_rombels.id_data_jenis_rombel','=',$id)
                  ->get();
        return view('kasek.kehadiran.detail', compact('kehadirans', 'jenis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DataKehadiranSiswa  $dataKehadiranSiswa
     * @return \Illuminate\Http\Response
     */
    public function show(DataKehadiranSiswa $dataKehadiranSiswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DataKehadiranSiswa  $dataKehadiranSiswa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kehadiran = DataKehadiranSiswa::where('id_data_kehadiran_siswa',$id)->first();
        $siswa = User::where('nis',$kehadiran->nis)->first();
        return view('kasek.kehadiran.edit', compact('kehadiran','siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DataKehadiranSiswa  $dataKehadiranSiswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $validator = Validator::make($r->all(),[
            'total_sakit' => 'required|numeric',
            'total_ijin' => 'required|numeric',
            'total_tanpaket' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user = DataKehadiranSiswa::where('id_data_kehadiran_siswa',$id)->first();
            $rombel = DataRombel::where('nis',$user->nis)->first();
            $kehadiran = DataKehadiranSiswa::where('id_data_kehadiran_siswa', $id)->update([
                'total_sakit' => $r->total_sakit,
                'total_ijin' => $r->total_ijin,
                'total_tanpaket' => $r->total_tanpaket
            ]);
            toastSuccess('Data berhasil diubah');
            return redirect(url('admin/kehadiran/detail/'.$rombel->id_data_jenis_rombel));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DataKehadiranSiswa  $dataKehadiranSiswa
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataKehadiranSiswa $dataKehadiranSiswa)
    {
        //
    }

    public function kepsekKehadiran(){
        $rombels = DataJenisRombel::all();
        $jenis = DataJenisRombel::all();
        return view('kepsek.data-kehadiran.index', compact('rombels','jenis'));
    }

    public function kepsekDetailKehadiran($id){
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$id)->first();
        $rombels = DataRombel::where('id_data_jenis_rombel',$id)->get();
        return view('kepsek.data-kehadiran.detail', compact('rombels', 'jenis'));
    }

    public function kepsekSiswaKehadiran($id){
        $siswas = DataKehadiranSiswa::where('nis',$id)->get();
        $rombels = DataRombel::where('nis',$id)->first();
        $jenis = DataJenisRombel::where('id_data_jenis_rombel',$rombels->id_data_jenis_rombel)->first();
        
        return view('kepsek.data-kehadiran.detail-siswa', compact('siswas','jenis','rombels'));
    }
}
