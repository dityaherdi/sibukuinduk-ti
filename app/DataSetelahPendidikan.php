<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataSetelahPendidikan extends Model
{
    protected $primaryKey = 'id_data_setelah_pendidikan';

    protected $fillable = [
        'id_user',
        'melanjutkan_di_bekerja',
        'tgl_kerja',
        'nama_perusahaan_lembaga_dll',
        'penghasilan',
    ];
}
