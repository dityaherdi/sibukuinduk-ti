<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPribadiSiswa extends Model
{
    protected $primaryKey = 'id_data_pribadi_siswa';

    protected $fillable = [
        'id_user',
        'nama_siswa_lengkap',
        'nama_siswa_panggilan',
        'jenis_kelamin',
        'ttgl_lahir',
        'agama',
        'kewarganegaraan',
        'anak_keberapa',
        'jum_saudara_kandung',
        'jum_saudara_tiri',
        'jum_saudara_angkat',
        'status_anak',
        'bahasa_seharihari',
        'alamat',
        'no_tlp',
        'tinggal_dgn',
        'jarak_kesekolah',
        'golongan_darah',
        'penyakit_yang_pernah_diderita',
        'kelainan_jasmani',
        'tinggi',
        'berat_badan',
        'lulusan_dari',
        'tgl_lulus',
        'no_sttb',
        'lama_belajar',
        'pindahan_sekolah',
        'alasan',
        'diterima_kls',
        'kelompok',
        'jurusan',
        'tgl_diterima',
        'kesenian',
        'olahraga',
        'kemasyarakatan',
        'lainya',
        'foto'
    ];
}
