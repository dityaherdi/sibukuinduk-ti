<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataJenisRombel extends Model
{
    protected $primaryKey = 'id_data_jenis_rombel';

    protected $fillable = [
        'nama_rombel',
        'jurusan',
        'thn_ajaran',
        'status'
    ];

    public function rombel(){
        return $this->hasMany('App\DataRombel', 'id_data_rombel');
    }
}
