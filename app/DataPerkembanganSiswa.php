<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataPerkembanganSiswa extends Model
{
    protected $primaryKey = 'id_data_perkembangan_siswa';

    protected $fillable = [
        'id_user',
        'menerima_beasiswa',
        'tgl_meninggalkan_sekolah',
        'alasan_keluar',
        'thn_tamat_belajar',
        'no_sttb_tamat'
    ];
}
