<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataKeluargaSiswa extends Model
{
    protected $primaryKey = 'id_data_keluarga_siswa';

    protected $fillable = [
        'id_user',
        'nama_ayah',
        'ttgl_lahir_ayah',
        'agama_ayah',
        'kewarganegaraan_ayah',
        'pendidikan_ayah',
        'pekerjaan_ayah',
        'penghasilan_bln_ayah',
        'alamat_ayah',
        'tlp_ayah',
        'ket_ayah',
        'nama_ibu',
        'ttgl_lahir_ibu',
        'agama_ibu',
        'kewarganegaraan_ibu',
        'pendidikan_ibu',
        'pekerjaan_ibu',
        'penghasilan_bln_ibu',
        'alamat_ibu',
        'tlp_ibu',
        'ket_ibu',
        'nama_wali',
        'ttgl_lahir_wali',
        'agama_wali',
        'kewarganegaraan_wali',
        'pendidikan_wali',
        'pekerjaan_wali',
        'penghasilan_bln_wali',
        'alamat_wali',
        'tlp_wali',
        'ket_wali'
    ];
}
