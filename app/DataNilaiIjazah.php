<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataNilaiIjazah extends Model
{
    protected $primaryKey = 'id_data_nilai_ijazah';

    protected $fillable = [
        'kode_rombel',
        'id_data_mata_pelajaran',
        'nis',
        'nilai_rata_rata',
        'nilai_ujian'
    ];
}
