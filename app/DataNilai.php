<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataNilai extends Model
{
    protected $primaryKey = 'id_data_nilai';

    protected $fillable = [
        'kode_rombel',
        'id_user',
        'id_data_mata_pelajaran',
        'nis',
        'nilai_pengetahuan',
        'nilai_keterampilan',
        'nilai_sikap'
    ];
}
