<?php
namespace App\Helpers;
use App\DataMataPelajaran;
class MapelHelper {
    
    // public static $allowedMapel = [
    //     'pendidikan_agama_dan_budi_pekerti',
    //     'ppkn',
    //     'bahasa_indonesia',
    //     'matematika',
    //     'bahasa_inggris',
    //     'penjasorkes',
    //     'bahasa_daerah_bali',
    //     'desain_grafis_percetakan',
    //     'teknik_animasi_2d_dan_3d',
    //     'produk_kreatif_dan_kewirausahaan'
    // ]

    public static function allowedMapel()
    {
        return DataMataPelajaran::pluck('nama_mp')->toArray();
    }

    public static function namaMapel($value) {
        
        // dd($allowedMapel);
        $valueArray = array_keys($value->toArray());
        $mapel = array_filter($valueArray, function ($nama) {
            return in_array($nama, self::allowedMapel());
            // return in_array($nama, self::$allowedMapel);
        });
        
        return array_values($mapel);
    }
}