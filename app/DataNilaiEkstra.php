<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataNilaiEkstra extends Model
{
    protected $primaryKey = 'id_data_nilai_ekstra';

    protected $fillable = [
        'id_data_ekstra',
        'nis',
        'semester',
        'id_user',
        'nilai_siswa'
    ];

    public function ekstras() {
        return $this->belongsTo('App\DataEkstra', 'id_data_ekstra', 'id_data_ekstra');
    }
}
