<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataRombel extends Model
{
    protected $primaryKey = 'id_data_rombel';

    protected $fillable = [
        'id_data_jenis_rombel',
        'id_user',
        'nis'
        // 'thn_ajaran'
    ];

    public function jenisRombel(){
        return $this->belongsTo('App\DataJenisRombel','id_data_jenis_rombel');
    }

    public function user(){
        return $this->belongsTo('App\User','id_user');
    }
}
