<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataKehadiranSiswa extends Model
{
    protected $primaryKey = 'id_data_kehadiran_siswa';

    protected $fillable = [
        'id_user',
        'nis',
        'semester',
        'total_sakit',
        'total_ijin',
        'total_tanpaket'
    ];
}
