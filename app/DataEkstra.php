<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataEkstra extends Model
{
    protected $primaryKey = 'id_data_ekstra';

    protected $fillable = [
        'nama_ekstra',
        'pembina_ekstra',
        'deskripsi_ekstra'
    ];
}
