<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Hash;

class UserImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'nis' => $row['nis'],
            'nama' => $row['nama'],
            'username' => $row['nis'],
            'password' => $this->generatePassword($row['nis']), 
            'level' => $row['level'],
            'status' => $row['status']
        ]);
    }

    private function generatePassword($nis) {
        // $strArrName = explode(" ", $name);
        // $lastName = end($strArrName);

        // nama yang terakhir (ex. Putra, Putri) + nis
        // Contoh I Putu Pertama NIS 0011 => passwordnya jadi Pertama0011
        return Hash::make($nis);
    }
}
