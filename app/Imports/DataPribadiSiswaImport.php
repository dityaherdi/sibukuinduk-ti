<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\DataPribadiSiswa;
use App\DataKeluargaSiswa;
use App\DataSetelahPendidikan;
use App\DataPerkembanganSiswa;
use App\User;

class DataPribadiSiswaImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $nisToGet = $collection->pluck('nis');
        // dd($nisToGet);
        $users = User::whereIn('nis', $nisToGet)->get();

        $siswas = [];
        foreach ($collection as $key => $value) {
            $idUser = $users->where('nis', $value['nis'])->pluck('id_user')->toArray()[0];
            
            $siswa = [
                'id_user' => $idUser,
                'nama_siswa_lengkap' => $value['nama']
            ];

            array_push($siswas, $siswa);
        }

        $keluarga = [];
        foreach ($collection as $key => $value) {
            $idUser = $users->where('nis', $value['nis'])->pluck('id_user')->toArray()[0];
            
            $siswa = [
                'id_user' => $idUser
            ];

            array_push($keluarga, $siswa);
        }

        $pendidikan = [];
        foreach ($collection as $key => $value) {
            $idUser = $users->where('nis', $value['nis'])->pluck('id_user')->toArray()[0];
            
            $siswa = [
                'id_user' => $idUser
            ];

            array_push($pendidikan, $siswa);
        }

        $perkembangan = [];
        foreach ($collection as $key => $value) {
            $idUser = $users->where('nis', $value['nis'])->pluck('id_user')->toArray()[0];
            
            $siswa = [
                'id_user' => $idUser
            ];

            array_push($perkembangan, $siswa);
        }


        DataPribadiSiswa::insert($siswas);
        DataKeluargaSiswa::insert($keluarga);
        DataSetelahPendidikan::insert($pendidikan);
        DataPerkembanganSiswa::insert($perkembangan);
    }
}
