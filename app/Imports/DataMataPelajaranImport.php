<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\DataMataPelajaran;
use App\Helpers\MapelHelper;
use DB;
class DataMataPelajaranImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {

        $dataMapelToInsert = [];
        foreach ($collection as $value) {
            foreach ($listMapel = MapelHelper::namaMapel($value) as $nama) {
                $mapel = [
                    'nama_mp' => $nama,
                    'semester' => $value['semester'],
                    'jurusan' => null,
                    'kkm' => null
                ];

                array_push($dataMapelToInsert, $mapel);
            }
            
        }

        $listMapelUnique = array_map('unserialize', array_unique(array_map('serialize', $dataMapelToInsert)));

        foreach ($listMapelUnique as $key => $value) {
            $dmp = DataMataPelajaran::firstOrCreate(['nama_mp' => $value['nama_mp']], $value);
        }
        // DataMataPelajaran::insert($listMapelUnique);
    }
}
