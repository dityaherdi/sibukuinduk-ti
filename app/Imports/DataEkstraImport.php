<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\DataEkstra;

class DataEkstraImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $ekstraToImport = [];
        $listEkstra = array_keys($collection->groupBy('jenis_ekstra')->toArray());

        foreach ($listEkstra as $key => $value) {
            $ekstra = [
                'nama_ekstra' => $value,
                'pembina_ekstra' => null,
                'deskripsi_ekstra' => null
            ];

            array_push($ekstraToImport, $ekstra);
        }
        // dd($ekstraToImport);

        foreach ($ekstraToImport as $key => $ekstra) {
            $eks = DataEkstra::firstOrCreate([ 'nama_ekstra' => $ekstra['nama_ekstra'] ]);
        }
        // DataEkstra::insert($ekstraToImport);

    }

}