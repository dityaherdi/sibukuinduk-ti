<?php

namespace App\Imports;

use App\DataRombel;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DataRombelImport implements ToModel, WithHeadingRow
{
    public function  __construct($jenisRombelId)
    {
        $this->jenisRombelId = $jenisRombelId;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new DataRombel([
            'id_data_jenis_rombel' => $this->jenisRombelId,
            'id_user' => $this->getIdUser($row['nis']),
            'nis' => $row['nis']
        ]);
    }

    public function getIdUser($nis) {
        return User::where('nis', $nis)->value('id_user');
    }
}
