<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\DataNilaiEkstra;
use App\DataEkstra;

class DataNilaiEkstraImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        // $ekstra = DataEkstra::all('id_data_ekstra', 'nama_ekstra');
        $nilaiEkstraToImport = [];
        // dd($ekstra);

        // dd($collection);
        $listEkstra = array_keys($collection->groupBy('jenis_ekstra')->toArray());
        $ekstra = DataEkstra::whereIn('nama_ekstra', $listEkstra)->get();
        // dd($ekstra, $listEkstra);
        // foreach ($collection as $key => $value) {
        //     foreach ($listEkstra as $key => $nama) {
        //         $nilaiEkstra = [
        //             'id_data_ekstra' => $ekstra->where('nama_ekstra', $nama)->first()['id_data_ekstra'],
        //             'nis' => $value['nis'],
        //             'semester' => $value['semester'],
        //             // 'id_user' => null,
        //             'nilai_siswa' => $value['nilai']
        //         ];

        //         array_push($nilaiEkstraToImport, $nilaiEkstra);
        //     }

        // }
        // DataNilaiEkstra::insert($nilaiEkstraToImport);
        // dd($ekstra);
        foreach ($collection as $key => $value) {
            // dd($value);
            $nilaiEkstra = [
                'id_data_ekstra' => $ekstra->where('nama_ekstra', $value['jenis_ekstra'])->first()['id_data_ekstra'],
                'nis' => $value['nis'],
                'semester' => $value['semester'],
                // 'id_user' => null,
                'nilai_siswa' => $value['nilai']
            ];
            array_push($nilaiEkstraToImport, $nilaiEkstra);
        }

        // dd($nilaiEkstraToImport);
        foreach ($nilaiEkstraToImport as $key => $value) {
            $dne = DataNilaiEkstra::firstOrCreate([
                'nis' => $value['nis'],
                'id_data_ekstra' => $value['id_data_ekstra']
            ], $value);
        }
    }

    // private function ekstraId($ekstra, $value) {
    //     foreach ($ekstra as $key => $namaEkstra) {
    //         if ($namaEkstra['nama_ekstra'] == $value) {
    //             return $namaEkstra['id_data_ekstra'];
    //         }
    //     }
    // }
}