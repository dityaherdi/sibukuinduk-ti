<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\DataNilai;
use App\DataMataPelajaran;
use App\Helpers\MapelHelper;

class DataNilaiImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        // $mapel = DataMataPelajaran::all();
        $nilaiToInsert = [];

        // foreach ($collection as $key => $value) {
        //     foreach (MapelHelper::namaMapel($value) as $key => $nama) {
        //         // dd($nama);
        //         $nilai = [
        //             'kode_rombel' => $value['kelas'],
        //             // 'id_user' => null,
        //             'id_data_mata_pelajaran' => $value['semester'].'_'.$nama,
        //             // 'id_data_mata_pelajaran' => $this->mapelId($mapel, $nama),
        //             'nis' => $value['nis'],
        //             'nilai_pengetahuan' => $this->nilaiPengetahuan($value, $nama),
        //             'nilai_keterampilan' => $this->nilaiKeterampilan($value, $nama),
        //             'nilai_sikap' => $this->nilaiSikap($value, $nama)
        //         ];
    
        //         array_push($nilaiToInsert, $nilai);
        //     }
        // }
        
        // DataNilai::insert($nilaiToInsert);
        // $collection->each(function ($item, $key) {
        //     dd($item->except(['nis', 'nama', 'kelas', 'semester', 'jenis_ekstra', 'nilai', 's', 'i', 'a'])->keys());
        // });

        foreach ($collection as $key => $value) {
            $mapelFromExcel = $value->except(['nis', 'nama', 'kelas', 'semester', 'jenis_ekstra', 'nilai', 's', 'i', 'a']);
            foreach ($mapelFromExcel as $key => $nilai) {

                $nilai = [
                    'kode_rombel' => $value['kelas'],
                    // 'id_user' => null,
                    'id_data_mata_pelajaran' => $value['semester'].'_'.$key,
                    // 'id_data_mata_pelajaran' => $this->mapelId($mapel, $nama),
                    'nis' => $value['nis'],
                    'nilai_pengetahuan' => $this->nilaiPengetahuan($nilai),
                    'nilai_keterampilan' => $this->nilaiKeterampilan($nilai),
                    'nilai_sikap' => $this->nilaiSikap($nilai)
                ];
    
                array_push($nilaiToInsert, $nilai);
            }
        }

        // dd($nilaiToInsert);

        foreach ($nilaiToInsert as $key => $value) {
            // dd($value);
            $nil = DataNilai::firstOrCreate([
                'id_data_mata_pelajaran' => $value['id_data_mata_pelajaran'],
                'nis' => $value['nis']
            ], $value);
        }
        // DataNilai::insert($nilaiToInsert);

    }

    // private function mapelId($mapel, $value) {
    //     foreach ($mapel as $key => $namaMapel) {
    //         // dd($namaMapel, $value);
    //         if ($namaMapel['nama_mp'] == $value) {
    //             return $namaMapel['id_data_mata_pelajaran'];
    //         }
    //     }
    // }

    private function nilaiPengetahuan($nilai) {
        return explode('|', $nilai)[0];
        // return explode('|', $value[$nama])[0];
    }
    
    private function nilaiKeterampilan($nilai) {
        return explode('|', $nilai)[1];
    }

    private function nilaiSikap($nilai) {
        return explode('|', $nilai)[2];
    }
}
