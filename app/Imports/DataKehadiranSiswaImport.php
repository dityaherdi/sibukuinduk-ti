<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\DataKehadiranSiswa;

class DataKehadiranSiswaImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $kehadiranToImport = [];

        foreach ($collection as $key => $value) {
            $kehadiran = [
                // 'id_user' => null,
                'nis' => $value['nis'],
                'semester' => $value['semester'],
                'total_sakit' => $value['s'],
                'total_ijin' => $value['i'],
                'total_tanpaket' => $value['a']
            ];

            array_push($kehadiranToImport, $kehadiran);
        }

        DataKehadiranSiswa::insert($kehadiranToImport);

    }
}
