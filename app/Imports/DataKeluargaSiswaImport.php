<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\DataKeluargaSiswa;
use App\User;

class DataKeluargaSiswaImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        $nisToGet = $collection->pluck('nis');
        $users = User::whereIn('nis', $nisToGet)->get();
        // dd($nisToGet);
        // $siswas = [];
        // foreach ($collection as $key => $value) {
        //     $idUser = $users->where('nis', $value['nis'])->pluck('id_user')->toArray()[0];
            
        //     $siswa = [
        //         'id_user' => $idUser
        //     ];

        //     array_push($siswas, $siswa);
        // }

        // DataKeluargaSiswa::insert($siswas);
    }
}
