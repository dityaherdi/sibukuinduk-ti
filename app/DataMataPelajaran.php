<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataMataPelajaran extends Model
{
    protected $primaryKey = 'id_data_mata_pelajaran';
    
    protected $fillable = [
        'kode_mapel',
        'nama_mp',
        'semester',
        'jurusan',
        'kkm'
    ];
}
