<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
Auth::routes(['register' => false, 'verify' => true]);

Route::get('/admin', function(){
    return redirect('/home');
});

// ROUTE PUBLIC
Route::group(['middleware' => 'auth'], function(){
    Route::get('admin/reset', 'UserController@resetDB');;
    Route::get('/home', 'DashboardController@index')->name('home');

    Route::get('profile', 'UserController@indexProfile');
    Route::put('profile/image/{id_user}', 'UserController@changeAvatar');
    Route::put('profile/{id_user}', 'UserController@updateBio');
    Route::put('profile/password/{id_user}', 'UserController@changePasswordAuth');

    Route::get('admin/user', 'UserController@index');
    Route::post('admin/user/tambah', 'UserController@store');
    Route::put('admin/user/password/{id}', 'UserController@changePassword');
    Route::put('admin/user/aktif/{id}', 'UserController@changeActive');
    Route::put('admin/user/nonaktif/{id}', 'UserController@changeInnactive');
    Route::post('admin/user/import', 'UserController@importExcel');

    Route::get('admin/master', 'UtilitiesImportController@index');

    Route::get('admin/mata-pelajaran', 'DataMataPelajaranController@index');
    Route::get('admin/mata-pelajaran/tambah', 'DataMataPelajaranController@create');
    Route::post('admin/mata-pelajaran/tambah', 'DataMataPelajaranController@store');
    Route::get('admin/mata-pelajaran/{id}/edit', 'DataMataPelajaranController@edit');
    Route::put('admin/mata-pelajaran/{id}/edit', 'DataMataPelajaranController@update');

    Route::get('admin/ekstra', 'DataEkstraController@index');
    Route::get('admin/ekstra/tambah', 'DataEkstraController@create');
    Route::post('admin/ekstra/tambah', 'DataEkstraController@store');
    Route::get('admin/ekstra/{id}/edit', 'DataEkstraController@edit');
    Route::put('admin/ekstra/{id}/edit', 'DataEkstraController@update');

    Route::get('admin/jenis-rombel', 'DataJenisRombelController@index');
    Route::post('admin/jenis-rombel/tambah', 'DataJenisRombelController@store');
    Route::put('admin/jenis-rombel/{id}/edit', 'DataJenisRombelController@update');

    Route::get('admin/rombel', 'DataRombelController@index');
    Route::get('admin/rombel/tambah', 'DataRombelController@create');
    Route::post('admin/rombel/tambah', 'DataRombelController@store');
    Route::get('admin/rombel/detail/{id}', 'DataRombelController@detail');
    Route::post('admin/rombel/import', 'DataRombelController@import')->name('import:rombel');
    Route::delete('admin/rombel/delete/{id_data_rombel}', 'DataRombelController@destroy');

    Route::get('admin/kehadiran', 'DataKehadiranSiswaController@index');
    Route::get('admin/kehadiran/detail/{id}', 'DataKehadiranSiswaController@detail');
    Route::get('admin/kehadiran/{id}/edit', 'DataKehadiranSiswaController@edit');
    Route::put('admin/kehadiran/{id}/edit', 'DataKehadiranSiswaController@update');

    Route::get('admin/keluarga', 'DataKeluargaSiswaController@index');

    Route::get('admin/perkembangan', 'DataPerkembanganSiswaController@index');

    Route::get('admin/setelah-pendidikan', 'DataSetelahPendidikanController@index');

    Route::get('admin/nilai-ekstra', 'DataNilaiEkstraController@index');
    Route::get('admin/nilai-ekstra/detail/{id}', 'DataNilaiEkstraController@detail');
    Route::get('admin/nilai-ekstra/detail-nilai/{id}', 'DataNilaiEkstraController@detailNilai');
    Route::get('admin/nilai-ekstra/{id}/edit', 'DataNilaiEkstraController@editNilai');
    Route::put('admin/nilai-ekstra/{id}/edit', 'DataNilaiEkstraController@updateNilai');

    Route::get('admin/nilai', 'DataNilaiController@index');
    Route::get('admin/data-nilai/detail/{id}', 'DataNilaiController@detail');
    Route::get('admin/data-nilai/detail-nilai/{id}', 'DataNilaiController@detailNilai');
    Route::get('admin/data-nilai/{id}/edit', 'DataNilaiController@editNilai');
    Route::put('admin/data-nilai/{id}/edit', 'DataNilaiController@updateNilai');

    Route::get('admin/tabel-buku-induk', 'BukuIndukController@tabelBukuInduk');
    Route::get('admin/tabel-buku-induk/download/{id_jenis_rombel}', 'BukuIndukController@downloadTabelBukuInduk');
    Route::get('admin/lembar-buku-induk', 'BukuIndukController@lembarBukuInduk');
    Route::get('admin/lembar-buku-induk/export/{id}', 'BukuIndukController@detailLembarBukuInduk');
    // Route::get('admin/laporan-hasil-belajar', 'BukuIndukController@hasilBelajarSiswa');
    Route::get('admin/laporan-hasil-belajar', 'BukuIndukController@laporanHasilBelajar');
    Route::get('admin/laporan-hasil-belajar/cetak', 'BukuIndukController@searchSemester');
    Route::get('admin/laporan-hasil-belajar/detail/{id}', 'BukuIndukController@detailNilai');
    Route::get('admin/laporan-hasil-belajar/search', 'BukuIndukController@dsearch');
    Route::get('admin/laporan-hasil-belajar/print/{id}', 'BukuIndukController@printLaporanHasilBelajar');
    
    Route::get('admin/export-buku-induk/{id_siswa}', 'BukuIndukController@exportBukuInduk')->name('export:bukuinduk');
    // Route::get('admin/export-lembar-nilai/{id_user?}/{rombel?}', 'BukuIndukController@exportLembarNilai')->name('export:lembarnilai');
    Route::get('admin/export-lembar-nilai/{id_user?}/{rombel?}/{semester?}', 'BukuIndukController@exportLembarNilai')->name('export:lembarnilai');

    Route::get('admin/data-pribadi', 'DataPribadiSiswaController@dataPribadiSiswa');
    Route::get('admin/data-pribadi/detail/{id}', 'DataPribadiSiswaController@detail');
    Route::get('admin/data-pribadi/data/{id}', 'DataPribadiSiswaController@editDataPribadiSiswa');
    Route::put('admin/data-pribadi-siswa/editpendidikan', 'DataSetelahPendidikanController@updatePendidikan');
    Route::put('admin/data-pribadi-siswa/editperkembangan', 'DataPerkembanganSiswaController@updatePerkembangan');

    // KELAS
    Route::get('admin/kelas', 'DataKelasController@index');
    Route::put('admin/kelas/nonaktif/{id}', 'DataKelasController@innactiveStatus');
    Route::put('admin/kelas/aktif/{id}', 'DataKelasController@activeStatus');

    // IJAZAH
    Route::get('admin/ijazah', 'IjazahController@index');
    Route::get('admin/data-ijazah/{id}', 'IjazahController@detail');
    Route::get('admin/ijazah/nilai/{nis}', 'IjazahController@nilai');
    Route::post('admin/ijazah/nilai/insert', 'IjazahController@insert');
    Route::get('admin/ijazah/nilai/hapus/{id}/{nis}', 'IjazahController@deleteOneNilai');

    // DATA PRIBADI
    Route::get('admin/siswa/tambah', 'DataPribadiSiswaController@create')->name('create:siswa');
    Route::post('admin/siswa/tambah', 'DataPribadiSiswaController@store')->name('store:siswa');
    Route::get('admin/siswa/edit/{dataPribadiSiswa}', 'DataPribadiSiswaController@edit')->name('edit:siswa');
    Route::put('admin/siswa/update/{dataPribadiSiswa}', 'DataPribadiSiswaController@update')->name('update:siswa');
    Route::delete('admin/siswa/delete/{id}', 'DataPribadiSiswaController@destroy')->name('delete:siswa');

    Route::post('admin/utils/import', 'UtilitiesImportController@importExcel');


    // SISWA
    Route::get('siswa/datapribadi', 'DataPribadiSiswaController@dataSiswa');
    Route::put('siswa/data-pribadi-siswa/edit', 'DataPribadiSiswaController@updateSiswa');
    Route::put('siswa/data-pribadi-siswa/editkeluarga', 'DataKeluargaSiswaController@updateKeluarga');

    // KEPSEK
    Route::get('kepsek/data-pribadi', 'DataPribadiSiswaController@kepsekDataSiswa');
    Route::get('kepsek/data-pribadi/data/{id}', 'DataPribadiSiswaController@showDataPribadiSiswa');

    Route::get('kepsek/data-nilai', 'DataNilaiController@kepsekDataNilai');
    Route::get('kepsek/data-nilai/detail/{id}', 'DataNilaiController@kepsekDetailNilai');
    Route::get('kepsek/detail-nilai/siswa/{id}', 'DataNilaiController@kepsekDetailNilaiSiswa');

    Route::get('kepsek/kehadiran', 'DataKehadiranSiswaController@kepsekKehadiran');
    Route::get('kepsek/kehadiran/detail/{id}', 'DataKehadiranSiswaController@kepsekDetailKehadiran');
    Route::get('kepsek/kehadiran/siswa/{id}', 'DataKehadiranSiswaController@kepsekSiswaKehadiran');
});

// Route::view('bukuinduk', 'kasek.buku-induk.buku-induk');
